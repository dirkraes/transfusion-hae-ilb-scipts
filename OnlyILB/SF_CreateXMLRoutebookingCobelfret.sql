ALTER FUNCTION "usr"."CreateXMLRoutebookingCobelfret"( in in_ordmodalityid integer,in in_type varchar(20) ) 
returns integer
/*
created     NOV 2011 mvr
modified    NOV 24 2011 mvr
knip een paar jaar
dec  5 2014 mvr and isnull(imoclass,'') <> '' and hardcoded equipmenttype GRH21;
dec  8 2014 mvr imoclass -> extract first part
dec  8 2014 mvr map ToSeeburger
jan 06 2015 mvr use option GDS data to decide which field to use propershippingname or ... fixed
jan 07 2015 mvr local_equipmentlength is not a string so set 782 instead '782'
jan 08 2015 mvr 
jan 16 2015 mvr implement CreateXMLstring
feb 10 2015 mvr HS implement map Seeburger
apr 23 2015 mvr added consignee vat - removed SHIPPER 
apr 24 2015 mvr added vat number ILB
may 15 2015 mvr added vat hs ilb
feb 18 2016 mvr return routecode max 6 chars
jun 28 2016 mvr implement VGM
jun 29 2016 mvr implement VGM - sum quantity all products
jun 30 2016 mvr implement VGM - puntjes op de i
jul 01 2016 mvr implement VGM - puntjes op de i vwb vgmResponsible
jul 06 2016 mvr implement VGM - puntjes op de i vwb vgmResponsible HS Hempenius 
jul 10 2016 mvr implement VGM - puntjes op de i vwb vgmResponsible HS Hempenius uppercase
oct 26 2016 mvr implement routecode -
oct 16 2016 mvr implement VGM - vgmResponsible uppercase Hae Ilb
mar 26 2019 mvr implement brexit changes
oct 22 2019 mvr implement brexit changes
nov 20 2020 PvdL Changes TSPPermit into CFSPEIDRPermit (not variables!) 
consignment info 
imdginfo UN 3257 then only when FULL 
// mvr NOV 03 2020
// mvr NOV 25 2020 brexit
// mvr DEC 10 2020 brexit
// mvr DEC 14 2020 brexit ILB
iscontrolledgood and CFSPEIDRPermit
*/
begin
  declare ReturnValue integer;
  declare local_lineCount integer;
  declare local_lineNumber integer;
  declare Tofilename varchar(250);
  declare local_HoursAdd integer;
  declare local_firstOrdproductid integer;
  declare local_orderid integer;
  declare local_InvoiceDepartment integer;
  declare local_dateOriginal date;
  declare local_timeOriginal time;
  declare local_clean integer;
  declare local_BrowseSequence integer;
  declare local_ordproductid integer;
  declare ThisCompany varchar(250);
  declare local_vgmResponsible varchar(250);
  declare local_map varchar(250);
  declare local_Modalitycodefld varchar(250);
  declare local_DepartureTerminalcodefld varchar(250);
  declare local_DepartureTerminalUIRR varchar(250);
  declare local_DepartureTerminalname varchar(250);
  declare local_DepartureTerminalPostalcode varchar(250);
  declare local_DepartureTerminalCityname varchar(250);
  declare local_DepartureTerminalCountrycode varchar(250);
  declare local_DestinationTerminalcodefld varchar(250);
  declare local_DestinationTerminalUIRR varchar(250);
  declare local_DestinationTerminalname varchar(250);
  declare local_DestinationTerminalPostalcode varchar(250);
  declare local_DestinationTerminalCityname varchar(250);
  declare local_DestinationTerminalCountrycode varchar(250);
  declare local_ProductShipperId integer;
  declare local_ProductShipperName varchar(250);
  declare local_ProductShipperAddress varchar(250);
  declare local_ProductShipperPlace varchar(250);
  declare local_ProductShipperCountry varchar(250);
  declare local_ProductImporterId integer;
  declare local_ProductImporterName varchar(250);
  declare local_ProductImporterAddress varchar(250);
  declare local_ProductImporterPlace varchar(250);
  declare local_ProductImporterCountry varchar(250);
  declare local_ProductImporterEORInr varchar(250);
  declare local_ProductImporterVATNumber varchar(250);
  declare local_ProductImporterTSPPermit varchar(250);
  declare local_ProductExporterId integer;
  declare local_ProductExporterName varchar(250);
  declare local_ProductExporterAddress varchar(250);
  declare local_ProductExporterPlace varchar(250);
  declare local_ProductExporterCountry varchar(250);
  declare local_ProductExporterEORInr varchar(250);
  declare local_ProductExporterVATNumber varchar(250);
  declare local_ProductConsigneeId integer;
  declare local_ProductConsigneeName varchar(250);
  declare local_ProductConsigneeAddress varchar(250);
  declare local_ProductConsigneePlace varchar(250);
  declare local_ProductConsigneeCountry varchar(250);
  declare local_ProductNotifyId integer;
  declare local_ProductNotifyName varchar(250);
  declare local_ProductNotifyAddress varchar(250);
  declare local_ProductNotifyPlace varchar(250);
  declare local_ProductNotifyCountry varchar(250);
  declare local_ProductNotifyEORInr varchar(250);
  declare local_ProductNotifyVATNumber varchar(250);
  declare local_ConsigneeName varchar(250);
  declare local_ConsigneeVat varchar(250);
  declare local_ShipperName varchar(250);
  declare local_ShipperCountry varchar(250);
  declare local_ShipperPlace varchar(250);
  declare local_TankcontainerNumber varchar(250);
  declare local_ISOSizeTypeCode varchar(250);
  declare local_Equipmentweight integer;
  declare local_Equipmentlength numeric(5,2);
  declare local_Equipmentwidth numeric(5,2);
  declare local_Equipmentheight numeric(5,2);
  declare local_Equipmentcapacity integer;
  declare local_EquipmentIsotank integer;
  declare local_EquipmentIsGlycolheating integer;
  declare local_EquipmentIsElectricalheating integer;
  declare local_interunitCode varchar(250);
  declare local_IsFull varchar(10);
  declare local_TotalLoaded integer;
  declare local_grossWeight integer;
  declare local_ediCounter integer;
  declare local_MGRemailaddress varchar(250);
  declare local_USRemailaddress varchar(250);
  declare local_emailaddressRejected varchar(250);
  declare local_isintermodal integer;declare local_BookingAddressId integer;
  declare local_generalinstructions varchar(250);
  declare local_sequence integer;
  declare local_BookingReference varchar(250);
  declare local_isRouteLOFO integer;
  declare local_RouteIMO4HazardousGoodsAllowed integer;
  declare local_instructionGeneral varchar(255); // cut off at 60 chars -> Cobelfret does not accept more chars!!
  declare local_CustomsRemarks varchar(250);
  declare local_BookingStatus varchar(250);
  declare local_ordmodalityStatus integer;
  declare local_roadCouplingCode integer;
  declare local_ituType varchar(250);
  declare local_isDangerousgood integer;
  declare local_exDangerousMark varchar(250);
  declare local_productdescription varchar(250);
  declare local_Fullproductdescription varchar(250);
  declare Local_ProductTemperature numeric(5,2);
  declare Local_ProductHarmonizedSystemCode varchar(250);
  declare Local_OrdProductCustomsStatus varchar(250);
  declare Local_OrdProductCustomsDocumentNumber varchar(250);
  declare Local_OrdProductDocumentExpiryDate varchar(250);
  declare local_productGrade varchar(250);
  declare local_productUnnumber varchar(250);
  declare local_productAdrClass varchar(250);
  declare local_productImoClass varchar(250);
  declare local_productWeight integer;
  declare local_productProperShippingNameFixed varchar(550);
  declare local_productProperShippingNameAdditional varchar(550);
  declare local_productIsControlledGood integer;
  declare local_productPackagingGroup integer;
  declare local_productIsNaFlashPoint integer;
  declare local_productFlashPoint numeric(30);
  declare local_productMarinePolution varchar(255);
  declare local_productEmSNo varchar(255);
  declare local_productQuantity integer;
  declare Local_productLiquidThermostatSetting integer;
  declare local_hazardousdescription varchar(500);
  declare XmLfile long varchar;
  declare local_invoiceeName varchar(250);
  declare local_invoiceeUIRRCode varchar(250);
  declare local_receiverName varchar(250);
  declare local_receiverUIRRCode varchar(250);
  declare Local_SenderUirrCode varchar(250);
  declare Local_SenderRegistrationCode varchar(250);
  declare Local_SenderPrefix varchar(250);
  declare Local_SenderName varchar(250);
  declare Local_SenderEmail varchar(250);
  declare Local_OperatorUirrCode varchar(250);
  declare Local_OperatorName varchar(250);
  declare local_Seconds integer;
  declare local_routeCodefld varchar(10);
  declare local_routeID integer;
  declare local_RouteSelfdrive integer;
  declare local_contractcode varchar(255);
  declare local_LiquidThermostatSetting integer;
  declare local_IsplugAtPortOfLoadingCode integer;
  declare local_IsplugOnVesselCode integer;
  declare local_IsplugAtPortOfDischargeCode integer;
  declare local_IsemptyUncleaned varchar(10);
  //  declare Local_SenderTransportId               varchar(250);
  //  declare Local_ReceiverTransportId             varchar(250);
  declare err_notfound exception for sqlstate value '02000';
  declare Cursor_Products dynamic scroll cursor for select distinct ordproduct.ordproductid
      from ordmodproduct
        join ordproduct
        left outer join ordmodality on ordmodproduct.ordmodalityid = ordmodality.ordmodalityid
        left outer join route on route.routeid = ordmodality.routeid
        join product
        left outer join modality on modality.modalityid = route.modalityid
      where ordproduct.orderid = local_orderid and ordmodality.browsesequence <= local_browsesequence;
  select getoption('Inforit_Company') into ThisCompany;
  if ThisCompany = 'DH' then set local_HoursAdd = 3
  else set local_HoursAdd = 3
  end if;
  select ordmodality.orderid,
    (if ThisCompany = 'ILB' then '' else BookingReference endif),
    isRouteLOFO,
    datediff(second,ordmodality.modified,getdate()),
    Orders.InvoiceDepartment,
    isnull(ordmodality.ediCounter,0)+1,
    ordmodality.BrowseSequence,
    Modality.Modalitycode,
    (if dateadd(hour,local_HoursAdd,ordmodality.timeOriginal) > '23:59' then DATEFORMAT(dateadd(day,1,ordmodality.dateOriginal),'yyyy-mm-dd') else DATEFORMAT(ordmodality.dateOriginal,'yyyy-mm-dd') endif),
    DATEFORMAT(dateadd(hour,local_HoursAdd,ordmodality.timeOriginal),'hh:nn'),
    isnull(ordmodality.clean,0),
    ordmodality.instructionGeneral,
    ordmodality.CustomsRemarks,
    DepartureTerminal.codefld,
    DepartureTerminal.UIRR,
    DepartureTerminal.name,
    DepartureTerminal.locationpostalcode,
    DepP.Name,
    DepC.Codefld,
    DestinationTerminal.codefld,
    DestinationTerminal.UIRR,
    DestinationTerminal.name,
    DestinationTerminal.locationpostalcode,
    DestP.Name,
    DestC.Codefld,
    (if Equipment.ContainerTruckObject = 'C' then substr(TankcontainerNumber,1,4) || substr(TankcontainerNumber,6,6) || '/' || substr(TankcontainerNumber,13,1) else TankcontainerNumber endif),
    Equipment.ISOSizeTypeCode,
    if Equipment.weight > 0 then Equipment.weight else null endif,
    if Equipment.length > 0 then Equipment.length else null endif,
    (if equipmenttype.codefld = 'GRH21' then 'G' else Equipment.ContainerTruckObject endif),
    if Equipment.width > 0 then Equipment.width else null endif,
    if Equipment.height > 0 then Equipment.height else null endif,
    Equipment.capacity,
    (if Equipment.Isotank = 1 then 'I' else 'I' endif),
    Equipment.Glycol,
    Fleetserie.heatingElectrical,
    users.emailaddress,
    route.routeid,
    isnull(GetExternalIdentifier(route.BookingAddress,'route',route.routeid),route.codefld),
    route.isintermodal,
    route.BookingAddress,
    route.generalinstructions,
    Route.Selfdriver,
    Route.IMO4HazardousGoodsAllowed
    into local_orderid,
    local_BookingReference,
    local_isRouteLOFO,
    local_Seconds,
    local_InvoiceDepartment,
    local_ediCounter,
    local_BrowseSequence,
    local_Modalitycodefld,
    local_dateOriginal,
    local_timeOriginal,
    local_clean,
    local_instructionGeneral,
    local_CustomsRemarks,
    local_DepartureTerminalcodefld,
    local_DepartureTerminalUIRR,
    local_DepartureTerminalname,
    local_DepartureTerminalPostalcode,
    local_DepartureTerminalCityname,
    local_DepartureTerminalCountrycode,
    local_DestinationTerminalcodefld,
    local_DestinationTerminalUIRR,
    local_DestinationTerminalname,
    local_DestinationTerminalPostalcode,
    local_DestinationTerminalCityname,
    local_DestinationTerminalCountrycode,
    local_TankcontainerNumber,
    local_ISOSizeTypeCode,
    local_Equipmentweight,
    local_Equipmentlength,
    local_interunitCode,
    local_Equipmentwidth,
    local_Equipmentheight,
    local_Equipmentcapacity,
    local_ituType,
    local_EquipmentIsGlycolheating,
    local_EquipmentIsElectricalheating,
    local_MGRemailaddress,
    local_routeId,
    local_routeCodefld,
    local_isintermodal,
    local_BookingAddressId,
    local_generalinstructions,
    local_RouteSelfdrive,
    local_RouteIMO4HazardousGoodsAllowed from ordmodality
      left outer join route
      left outer join orders
      left outer join users on orders.account = users.userid
      join modality on modality.modalityid = route.modalityid
      left outer join equipment on equipment.equipmentid = orders.equipmentid
      left outer join equipmenttype on equipmenttype.equipmenttypeid = equipment.equipmenttypeid
      left outer join fleetserie on fleetserie.fleetserieid = equipment.fleetserieid
      join address as DepartureTerminal on DepartureTerminal.addressid = route.addressfrom
      left outer join place as DepP on DepP.placeid = DepartureTerminal.locationplaceid
      left outer join country as DepC on DepC.countryid = DepP.countryid
      join address as DestinationTerminal on DestinationTerminal.addressid = route.addressto
      left outer join place as DestP on DestP.placeid = DestinationTerminal.locationplaceid
      left outer join country as DestC on DestC.countryid = DestP.countryid
    where ordmodality.ordmodalityid = in_ordmodalityid;
  //als clean op modality aanstaat dan geen gevaarlijke stof else
  //als rail en adrklasse en unnumber gevuld in product
  //als ferry/deepsea en imoclass en unnumber gevuld in product
  if local_clean = 1 then
    set local_IsDangerousGood = 0
  else
  end if;
  if local_isIntermodal = 1 then
    set local_roadCouplingCode = 9
  else
    set local_roadCouplingCode = 1
  end if;
  set local_isFull = 'FALSE';
  if exists(select OrdModalityId from OrdModProduct where OrdModalityId = in_OrdModalityId) then
    set local_isFull = 'TRUE';
    //    select sum(isnull(Loadedweight,Quantity,0)) into local_TotalLoaded from OrdModProduct join ordproduct where OrdModalityId = in_OrdModalityId
    select sum(if isnull(loadedweight,0) > 0 then loadedweight else isnull(quantity,0) endif) into local_TotalLoaded from OrdModProduct join ordproduct where OrdModalityId = in_OrdModalityId
  end if;
  //  set local_shipperid   = (select principalid from orders where orderid=local_orderid);
  //  set local_consigneeid = (select first daddressid from ordproduct where orderid=local_orderid);
  // get data from FIRST ordproduct related to modality
  select first A.Ordproductid,product.description,product.Unnumber,product.ImoClass,product.ProperShippingNameAdditional,(if ThisCompany <> 'ILB' then 1 else 0 endif),isnull(A.loadedweight,A.quantity),A.HarmonizedSystemCode,product.grade,A.deliverytemp,A.Daddressid,A.Laddressid,A.ImporterId,A.ExporterId,A.AddressNotify1Id,A.CustomsStatus,A.CustomsDocumentNumber,A.DocumentExpiryDate into local_firstOrdproductid,local_Fullproductdescription,local_productUnnumber,local_productImoClass,Local_productProperShippingNameAdditional,local_productIsControlledGood,local_productQuantity,Local_ProductHarmonizedSystemCode,local_productGrade,Local_ProductTemperature,local_ProductConsigneeid,local_ProductShipperId,local_ProductImporterId,local_ProductExporterId,local_ProductNotifyId,local_OrdProductCustomsStatus,local_OrdProductCustomsDocumentNumber,local_OrdProductDocumentExpiryDate from OrdModProduct as OMP join ordproduct as A on OMP.ordproductid = A.ordproductid join product where OMP.ordmodalityid = in_ordmodalityid;
  // if no product related to ordmodality find it another way.....
  if local_ProductShipperid is null then
    //    set local_Productshipperid = (select principalid from orders where orderid = local_orderid)
    set local_Productshipperid = (select first daddressid from ordproduct where orderid = local_orderid)
  end if;
  if local_ProductConsigneeid is null then
    //    set local_ProductConsigneeid = (select first daddressid from ordproduct where orderid = local_orderid)
    set local_ProductConsigneeid = (select first laddressid from ordproduct where orderid = local_orderid)
  end if;
  if local_ProductNotifyid is null then
    //    set local_ProductConsigneeid = (select first daddressid from ordproduct where orderid = local_orderid)
    set local_ProductNotifyid = (select first AddressNotify1Id from ordproduct where orderid = local_orderid)
  end if;
  case ThisCompany
  when 'DH' then
    set local_HoursAdd = 3;
    set local_USRemailaddress = (select first emailaddress from users where useridbackend = current user and emailaddress is not null order by userid asc);
    if current user = 'usr' or isnull(local_USRemailaddress,'') = '' then set local_USRemailaddress = local_MGRemailaddress
    end if;
    set local_Senderemail = 'edisupport@denhartogh.com';
    set local_emailaddressRejected = 'edisupport@denhartogh.com';
    set Local_SenderRegistrationCode = 'DHRU';
    set local_vgmResponsible = 'DEN HARTOGH';
    case local_InvoiceDepartment
    when 37 then
      set Local_Contractcode = 'HARTGAS';
      set local_ShipperName = 'DEN HARTOGH GAS LOGISTICS B.V.';
      set local_ShipperCountry = 'NL';
      set local_ShipperPlace = 'Rotterdam';
      set local_ConsigneeName = 'DEN HARTOGH GAS LOGISTICS B.V.';
      set local_ConsigneeVat = 'NL001288921B01'
    when 38 then
      set Local_Contractcode = 'DENHARTG';
      set local_ShipperName = 'DEN HARTOGH GLOBAL B.V.';
      set local_ShipperCountry = 'NL';
      set local_ShipperPlace = 'Rotterdam';
      set local_ConsigneeName = 'DEN HARTOGH GLOBAL B.V.';
      set local_ConsigneeVat = 'NL813402335B01'
    else
      set Local_Contractcode = 'HARTOGLO';
      set local_ShipperName = 'DEN HARTOGH LIQUID LOGISTICS B.V.';
      set local_ShipperCountry = 'NL';
      set local_ConsigneeName = 'DEN HARTOGH LIQUID LOGISTICS B.V.';
      set local_ShipperPlace = 'Rotterdam';
      set local_ConsigneeVat = 'NL001288908B01'
    end case;
    set Local_SenderPrefix = 'HARTOG';
    set Local_OperatorName = 'CWEB';
    set Local_Map = 'ToSeeburgerCloud'
  when 'HS' then
    set local_HoursAdd = 0;
    set local_USRemailaddress = (select first emailaddress from users where useridbackend = current user and emailaddress is not null order by userid asc);
    if current user = 'usr' or isnull(local_USRemailaddress,'') = '' then set local_USRemailaddress = local_MGRemailaddress
    end if;
    set Local_SenderEmail = 'edisupport@hs-group.eu';
    set local_emailaddressRejected = 'edisupport@hs-group.eu';
    set Local_SenderRegistrationCode = 'HSFOODOS';
    set local_vgmResponsible = 'U.HEMPENIUS';
    set Local_Contractcode = 'HSFOODOS';
    set local_ShipperName = 'H&S FOODTRANS BV';
    set local_ShipperCountry = 'NL';
    set local_ShipperPlace = 'Barneveld';
    set local_ConsigneeName = 'H&S FOODTRANS BV';
    set local_ConsigneeVat = 'NL810785201B01';
    set Local_SenderPrefix = 'HS';
    set Local_OperatorName = 'CWEB';
    set Local_Map = 'ToSeeburgerCloud'
  //    set Local_Map = 'Cobelfret\\Book'
  when 'HAE' then
    set local_HoursAdd = 0;
    set local_USRemailaddress = (select first emailaddress from users where useridbackend = current user and emailaddress is not null order by userid asc);
    if current user = 'usr' or isnull(local_USRemailaddress,'') = '' then
      set local_USRemailaddress = local_MGRemailaddress
    end if;
    set Local_SenderEmail = 'mdemaeyer@haesaerts.be';
    set local_emailaddressRejected = 'mdemaeyer@haesaerts.be';
    set Local_SenderRegistrationCode = 'HAE';
    set local_vgmResponsible = 'HAESAERTS INTERMODAL';
    set Local_Contractcode = 'HAE';
    set local_ShipperName = 'Haesaerts Intermodal';
    set local_ShipperCountry = 'BE';
    set local_ConsigneeName = 'Haesaerts Intermodal';
    set local_ConsigneeVat = '';
    set local_ShipperPlace = 'Breendonk';
    set Local_SenderPrefix = 'HAE';
    set Local_OperatorName = 'CWEB';
    set Local_Map = 'Cobelfret\\Book'
  when 'ILB' then
    set local_HoursAdd = 0;
    set local_USRemailaddress = (select first emailaddress from users where useridbackend = current user and emailaddress is not null order by userid asc);
    if current user = 'usr' or isnull(local_USRemailaddress,'') = '' then
      set local_USRemailaddress = local_MGRemailaddress
    end if;
    set Local_SenderEmail = 'bulk@ilbenelux.com';
    set local_emailaddressRejected = 'bulk@ilbenelux.com';
    set Local_SenderRegistrationCode = 'ILBINTER';
    set local_vgmResponsible = 'ILB LOGISTICS';
    set Local_Contractcode = 'ILBINTER';
    set local_ShipperName = 'ILB Logistics';
    set local_ShipperCountry = 'NL';
    set local_ShipperPlace = 'Antwerpen';
    set local_ConsigneeName = 'ILB Logistics';
    set local_ConsigneeVat = 'BE0874963358';
    set Local_SenderPrefix = 'ILBINTER';
    set Local_OperatorName = 'CWEB';
    //    set Local_Map = 'Cobelfret\\Book'
    if @@servername = 'transfusion' then
      set Local_Map = '\\XMLoutbox\\Cobelfret\\Book\\'
    else
      set Local_Map = '\\XMLoutboxTest\\Cobelfret\\Book\\'
    end if
  else
    return 0
  end case;
  case in_type when 'BOOK' then
    set local_BookingStatus = '10';
    set local_ordmodalityStatus = 1 when 'CANCEL' then
    set local_BookingStatus = '11';
    set local_ordmodalityStatus = 0
  end case;
  if local_TankcontainerNumber is null then
    set local_interunitCode = '24';
    set local_Equipmentlength = 782;
    set local_ituType = 'I';
    set local_grossWeight = '35000';
    set local_Equipmentweight = '4980';
    set local_TankcontainerNumber = in_ordmodalityid
  end if;
  select address.Name,Address.LocationAddress,place.name,country.codefld
    into local_Productshippername,local_ProductShipperAddress,local_ProductShipperPlace,local_ProductShipperCountry
    from Address
      left outer join place on placeid = address.locationplaceid
      left outer join country on country.countryid = place.countryid
    where address.addressid = local_Productshipperid;
  select address.Name,Address.LocationAddress,place.name,country.codefld
    into local_Productconsigneename,local_ProductconsigneeAddress,local_ProductconsigneePlace,local_ProductconsigneeCountry
    from Address
      left outer join place on placeid = address.locationplaceid
      left outer join country on country.countryid = place.countryid
    where address.addressid = local_Productconsigneeid;
  select address.Name,TRIM(address.EORInr),address.VATNumber,Address.LocationAddress,place.name,country.codefld
    into local_ProductNotifyname,local_ProductNotifyEORInr,local_ProductNotifyVATNumber,local_ProductNotifyAddress,local_ProductNotifyPlace,local_ProductNotifyCountry
    from Address
      left outer join place on placeid = address.locationplaceid
      left outer join country on country.countryid = place.countryid
    where address.addressid = local_ProductNotifyid;
  select address.Name,TRIM(address.EORInr),address.VATNumber,Address.LocationAddress,1,place.name,country.codefld
    into local_ProductImportername,local_ProductImporterEORInr,local_ProductImporterVATNumber,local_ProductImporterAddress,local_ProductImporterTSPPermit,local_ProductImporterPlace,local_ProductImporterCountry
    from Address
      left outer join place on placeid = address.locationplaceid
      left outer join country on country.countryid = place.countryid
    where address.addressid = local_ProductImporterid;
  select address.Name,TRIM(address.EORInr),address.VATNumber,Address.LocationAddress,place.name,country.codefld
    into local_ProductExportername,local_ProductExporterEORInr,local_ProductExporterVATNumber,local_ProductExporterAddress,local_ProductExporterPlace,local_ProductExporterCountry
    from Address
      left outer join place on placeid = address.locationplaceid
      left outer join country on country.countryid = place.countryid
    where address.addressid = local_ProductExporterid;
  //  MESSAGE 'consignee:' || local_Productshippername || '**' || local_ProductShipperPlace type info to client;
  //  MESSAGE 'consignee:' || local_Productconsigneename || '**' || local_ProductconsigneePlace type info to client;
  set local_grossWeight = isnull(local_Equipmentweight,0)+isnull(local_TotalLoaded,0);
  if local_Equipmentweight is null or local_Equipmentlength is null then
    raiserror 99999 'Tankcontainer weight, lenght, width and/or height empty (check Equiment)';
    return 0
  end if;
  // set Local_SenderTransportId      = 'sender';
  // set Local_ReceiverTransportId    = 'receiver';
  //  if IsUnaccompanied = '0' then 
  //     return 0 
  //  end if;
  //  message 'Booking 1...' || current time type info to client;
  set XMLfile = xmlfile+'<?xml version="1.0" encoding="UTF-8"?>';
  set XMLfile = xmlfile+'<cwebBookingRequest version="1.0">';
  set XMLfile = xmlfile+'   <header>';
  set XMLfile = xmlfile+'      <senderID>' || Local_SenderRegistrationCode || '</senderID>';
  set XMLfile = xmlfile+'      <receiverID>' || Local_OperatorName || '</receiverID>';
  set XMLfile = xmlfile+'      <replyMail replyEvent="rejected,transferred,approved,invalid">' || local_emailaddressRejected || ';' || local_USRemailaddress || '</replyMail>';
  //  set XMLfile=xmlfile+'      <replyMail replyEvent="transferred">' || local_emailaddressRejected || ';' || local_USRemailaddress || '</replyMail>';
  //  set XMLfile=xmlfile+'      <replyMail replyEvent="approved">' || local_emailaddressRejected || ';' || local_USRemailaddress || '</replyMail>';
  set XMLfile = xmlfile+'      <msgDate>' || DATEFORMAT(current time,'yyyymmdd') || '</msgDate>';
  set XMLfile = xmlfile+'      <msgTime>' || DATEFORMAT(current time,'hh:nn:ss') || '</msgTime>';
  set XMLfile = xmlfile+'      <msgReference>' || in_ordmodalityid || '.' || local_ediCounter || '</msgReference>';
  set XMLfile = xmlfile+'   </header>';
  if in_type = 'BOOK' then
    if local_edicounter = 1 then
      set XMLfile = xmlfile+'      <create>'
    else
      set XMLfile = xmlfile+'      <modify>'
    end if
  else set XMLfile = xmlfile+'      <cancel>'
  end if;
  if in_type = 'CANCEL' then
    set XMLfile = xmlfile+'         <cancelBookingRequestBody>';
    set XMLfile = xmlfile+'            <clientReference>' || local_orderid || '</clientReference>';
    set XMLfile = xmlfile+'            <shippingLineReference>' || local_BookingReference || '</shippingLineReference>';
    set XMLfile = xmlfile+'            <transportOrderReference>' || in_ordmodalityid || '</transportOrderReference>';
    set XMLfile = xmlfile+'         </cancelBookingRequestBody>'
  else
    set XMLfile = xmlfile+'         <bookingRequestBody>';
    set XMLfile = xmlfile+'            <clientReference>' || local_orderid || '</clientReference>';
    if local_edicounter = 1 then
      set XMLfile = xmlfile+'            <shippingLineReference>' || '' || '</shippingLineReference>'
    else
      set XMLfile = xmlfile+'            <shippingLineReference>' || local_BookingReference || '</shippingLineReference>'
    end if;
    set XMLfile = xmlfile+'            <transportOrderReference>' || in_ordmodalityid || '</transportOrderReference>';
    set XMLfile = xmlfile+'            <contractCode>' || Local_Contractcode || '</contractCode>';
    //    set XMLfile = xmlfile+'            <routeCode>' || substr(local_routeCodefld,1,6) || '</routeCode>';
    set XMLfile = xmlfile+'            <routeCode>' || inf_getfield(local_routeCodefld,1,'-') || '</routeCode>';
    //      set XMLfile=xmlfile+'            <shipsOptionInd>' || local_orderid || '</shipsOptionInd>';
    set XMLfile = xmlfile+'            <sailingDate>' || DATEFORMAT(local_dateOriginal,'yyyymmdd') || '</sailingDate>';
    set XMLfile = xmlfile+'            <sailingTime>' || DATEFORMAT(local_TIMEOriginal,'hh:mm') || '</sailingTime>';
    //      set XMLfile=xmlfile+'            <equipmentCode>'  || local_interunitCode || '</equipmentCode>';
    // force selfdrive
    if local_RouteSelfdrive = 1 then
      set local_interunitCode = 'T'
    end if;
    case local_interunitCode
    when 'C' then
      set XMLfile = xmlfile+'            <equipmentCode>' || 'TKCT' || '</equipmentCode>'
    when 'T' then
      set XMLfile = xmlfile+'            <equipmentCode>' || 'SELF' || '</equipmentCode>'
    when 'R' then
      set XMLfile = xmlfile+'            <equipmentCode>' || 'TKTR' || '</equipmentCode>'
    when 'G' then // other - hard coded equipmenttype GRH21
      set XMLfile = xmlfile+'            <equipmentCode>' || 'TRLR' || '</equipmentCode>'
    else
      set XMLfile = xmlfile+'            <equipmentCode>' || 'TKCT' || '</equipmentCode>'
    end case;
    set XMLfile = xmlfile+'            <unitNumber>' || local_TankcontainerNumber || '</unitNumber>';
    set XMLfile = xmlfile+'            <fullInd>' || local_isFull || '</fullInd>';
    // NOV 25 2020 mail jacco
    if local_isFull = 'TRUE' and(local_DepartureTerminalCountrycode = 'GB' or local_DestinationTerminalCountrycode = 'GB') then
      if local_DestinationTerminalCountrycode = 'GB' then
        if local_OrdProductCustomsStatus in( 'T1','T2' ) then
          set XMLfile = xmlfile+'            <ukBorderProcess >' || 'M' || '</ukBorderProcess >'
        else
          set XMLfile = xmlfile+'            <ukBorderProcess >' || 'T' || '</ukBorderProcess >'
        end if
      else set XMLfile = xmlfile+'            <ukBorderProcess >' || 'T' || '</ukBorderProcess >';
        set XMLfile = xmlfile+'            <customsStatus>' || '' || '</customsStatus>'
      end if
    else set XMLfile = xmlfile+'            <ukBorderProcess >' || '' || '</ukBorderProcess >';
      set XMLfile = xmlfile+'            <customsStatus>' || 'C' || '</customsStatus>'
    end if;
    set XMLfile = xmlfile+'            <goodDescription>' || CreateXMLstring(local_Fullproductdescription) || '</goodDescription>';
    set XMLfile = xmlfile+'            <measurementInfo>';
    set XMLfile = xmlfile+'               <length>' || local_Equipmentlength || '</length>';
    set XMLfile = xmlfile+'               <tareWeight>' || local_Equipmentweight || '</tareWeight>';
    set XMLfile = xmlfile+'               <cargoWeight>' || (if local_TotalLoaded > 0 then local_TotalLoaded endif) || '</cargoWeight>';
    if local_RouteIMO4HazardousGoodsAllowed = 0 and local_isFull = 'TRUE' then
      set XMLfile = xmlfile+'               <vgm>' || local_Equipmentweight+local_TotalLoaded || '</vgm>';
      //      set XMLfile = xmlfile+'               <vgm>' || local_Equipmentweight+(select sum(isnull(Loadedweight,Quantity,0)) from ordproduct where OrderId = local_orderid) || '</vgm>';
      set XMLfile = xmlfile+'               <vgmResponsible>' || CreateXMLstring(local_vgmResponsible) || '</vgmResponsible>'
    end if;
    set XMLfile = xmlfile+'            </measurementInfo>';
    if local_isFull = 'TRUE' and(local_DepartureTerminalCountrycode = 'GB' or local_DestinationTerminalCountrycode = 'GB') then
      set XMLfile = xmlfile+'            <consignmentInfo>';
      set XMLfile = xmlfile+'                   <shippingLineReference/>';
      //      set "XMLfile" = "xmlfile"+'                   <cstmEntryProcTp>' || (if "local_ProductImporterTSPPermit" = '1' then 'P' else '1' endif) || '</cstmEntryProcTp>';
      set XMLfile = xmlfile+'                   <consignConfInd>' || 'TRUE' || '</consignConfInd>';
      set XMLfile = xmlfile+'                   <consignmentList>';
      set XMLfile = xmlfile+'                         <consignment>';
      set XMLfile = xmlfile+'                                <consignmentId>' || local_firstOrdproductid || '</consignmentId>';
      set XMLfile = xmlfile+'                                <typeOfGoods>' || (if local_productIsControlledGood = 1 then 'C' else 'S' endif) || '</typeOfGoods>';
      if local_ProductImporterid is not null then
        set XMLfile = xmlfile+'                                <consignee>';
        set XMLfile = xmlfile+'                                       <name>' || CreateXMLstring(local_ProductImporterName) || '</name>';
        if isnull(local_ProductImporterEORInr,'') = '' then
          set XMLfile = xmlfile+'                                       <address>' || CreateXMLstring(local_ProductImporterAddress) || '</address>';
          set XMLfile = xmlfile+'                                       <city>' || CreateXMLstring(local_ProductImporterPlace) || '</city>';
          set XMLfile = xmlfile+'                                       <postal>' || CreateXMLstring(local_ProductImporterName) || '</postal>';
          set XMLfile = xmlfile+'                                       <country>' || CreateXMLstring(local_ProductImporterCountry) || '</country>';
          set XMLfile = xmlfile+'                                       <vat>' || CreateXMLstring(local_ProductImporterVatNumber) || '</vat>'
        else
          set XMLfile = xmlfile+'                                       <eori>' || CreateXMLstring(local_ProductImporterEORInr) || '</eori>'
        end if;
        set XMLfile = xmlfile+'                                </consignee>'
      end if;
      if local_ProductExporterid is not null then
        set XMLfile = xmlfile+'                                <consignor>';
        set XMLfile = xmlfile+'                                       <name>' || CreateXMLstring(local_ProductExporterName) || '</name>';
        if isnull(local_ProductExporterEORInr,'') = '' then
          set XMLfile = xmlfile+'                                       <address>' || CreateXMLstring(local_ProductExporterAddress) || '</address>';
          set XMLfile = xmlfile+'                                       <city>' || CreateXMLstring(local_ProductExporterPlace) || '</city>';
          set XMLfile = xmlfile+'                                       <postal>' || CreateXMLstring(local_ProductExporterName) || '</postal>';
          set XMLfile = xmlfile+'                                       <country>' || CreateXMLstring(local_ProductExporterCountry) || '</country>';
          set XMLfile = xmlfile+'                                       <vat>' || CreateXMLstring(local_ProductExporterVatNumber) || '</vat>'
        else
          set XMLfile = xmlfile+'                                       <eori>' || CreateXMLstring(local_ProductExporterEORInr) || '</eori>'
        end if;
        set XMLfile = xmlfile+'                                </consignor>'
      end if;
      if local_ProductNotifyid is not null then
        set XMLfile = xmlfile+'                                <notify>';
        set XMLfile = xmlfile+'                                       <name>' || CreateXMLstring(local_ProductNotifyName) || '</name>';
        if isnull(local_ProductNotifyEORInr,'') = '' then
          set XMLfile = xmlfile+'                                       <address>' || CreateXMLstring(local_ProductNotifyAddress) || '</address>';
          set XMLfile = xmlfile+'                                       <city>' || CreateXMLstring(local_ProductNotifyPlace) || '</city>';
          set XMLfile = xmlfile+'                                       <postal>' || CreateXMLstring(local_ProductNotifyName) || '</postal>';
          set XMLfile = xmlfile+'                                       <country>' || CreateXMLstring(local_ProductNotifyCountry) || '</country>';
          set XMLfile = xmlfile+'                                       <vat>' || CreateXMLstring(local_ProductNotifyVatNumber) || '</vat>'
        else
          set XMLfile = xmlfile+'                                       <eori>' || CreateXMLstring(local_ProductNotifyEORInr) || '</eori>'
        end if;
        set XMLfile = xmlfile+'                                </notify>'
      end if;
      //    set XMLfile = xmlfile+'                                <consignedPlace>FRVOR</consignedPlace>';
      //    set XMLfile = xmlfile+'                                <receiptPlace>GBRDD</receiptPlace>';
      set XMLfile = xmlfile+'                                <documentType>' || (if local_OrdProductCustomsStatus = 'EX 1' then 'EX' else local_OrdProductCustomsStatus endif) || '</documentType>';
      set XMLfile = xmlfile+'                                <documentNr>' || local_OrdProductCustomsDocumentNumber || '</documentNr>';
      set XMLfile = xmlfile+'                                <expiryDate>' || DATEFORMAT(local_OrdProductDocumentExpiryDate,'yyyymmdd') || '</expiryDate>';
      set XMLfile = xmlfile+'                                <validTo>' || '' || '</validTo>';
      set XMLfile = xmlfile+'                                <itemList>';
      set XMLfile = xmlfile+'                                       <item>';
      //    set XMLfile = xmlfile+'                                              <shippingMarks/>';
      set XMLfile = xmlfile+'                                              <numberOfPackages>1</numberOfPackages>';
      set XMLfile = xmlfile+'                                              <packageCode>' || (if local_productGrade = '2' then 'VG' else 'VL' endif) || '</packageCode>';
      if isnull(local_ProductHarmonizedSystemCode,'0') = '0' or len(local_ProductHarmonizedSystemCode) < 6 then
        set XMLfile = xmlfile+'                                              <commodityCode>' || '00000000' || '</commodityCode>'
      else
        set XMLfile = xmlfile+'                                              <commodityCode>' || CreateXMLstring(local_ProductHarmonizedSystemCode) || '</commodityCode>'
      end if;
      set XMLfile = xmlfile+'                                              <description>' || CreateXMLstring(local_Fullproductdescription) || '</description>';
      set XMLfile = xmlfile+'                                              <grossWeight>' || local_productQuantity || '</grossWeight>';
      set XMLfile = xmlfile+'                                              <nettoWeight>' || local_productQuantity || '</nettoWeight>';
      set XMLfile = xmlfile+'                                              <unNumber>' || local_productUnnumber || '</unNumber>';
      set XMLfile = xmlfile+'                                              <primaryClass>' || local_productImoClass || '</primaryClass>';
      set XMLfile = xmlfile+'                                              <technicalName>' || CreateXMLstring(Local_productProperShippingNameAdditional) || '</technicalName>';
      set XMLfile = xmlfile+'                                       </item>';
      set XMLfile = xmlfile+'                                </itemList>';
      set XMLfile = xmlfile+'                         </consignment>';
      set XMLfile = xmlfile+'                   </consignmentList>';
      set XMLfile = xmlfile+'            </consignmentInfo>'
    end if;
    if local_isRouteLOFO = 1 then
      set XMLfile = xmlfile+'               <lofoInd>' || 'TRUE' || '</lofoInd>'
    end if;
    //  message 'Booking 2...' || current time type info to client;
    if local_isFull = 'FALSE' and isnull(local_clean,0) = 0 then
      set local_IsemptyUncleaned = 'TRUE'
    else
      set local_IsemptyUncleaned = 'FALSE'
    end if;
    select count(distinct ordproduct.ordproductid)
      into local_lineCount from ordmodproduct
        join ordproduct
        join product on product.productid = OrdProduct.ProductId
        left outer join ordmodality on ordmodproduct.ordmodalityid = ordmodality.ordmodalityid
      where ordproduct.orderid = local_orderid and ordmodality.browsesequence <= local_browsesequence and isnull(product.imoclass,'') <> '';
    set local_lineNumber = 0;
    open Cursor_Products;
    LineLoop: loop
      fetch next Cursor_Products into local_ordproductid;
      if sqlstate = err_notfound then
        leave LineLoop
      end if;
      select product.grade,product.description,product.Unnumber,product.AdrClass,product.ImoClass,(if isnull(getoption('Inforit_EnableGDSData'),0) = 1 then product.ProperShippingNameFixed else product.ProperShippingName endif),product.ProperShippingNameAdditional,product.PackagingGroup,product.isNAFlashPoint,product.FlashPoint,product.MarinePolution,product.EmSNo,isnull(Loadedweight,Quantity,0),
        isnull(IsplugAtPortOfLoadingCode,0),isnull(IsplugOnVesselCode,0),isnull(IsplugAtPortOfDischargeCode,0) into local_productGrade,local_productdescription,
        local_productUnnumber,local_productAdrClass,local_productImoClass,Local_productProperShippingNameFixed,Local_productProperShippingNameAdditional,local_productPackagingGroup,local_productisNAFlashPoint,local_productFlashPoint,local_productMarinePolution,local_productEmSNo,local_productQuantity,
        local_IsplugAtPortOfLoadingCode,local_IsplugOnVesselCode,
        local_IsplugAtPortOfDischargeCode from ordproduct as A join product where A.ordproductid = local_ordproductid;
      if isnull(local_ProductUnnumber,'') <> '' then
        if isnull(local_clean,0) = 0 and not(local_ProductUnnumber in( '3256','3257' ) and local_isfull = 'FALSE') then // DG were loaded but not present anymore and tank is NOT cleaned 
          set local_lineNumber = local_lineNumber+1;
          // this line must be added only once!! 
          // check the endtag as well!!
          set local_productImoClass = (select rtrim(Inf_GetField(local_productImoClass,1,'(')));
          if local_lineNumber = 1 then
            set XMLfile = xmlfile+'            <imdgInfo count="' || local_lineCount || '">'
          end if;
          //          set "XMLfile" = "xmlfile"+'               <un>' || "local_ProductUnnumber" || '</un>';
          //          set "XMLfile" = "xmlfile"+'               <clean>' || "local_clean" || '</clean>';
          //          set "XMLfile" = "xmlfile"+'               <full>' || "local_isfull" || '</full>';
          set XMLfile = xmlfile+'               <dgd sequence="' || local_lineNumber || '">';
          set XMLfile = xmlfile+'                  <partyInfo>';
          set XMLfile = xmlfile+'                     <partyCode>SHIPPER</partyCode>';
          set XMLfile = xmlfile+'                     <name>' || CreateXMLstring(local_ProductShipperName) || '</name>';
          set XMLfile = xmlfile+'                     <address>' || CreateXMLstring(local_ProductShipperAddress) || '</address>';
          set XMLfile = xmlfile+'                     <countryCode>' || CreateXMLstring(local_ProductShipperCountry) || '</countryCode>';
          set XMLfile = xmlfile+'                     <city>' || CreateXMLstring(local_ProductShipperPlace) || '</city>';
          set XMLfile = xmlfile+'                  </partyInfo>';
          set XMLfile = xmlfile+'                  <partyInfo>';
          set XMLfile = xmlfile+'                     <partyCode>CONSIGNEE</partyCode>';
          set XMLfile = xmlfile+'                     <name>' || CreateXMLstring(local_ProductConsigneeName) || '</name>';
          set XMLfile = xmlfile+'                     <address>' || CreateXMLstring(local_ProductConsigneeAddress) || '</address>';
          set XMLfile = xmlfile+'                     <countryCode>' || CreateXMLstring(local_ProductConsigneeCountry) || '</countryCode>';
          set XMLfile = xmlfile+'                     <city>' || CreateXMLstring(local_ProductConsigneePlace) || '</city>';
          set XMLfile = xmlfile+'                  </partyInfo>';
          set XMLfile = xmlfile+'                  <product>';
          set XMLfile = xmlfile+'                     <unNumber>' || local_ProductUnnumber || '</unNumber>';
          set XMLfile = xmlfile+'                     <properShippingName>' || CreateXMLstring(Local_productProperShippingNameFixed) || '</properShippingName>';
          set XMLfile = xmlfile+'                     <technicalName>' || CreateXMLstring(Local_productProperShippingNameAdditional) || '</technicalName>';
          set XMLfile = xmlfile+'                     <primaryClass>' || local_productImoClass || '</primaryClass>';
          set XMLfile = xmlfile+'                     <packingGroup>' || (case local_productPackagingGroup when '1' then 'I' when '2' then 'II' when 3 then 'III' else '' end) || '</packingGroup>';
          set XMLfile = xmlfile+'                     <marinePollutantInd>' || (if local_productMarinePolution = 'Y' then 'TRUE' else 'FALSE' endif) || '</marinePollutantInd>';
          set XMLfile = xmlfile+'                     <flashpoint>' || (if isnull(local_productisNAFlashPoint,0) = 1 then null else local_productFlashPoint endif) || '</flashpoint>';
          set XMLfile = xmlfile+'                     <ems>' || local_productEmSNo || '</ems>';
          set XMLfile = xmlfile+'                     <numberOfPackages>1</numberOfPackages>';
          set XMLfile = xmlfile+'                     <packageCode>' || (if local_productGrade = '2' then 'VG' else 'VL' endif) || '</packageCode>';
          set XMLfile = xmlfile+'                     <weight>' || (if local_isFull = 'TRUE' then local_productQuantity else 0 endif) || '</weight>';
          set XMLfile = xmlfile+'                     <limitedQuantityInd>FALSE</limitedQuantityInd>';
          set XMLfile = xmlfile+'                     <emptyUncleanedInd>' || local_IsemptyUncleaned || '</emptyUncleanedInd>';
          //          if isnull(local_clean,0) = 1 then // DG were loaded but not present anymore and tank is cleaned
          //            set XMLfile = xmlfile+'                     <description>' || 'EMPTY UNCLEAN of ' || CreateXMLstring(local_productdescription) || '</description>'
          //          else
          //            set XMLfile = xmlfile+'                     <description>' || CreateXMLstring(local_hazardousdescription) || '</description>'
          //          end if;
          set XMLfile = xmlfile+'                  </product>';
          if local_IsemptyUncleaned = 'TRUE' then
            set XMLfile = xmlfile+'                  <declaration type="DGD">';
            set XMLfile = xmlfile+'                     <companyName>' || (select first CreateXMLstring(address.name) from department join address on defaultaddressid = addressid where department.departmentid = local_InvoiceDepartment order by department.DepartmentId asc) || '</companyName>';
            set XMLfile = xmlfile+'                     <declarantName>' || (select first CreateXMLstring(name) from users where useridbackend = current user order by userid asc) || '</declarantName>';
            set XMLfile = xmlfile+'                     <declarantStatus>' || (select first CreateXMLstring(users.position) from users where useridbackend = current user order by userid asc) || '</declarantStatus>';
            set XMLfile = xmlfile+'                     <place>' || CreateXMLstring(local_ShipperPlace) || '</place>';
            set XMLfile = xmlfile+'                     <date>' || DATEFORMAT(current time,'yyyymmdd') || '</date>';
            set XMLfile = xmlfile+'                     <signature>' || (select first ucase(name) from users where useridbackend = current user order by userid asc) || '</signature>';
            set XMLfile = xmlfile+'                  </declaration>'
          end if;
          set XMLfile = xmlfile+'               </dgd>'
        // whenever lines are added this endtag mist be added also!
        end if
      end if
    end loop LineLoop;
    if local_lineNumber > 0 then
      set XMLfile = xmlfile+'            </imdgInfo>'
    end if;
    close Cursor_Products;
    select first ordproduct.LiquidThermostatSetting
      into local_LiquidThermostatSetting from ordmodproduct join ordproduct left outer join ordmodality on ordmodproduct.ordmodalityid = ordmodality.ordmodalityid
      where ordmodproduct.ordmodalityid = in_OrdModalityId and isnull(ordproduct.LiquidThermostatSetting,0) <> 0;
    if local_isFull = 'TRUE' or local_LiquidThermostatSetting is not null then
      if local_IsplugOnVesselCode = 1 then // 5 apr 2012 jacco B
        set XMLfile = xmlfile+'            <temperatureInfo>';
        set XMLfile = xmlfile+'               <temperature>' || local_LiquidThermostatSetting || '</temperature>';
        set XMLfile = xmlfile+'               <mintemperature>' || (if isnull(local_LiquidThermostatSetting,0) > 0 then local_LiquidThermostatSetting-10 endif) || '</mintemperature>';
        set XMLfile = xmlfile+'               <maxtemperature>' || (if isnull(local_LiquidThermostatSetting,0) > 0 then local_LiquidThermostatSetting+10 endif) || '</maxtemperature>';
        set XMLfile = xmlfile+'               <plugAtPortOfLoadingCode>' || (if local_IsplugAtPortOfLoadingCode = 1 then 'Y' else 'N' endif) || '</plugAtPortOfLoadingCode>';
        set XMLfile = xmlfile+'               <plugOnVesselCode>' || (if local_IsplugOnVesselCode = 1 then 'Y' else 'N' endif) || '</plugOnVesselCode>';
        set XMLfile = xmlfile+'               <plugAtPortOfDischargeCode>' || (if local_IsplugAtPortOfDischargeCode = 1 then 'Y' else 'N' endif) || '</plugAtPortOfDischargeCode>';
        set XMLfile = xmlfile+'            </temperatureInfo>'
      end if end if;
    set XMLfile = xmlfile+'            <remarks>' || '' || '</remarks>';
    set XMLfile = xmlfile+'            <adminRemarks>' || createXMLstring(local_instructionGeneral) || '</adminRemarks>';
    //    set XMLfile = xmlfile+'            <partyInfo>';
    //    set XMLfile = xmlfile+'               <partyCode>SHIPPER</partyCode>';
    //    set XMLfile = xmlfile+'               <name>' || CreateXMLstring(local_ShipperName) || '</name>';
    //    set XMLfile = xmlfile+'               <countryCode>' || CreateXMLstring(local_ShipperCountry) || '</countryCode>';
    //    set XMLfile = xmlfile+'            </partyInfo>';
    set XMLfile = xmlfile+'            <partyInfo>';
    set XMLfile = xmlfile+'               <partyCode>CONSIGNEE</partyCode>';
    set XMLfile = xmlfile+'                 <name>' || CreateXMLstring(local_ConsigneeName) || '</name>';
    set XMLfile = xmlfile+'                 <vat>' || CreateXMLstring(local_ConsigneeVat) || '</vat>';
    set XMLfile = xmlfile+'            </partyInfo>';
    set XMLfile = xmlfile+'         </bookingRequestBody>'
  end if;
  if in_type = 'BOOK' then
    if Local_edicounter = 1 then
      set XMLfile = xmlfile+'      </create>'
    else
      set XMLfile = xmlfile+'      </modify>'
    end if
  else set XMLfile = xmlfile+'      </cancel>'
  end if;
  set XMLfile = xmlfile+'</cwebBookingRequest>';
  //  set ToFilename='''' || '\\XMLoutbox\\' || Local_Map || '\\' || Local_SenderPrefix || '.IMP.S' || local_BookingStatus || '.' || DATEFORMAT(current time,'yymmdd') || '.' || in_ordmodalityid || local_ediCounter || '.xml' || '''';
  // the filename is given by hupac 
  // if 2 2 files are generated within a second the first will be overwritten
  // no problem if changes but to prvent that the second XML overwrites the very first and thus New booking i wait for 1 second.
  // 
  //  message 'Booking 6...' || current time type info to client;
  if local_Seconds < 2 and local_edicounter = 2 then
    waitfor delay '00:00:01'
  end if;
  // set Local_Map = 'Cobelfret';
  set ToFilename = '''' || Local_Map || Local_SenderPrefix || '.IMP.S' || local_BookingStatus || '.' || substr(local_orderid,-1,-6) || '.' || DATEFORMAT(current time,'hhnnss') || '.xml' || '''';
  //  set ToFilename = '''' || '\\XMLoutboxTest\\' || Local_Map || '\\' || Local_SenderPrefix || '.IMP.' || substr(local_orderid,-1,-6) || '.' || DATEFORMAT(current time,'hhnnss') || '.xml' || '''';
  execute immediate ' unload select xmlfile to ' || ToFileName || ' format ascii escapes off quotes off';
  message 'local_ordmodalityStatus:' || local_ordmodalityStatus type info to client;
  if @@error = 0 then
    case in_type
    when 'BOOK' then
      message 'edicounter:' || (select ediCounter from ordmodality where ordmodalityid = in_ordmodalityid) type info to client;
      update ordmodality set ediCounter = (isnull(ediCounter,0)+1),status = local_ordmodalityStatus where ordmodalityid = in_ordmodalityid;
      //      update ordmodality set ediCounter = 2   where ordmodalityid = in_ordmodalityid;
      set returnvalue = local_ediCounter;
      message 'returnvalue:' || returnvalue type info to client
    when 'CANCEL' then
      update ordmodality
        set ediCounter = 0,status = local_ordmodalityStatus
        where ordmodalityid = in_ordmodalityid;
      set returnvalue = local_ediCounter
    end case
  end if;
  message 'uit' type info to client;
  return returnvalue
end