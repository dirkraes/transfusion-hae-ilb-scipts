ALTER TRIGGER "ClearIDsOrdProduct" before insert,update order 3 on USR.OrdProduct
referencing old as newOrdProduct new as newOrdProduct
for each row /*
Modified FV 03-09-2009 Added new fields Consignee, Notify1 .. 3, BillOfLading
FV 04-09-2013 Added new fields ObjectLWithHoseId1, ... ObjectDWithHoseId2
MvR JAN 2015 added AddressUNStatuscodeId
*/
begin
  if newOrdProduct.LAddressId = 0 then
    set newOrdProduct.LAddressId = null
  end if;
  if newOrdProduct.DAddressId = 0 then
    set newOrdProduct.DAddressId = null
  end if;
  if newOrdProduct.CustomerId = 0 then
    set newOrdProduct.CustomerId = null
  end if;
  if newOrdProduct.ExporterId = 0 then
    set newOrdProduct.ExporterId = null
  end if;
  if newOrdProduct.ImporterId = 0 then
    set newOrdProduct.ImporterId = null
  end if;
  if newOrdProduct.ExportClrId = 0 then
    set newOrdProduct.ExportClrId = null
  end if;
  if newOrdProduct.ImportClrId = 0 then
    set newOrdProduct.ImportClrId = null
  end if;
  if newOrdProduct.LTimeLatest = '00:00:00' then
    set newOrdProduct.LTimeLatest = null
  end if;
  if newOrdProduct.DTimeLatest = '00:00:00' then
    set newOrdProduct.DTimeLatest = null
  end if;
  if newOrdProduct.TimeLoaded = '00:00:00' then
    set newOrdProduct.TimeLoaded = null
  end if;
  if newOrdProduct.LoadingTime = '00:00:00' then
    set newOrdProduct.LoadingTime = null
  else
    set newOrdProduct.LoadingTime = dateformat(newOrdProduct.LoadingTime,'hh:nn:ss')
  end if;
  if newOrdProduct.TimeDelivered = '00:00:00' then
    set newOrdProduct.TimeDelivered = null
  end if;
  if newOrdProduct.DeliveryTime = '00:00:00' then
    set newOrdProduct.DeliveryTime = null
  else
    set newOrdProduct.DeliveryTime = dateformat(newOrdProduct.DeliveryTime,'hh:nn:ss')
  end if;
  if newOrdProduct.LoadedBy = 0 then
    set newOrdProduct.LoadedBy = null
  end if;
  if newOrdProduct.DeliveredBy = 0 then
    set newOrdProduct.DeliveredBy = null
  end if;
  if newOrdProduct.LoadingReference <> '' then
    call OrdProCheckReference(newOrdProduct.LoadingReference,newOrdProduct.OrdProductId,newOrdProduct.CustomerId)
  end if;
  if newOrdProduct.DeliveryReference <> '' then
    call OrdProCheckReference(newOrdProduct.DeliveryReference,newOrdProduct.OrdProductId,newOrdProduct.CustomerId)
  end if;
  if newOrdProduct.CustomerReference <> '' then
    call OrdProCheckReference(newOrdProduct.CustomerReference,newOrdProduct.OrdProductId,newOrdProduct.CustomerId)
  end if;
  if newOrdProduct.AddressConsigneeId = 0 then
    set newOrdProduct.AddressConsigneeId = null
  end if;
  if newOrdProduct.AddressNotify1Id = 0 then
    set newOrdProduct.AddressNotify1Id = null
  end if;
  if newOrdProduct.AddressNotify2Id = 0 then
    set newOrdProduct.AddressNotify2Id = null
  end if;
  if newOrdProduct.AddressNotify3Id = 0 then
    set newOrdProduct.AddressNotify3Id = null
  end if;
  if newOrdProduct.AddressBillOfLadingId = 0 then
    set newOrdProduct.AddressBillOfLadingId = null
  end if;
  if newOrdProduct.ObjectLWithHoseId1 = 0 then
    set newOrdProduct.ObjectLWithHoseId1 = null
  end if;
  if newOrdProduct.ObjectLWithHoseId2 = 0 then
    set newOrdProduct.ObjectLWithHoseId2 = null
  end if;
  if newOrdProduct.ObjectDWithHoseId1 = 0 then
    set newOrdProduct.ObjectDWithHoseId1 = null
  end if;
  if newOrdProduct.ObjectDWithHoseId2 = 0 then
    set newOrdProduct.ObjectDWithHoseId2 = null
  end if;
  if newOrdProduct.AddressUNStatusId = 0 then
    set newOrdProduct.AddressUNStatusId = null
  end if
end