
If  Exists(Select trigger_name From systrigger where trigger_name = 'ClearIDsEquipment') then
    drop trigger ClearIDsEquipment
End if;

CREATE TRIGGER "ClearIDsEquipment" before insert,update order 2 on USR.Equipment
referencing old as newEquipment new as newEquipment
for each row
/* 
Modified MvR  11 OCT 2004 applied categoryid 
         FV   14 OCT 2004 added TrailerId
         MvR  26 OCT added ServiceAddressId
         FV   10-10-2005 Added MaintenanceContract fields
         MvR  OCT 2005 added SpaceChecker
         MvR  JAN 2005 added MaintenanceContractTypeOfContractID
         MvR  JUL 2006 added DedicatedProductCategoryId
         PvdL NOV 2006 added DedicatedStandardPreOrderId
         FV   15-01-2007 Added LastOrdModalityId
         PvdL 28-04-2008 Added LocationCleanedId
         PvdL 27-08-2008 Added RequiredDepotAddressId
         PT   27-04-2009 Added ObjectGeneratorId
		 WvdS 26 apr 2012 Added PreferredTestAddressId
         AvdH 15-02-2010 Added FormerOwner, NewOwner, LeaseContractId
         AvdH 12-04-2011 Added Insurance1, Insurance2
         PvdL 13-12-2012 Added mmpPlannedPreOrderId / mmpPlannedAddressId
         PT   17-04-2018 Added 
                         - GeneralPurposeObjectTypeOfTelematicsId
                         - GeneralPurposeObjectTelematicsBrandTypeId
                         - GeneralPurposeObjectLengthId
         FV  03-05-2021 Added ManufacturerId
*/

begin
  if newEquipment.FinanceCompanyId = 0 then
    set newEquipment.FinanceCompanyId=null
  end if;
  if newEquipment.LeaseRateCurrencyId = 0 then
    set newEquipment.LeaseRateCurrencyId=null
  end if;
  if newEquipment.OperationalOwneraddressId = 0 then
    set newEquipment.OperationalOwneraddressId=null
  end if;
  if newEquipment.DedicatedaddressId = 0 then
    set newEquipment.DedicatedaddressId=null
  end if;
  if newEquipment.ContainerTruckObject <> 'O' then
    set newEquipment.TrailerId=null
  end if;
  if newEquipment.CategoryId = 0 then
    set newEquipment.CategoryId=null
  end if;
  if newEquipment.TrailerId = 0 then
    set newEquipment.TrailerId=null
  end if;
  if newEquipment.ServiceAddressId = 0 then
    set newEquipment.ServiceAddressId=null
  end if;
  if newEquipment.MaintenanceContractContractPartnerId = 0 then
    set newEquipment.MaintenanceContractContractPartnerId=null
  end if;
  if newEquipment.MaintenanceContractDealerId = 0 then
    set newEquipment.MaintenanceContractDealerId=null
  end if;
  if newEquipment.MaintenanceContractRepairShopId = 0 then
    set newEquipment.MaintenanceContractRepairShopId=null
  end if;
  if newEquipment.SpaceCheckerId = 0 then
    set newEquipment.SpaceCheckerId=null
  end if;
  if newEquipment.MaintenanceContractTypeOfContractID = 0 then
    set newEquipment.MaintenanceContractTypeOfContractID=null
  end if;
  if newEquipment.DedicatedProductCategoryId = 0 then
    set newEquipment.DedicatedProductCategoryId=null
  end if;
  if newEquipment.DedicatedStandardPreOrderId = 0 then
    set newEquipment.DedicatedStandardPreOrderId=null
  end if;
  if newEquipment.LastOrdModalityId = 0 then
     set newEquipment.LastOrdModalityId = null
  end if;
  if newEquipment.PreferredServiceAddressId = 0 then
     set newEquipment.PreferredServiceAddressId = null
  end if;
  if newEquipment.LocationCleanedId = 0 then
     set newEquipment.LocationCleanedId = null
  end if;
  if newEquipment.RequiredDepotAddressId = 0 then
     set newEquipment.RequiredDepotAddressId = null
  end if;
  if newEquipment.ObjectGeneratorId = 0 then
     set newEquipment.ObjectGeneratorId = null
  end if ;
  IF newEquipment.FormerOwner = 0 THEN
    SET newEquipment.FormerOwner = NULL
  END IF ;  
  IF newEquipment.NewOwner = 0 THEN
    SET newEquipment.NewOwner = NULL
  END IF ;  
  IF newEquipment.LeaseContractId = 0 THEN
    SET newEquipment.LeaseContractId = NULL
  END IF ;  
  IF newEquipment.Insurance1Id = 0 THEN
    SET newEquipment.Insurance1Id = NULL
  END IF ;
  IF newEquipment.Insurance2Id = 0 THEN
    SET newEquipment.Insurance2Id = NULL
  END IF ;
  IF newEquipment.PreferredTestAddressId = 0 THEN
	SET newEquipment.PreferredTestAddressId = NULL
  END IF ;
  IF newEquipment.mmpPlannedPreOrderId = 0 THEN
	SET newEquipment.mmpPlannedPreOrderId = NULL
  END IF ;
  IF newEquipment.mmpPlannedAddressId = 0 THEN
	SET newEquipment.mmpPlannedAddressId = NULL
  END IF ;
  IF newEquipment.GeneralPurposeObjectTypeOfTelematicsId = 0 then
        SET newEquipment.GeneralPurposeObjectTypeOfTelematicsId = Null
  END IF ;  
  IF newEquipment.GeneralPurposeObjectTelematicsBrandTypeId = 0 then
        SET newEquipment.GeneralPurposeObjectTelematicsBrandTypeId = Null
  END IF ;  
  IF newEquipment.GeneralPurposeObjectLengthId = 0 then
        SET newEquipment.GeneralPurposeObjectLengthId = Null
  END IF ;  
  IF newEquipment.ManufacturerId = 0 then
        SET newEquipment.ManufactureId = Null
  END IF ;  

end;
