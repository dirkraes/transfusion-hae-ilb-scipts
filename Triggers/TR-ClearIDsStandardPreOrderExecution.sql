Create or replace TRIGGER "ClearIDsStandardPreOrderExecution" before insert,update order 2 on USR.StandardPreOrderExecution
referencing old as NewStandardPreOrderExecution new as NewStandardPreOrderExecution
for each row
// 05 DEC 2006 MvR Created 
// 20 APR 2007 WB Added MoveToAddress

begin
  if newStandardPreOrderExecution.RouteId = 0 then
    set newStandardPreOrderExecution.RouteId=null
  end if;
  if newStandardPreOrderExecution.StartAddressId = 0 then
    set newStandardPreOrderExecution.StartAddressId=null
  end if;
  if newStandardPreOrderExecution.EndAddressId = 0 then
    set newStandardPreOrderExecution.EndAddressId=null
  end if;
  if newStandardPreOrderExecution.MoveToAddressId = 0 then
    set newStandardPreOrderExecution.MoveToAddressId=null
  end if;
  if newStandardPreOrderExecution.PickupFromAddressId = 0 then
    set newStandardPreOrderExecution.PickupFromAddressId=null
  end if;
end