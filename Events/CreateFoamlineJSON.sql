drop event if exists usr.CreateFoamlineJSON;

create event USR.CreateFoamlineJSON
schedule "hourly" between '06:00' and '20:00' every 1 hours
handler
currentevent:
begin
	call usr.ProcessFoamLineOrders();
end ;
