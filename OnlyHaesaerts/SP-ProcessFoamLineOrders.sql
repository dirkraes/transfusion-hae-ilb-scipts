create or replace PROCEDURE usr.ProcessFoamLineOrders(in in_EnableLogging integer default 0)
/*
   Created  DR 25-11-2020
   Modified          
*/
begin
   declare err_notfound exception for sqlstate value '02000';
   declare eq_modality_truck              INTEGER = 1;
   declare eq_modality_rail               INTEGER = 3;
   declare local_tel                      INTEGER;
   
   declare local_orderid                  INTEGER;
   declare local_equipmentid              INTEGER;
   declare local_tankcontainernumber      VARCHAR(30);
   declare local_deliveryreference        VARCHAR(50);
   declare local_manufacturerid           VARCHAR(30);
   declare local_manufacturercode         VARCHAR(30);
   declare local_manufacturername         VARCHAR(50);
   declare local_productid                INTEGER;
   declare local_productremarks           VARCHAR(255);
   declare local_productdescription       VARCHAR(255);
                                          
   declare local_loadaddressid            INTEGER;
   declare local_loadaddressplaceid       INTEGER;
   declare local_loadaddressplacename     VARCHAR(50);
   declare local_deliveryaddressid        INTEGER;
   declare local_deliveryaddressplaceid   INTEGER;
   declare local_deliveryaddressplacename VARCHAR(50);
   declare local_customerreference        VARCHAR(80);
   declare local_dateloaded               DATE;
   declare local_timeloaded               TIME;
   declare local_loadingtime              TIME;
                                          
   declare local_loadeddateuntil          DATE;
   declare local_ldate                    DATE;
   declare local_ltime                    VARCHAR(50);
   declare local_datedelivered            DATE;
   declare local_timedelivered            TIME;
   declare local_deliverytime             TIME;
   declare local_datedelivereduntil       DATE;
   declare local_ddate                    DATE;
   declare local_dtime                    VARCHAR(50);
   declare local_dtimelatest              TIME;
   declare local_deliveryaddressname      VARCHAR(50);
   declare local_equipmentweight          INTEGER;
   
   declare local_last_modality_seq        INTEGER;
   declare local_modalityid               INTEGER;
   declare local_ordmodstatus             INTEGER;
   declare local_sendOrderFlag            INTEGER;
   declare local_ordmodenddate            DATE;
   declare local_ordmodendtime            TIME;
   declare local_count_ordmodproduct      INTEGER;
   
   declare local_om_modalityid            INTEGER;
   declare local_m_modalitycode           VARCHAR(10);
   declare local_om_routeid               INTEGER;
   declare local_r_Description            VARCHAR(100);
   declare local_om_transportunitid       INTEGER;
   declare local_tu_transportunitid       INTEGER;
   declare local_tu_truckid               INTEGER;
   declare local_e_RegistrationNumber     VARCHAR(30);
   declare local_tu_driverid              INTEGER;
   declare local_d_name                   VARCHAR(60);
   declare local_d_telareacode            VARCHAR(10);
   declare local_d_tel                    VARCHAR(30);
   declare local_om_dateactual            DATE;
   declare local_om_timeactual            TIME;
   declare local_om_daterevised           DATE;
   declare local_om_timerevised           TIME;
   declare local_om_DateOriginal          DATE;
   declare local_om_TimeOriginal          TIME;
   declare local_om_Enddate               DATE;
   declare local_om_endtime               TIME;
   declare local_om_FromPlaceId           INTEGER;
   declare local_fp_name                  VARCHAR(60);
   declare local_om_toPlaceid             INTEGER;
   declare local_tp_name                  VARCHAR(60);
   declare local_om_status                INTEGER;
   declare local_om_browsersequence       INTEGER;
   declare local_om_position              VARCHAR(50);
   declare local_om_positionDate          DATE;
   declare local_om_voyageNumber          VARCHAR(40);
   declare local_om_trucknumber           VARCHAR(20);
   declare local_om_drivername            VARCHAR(50);
   declare local_om_driverphonenumber     VARCHAR(15);
   
   declare local_json_all                 LONG VARCHAR;
   declare local_json_order               LONG VARCHAR;
   declare local_ToFileName               VARCHAR(255);
   declare local_currentDateTime          VARCHAR(25);
   
   declare cur_Foamline dynamic scroll cursor for
      select o.orderid,       e.equipmentid,          e.tankcontainernumber,     op.DeliveryReference,      p.ManufacturerName,     p.manufacturerid, 
             op.productid,    p.remarks,              p.description,             op.laddressid,             la.locationplaceid,     lap.name, 
             op.daddressid,   da.locationplaceid,     dap.name,                  op.customerreference,      op.Dateloaded,          op.timeloaded, 
             op.loadingtime,  op.loadeddateuntil,     op.ldate,                  op.ltime,                  op.DateDelivered,       op.timedelivered, 
             op.deliverytime, op.deliveredDateUntil,  op.ddate,                  op.dtime,                  op.dtimelatest,         da.name,                
             op.LoadedWeight
      from orders as o
      inner join equipment as e on e.equipmentid = o.equipmentid
      inner join ordproduct as op on op.ordproductid = FirstOrdProductId
      inner join product as p on p.productid = op.productid
      inner join address as la on la.addressid = op.laddressid
      inner join Place as lap on lap.placeid = la.locationplaceid
      inner join address as da on da.addressid = op.daddressid
      inner join Place as dap on dap.placeid = da.locationplaceid
      where o.PrincipalId in (select addressid from address where codefld in ('TRUMOSEUR','ALFSEM', 'RIFROSEUR', 'FOAMOSEUR')) 
            and o.void = 'N' order by o.orderid ;    
      
   declare cur_ordmods dynamic scroll cursor for 
      select om.modalityid, m.modalitycode,om.routeid, r.Description, om.transportunitid,tu.transportunitid, tu.truckid, e.RegistrationNumber, tu.driverid, d.name, d.telareacode, d.tel,
       om.dateactual, om.timeactual, om.daterevised, om.timerevised, om.DateOriginal, om.TimeOriginal, om.Enddate, om.endtime,
       om.FromPlaceId, fp.name, om.toPlaceid, tp.name, om.status, om.browsesequence, om.position, om.positionDate, om.voyageNumber,
       om.trucknumber, om.drivername, om.driverphonenumber
      from ordmodality om
      inner join modality m on m.modalityid = om.modalityid
      left outer join route r on r.routeid = om.routeid
      left outer join TransportUnit tu on tu.transportunitid = om.transportunitid
      left outer JOIN EQUIPMENT e on e.equipmentid = tu.truckid
      left outer join driver d on d.driverid = tu.driverid
      inner join place as fp on fp.placeid = om.fromplaceid
      inner join place as tp on tp.placeid = om.toplaceid
      where orderid = local_orderid
      and exists (select 1 from ordModproduct where ordmodalityid = om.ordmodalityid) 
      order by browsesequence;
      
   if in_EnableLogging = 1 then 
      message '*********** Start ProcessFoamLineOrders ****************' type info to console;
   end if;                                                                                           
   //
   // Loop cursor
   //
   select DateFormat(now(*), 'yyyy-mm-ddThh:mm:ss') into local_currentDateTime;
   set local_json_all = '{ "QueryDateTime": "' || local_currentDateTime || '",';
   set local_json_all = local_json_all || '"DeliveryDate": "' || local_currentDateTime || '",';
   set local_json_all = local_json_all || '"From": "Haesaerts", ';
   set local_json_all = local_json_all || '"Orders": [ ';
   
   open cur_Foamline;
   set local_tel = 0;
   set local_json_order = '';
   L1: loop
  	   fetch next cur_Foamline     
         into local_orderid,              local_equipmentid,            local_tankcontainernumber,       local_deliveryreference,          local_manufacturercode,          local_manufacturerid,
              local_productid,            local_productremarks,         local_productdescription,        local_loadaddressid,             local_loadaddressplaceid,        local_loadaddressplacename, 
              local_deliveryaddressid,    local_deliveryaddressplaceid, local_deliveryaddressplacename,  local_customerreference,         local_dateloaded,                local_timeloaded,           
              local_loadingtime,          local_loadeddateuntil,        local_ldate,                     local_ltime,                     local_datedelivered,             local_timedelivered,        
              local_deliverytime,         local_datedelivereduntil,     local_ddate,                     local_dtime,                     local_dtimelatest,               local_deliveryaddressname,       
              local_equipmentweight;	
      if sqlstate = err_notfound then 
         leave L1 
   	end if;
      
      
      // bepaal laatste geladen ordmodality (probleem is de laatste geladen ordmodality, hoe bepaal je die)
      with temp as 
      (select om.browsesequence, modalityid, status, enddate, endtime, 
         (select count() from ordModproduct where ordmodalityid = om.ordmodalityid) as count_ordmodproduct
         from ordmodality om         
         where  om.orderid = local_orderid order by om.BrowseSequence)
      select first browsesequence, modalityid, status, enddate, endtime, count_ordmodproduct
         into local_last_modality_seq, local_modalityid, local_ordmodstatus, local_ordmodenddate, local_ordmodendtime, local_count_ordmodproduct
         from temp
         where count_ordmodproduct > 0 
         order by BrowseSequence desc;
      
      // controles of order opgestuurd moet worden   
      set local_sendOrderFlag = 0;
      // START: als ordproduct.dateloaded is ingevuld en een loaddatum ligt voor vandaag
      if local_dateloaded is not null then 
         if in_EnableLogging = 1 then 
            message '* Orderid:' || local_orderid || ' - ' || local_dateloaded type info to console;
         end if;
      END if;
      
      if ((isnull(local_dateloaded,'2100-12-31') <= current date)) THEN
         set local_sendOrderFlag = 0;
         // Wanneer wel: Als de enddata van de laatste modality (rail) groter is dan vandaag
         IF (local_modalityid = eq_modality_rail and (local_ordmodstatus < 4 OR (local_ordmodstatus = 4 and isnull(local_ordmodenddate + 1 ,'1900-01-01') > current date ))) THEN
            set local_sendOrderFlag = 1;
            if in_EnableLogging = 1 then 
               message '* rail modality:' || local_modalityid || ' - ' || local_ordmodstatus  || ' ' ||  local_ordmodenddate type info to console; 
            end if;
         else
            // Of wanneer de laatste modality (truck) is en datedelivered ligt na morgen 
            IF (local_modalityid = 1 and isnull(local_datedelivered,'2100-12-31') >= current date ) then 
               set local_sendOrderFlag = 1;
               if in_EnableLogging = 1 then 
                  message '* truck modality:' || local_modalityid || ' - ' || local_datedelivered   type info to console;
               end if;
            else 
               if in_EnableLogging = 1 then 
                  message '* truck modality:' || local_modalityid || ' - ' || local_datedelivered || ' ' || local_ordmodstatus || ' ' || local_ordmodenddate type info to console;
               end if;
            end if;
         end if;
      end if;
                  
      select max(om.browsesequence) into local_last_modality_seq 
      from ordmodality om 
      where om.orderid = local_orderid and exists (select 1 from ordModproduct where ordmodalityid = om.ordmodalityid);
      
      // als de order verstuurd mag worden opbouwen order JSON
      if (local_sendOrderFlag = 1) then
         set local_tel = local_tel + 1; 
         if in_EnableLogging = 1 then 
            message '*********** Orderid:' || local_orderid || ' - ' || local_tel type info to console;
         end if;
         if local_json_order = '' then
            set local_json_order = '{ "OrderNumber": "' || local_orderid || '", "Containers": [';
         else
            set local_json_order = ', { "OrderNumber": "' || local_orderid || '", "Containers": [';
         end if;

         set local_json_order = local_json_order || '{ "ContainerNumber": "' || local_tankcontainernumber ||'", ';
         set local_json_order = local_json_order || '"CustomerOrderNumber": "' || local_deliveryreference || '",';
         set local_json_order = local_json_order || '"ManufacturerName": "' || local_manufacturercode || '",';
         set local_json_order = local_json_order || '"ManufacturerID": "' || local_manufacturerid || '",';
--         set local_json_order = local_json_order || '"extra dateloaded": "' || local_dateloaded || '",';
--         set local_json_order = local_json_order || '"extra modalityid": "' || local_modalityid || '",';
--         set local_json_order = local_json_order || '"extra ordmodstatus": "' || local_ordmodstatus || '",';
--         set local_json_order = local_json_order || '"extra ordmodenddate": "' || local_ordmodenddate || '",';
--         set local_json_order = local_json_order || '"extra datedelivered": "' || local_datedelivered || '",';
         set local_json_order = local_json_order || '"ProductPartNumber": "' || local_productremarks || '",';
         set local_json_order = local_json_order || '"ProductName": "' || local_productdescription || '",';    
         set local_json_order = local_json_order || '"LoadingPlace": "' || local_loadaddressplacename || '",';    
         set local_json_order = local_json_order || '"UnloadingPlace": "' || local_deliveryaddressplacename || '",';    
         set local_json_order = local_json_order || '"CustomerTrackingNumber": "' || local_customerreference || '",';  
         set local_json_order = local_json_order || '"ETD": "' || DateFormat(local_loadeddateuntil + DateADD(hour, HOURS(local_loadingtime), local_timeloaded), 'yyyy-mm-ddThh:mm:ss' ) || '",';  
         set local_json_order = local_json_order || '"ETA": "' || DateFormat(local_ordmodenddate + local_ordmodendtime, 'yyyy-mm-ddThh:mm:ss' ) || '",';  
         set local_json_order = local_json_order || '"ReceiverName": "' || local_deliveryaddressname || '",';  
         set local_json_order = local_json_order || '"Weight": "' || local_equipmentweight || '",';  
         set local_json_order = local_json_order || '"DeliveryData": [ ';
         open cur_ordmods;
         L2: loop
            fetch next cur_ordmods into 
               local_om_modalityid,      local_m_modalitycode,     local_om_routeid,           local_r_Description,       local_om_transportunitid, 
               local_tu_transportunitid, local_tu_truckid,         local_e_RegistrationNumber, local_tu_driverid,         local_d_name,
               local_d_telareacode,      local_d_tel,              local_om_dateactual,        local_om_timeactual,       local_om_daterevised, 
               local_om_timerevised,     local_om_DateOriginal,    local_om_TimeOriginal,      local_om_Enddate,          local_om_endtime, 
               local_om_FromPlaceId,     local_fp_name,            local_om_toPlaceid,         local_tp_name,             local_om_status,
               local_om_browsersequence, local_om_position,        local_om_positionDate,      local_om_voyageNumber,     local_om_trucknumber,
               local_om_drivername,      local_om_driverphonenumber;
            if sqlstate = err_notfound then 
               leave L2
            end if;
            set local_json_order = local_json_order || '{ "DeliveryType" : "'|| local_m_modalitycode || '",';
            CASE local_om_modalityid
            // Truck 
            WHEN 1 THEN 
               set local_json_order = local_json_order || ' "TruckNumber" : "'|| local_om_trucknumber || '",';
               set local_json_order = local_json_order || ' "Driver" : "'|| local_om_drivername || '",';
               set local_json_order = local_json_order || ' "DriverPhoneNumber" : "'|| local_om_driverphonenumber || '",';
               set local_json_order = local_json_order || ' "ETD" : "'|| DateFormat(IFNULL(local_om_dateactual,IFNULL(local_om_daterevised,local_om_DateOriginal+local_om_TimeOriginal, local_om_daterevised+local_om_timerevised), local_om_dateactual+local_om_timeactual ), 'yyyy-mm-ddThh:mm:ss')  || '",';
               set local_json_order = local_json_order || ' "ETA" : "'|| Dateformat(local_om_Enddate+local_om_endtime, 'yyyy-mm-ddThh:mm:ss' ) || '",';
               set local_json_order = local_json_order || ' "LoadingPlace" : "'|| local_fp_name || '",';
               set local_json_order = local_json_order || ' "UnloadingPlace" : "'|| local_tp_name || '",';
               set local_json_order = local_json_order || ' "CurrentPosition" : "'|| local_om_position || '",';
               CASE local_om_status
               WHEN 0 THEN
                  set local_json_order = local_json_order || '  "Status" : "Planned"}';
               WHEN 1 THEN
                  set local_json_order = local_json_order || '  "Status" : "In progress"}';
               ELSE
                  set local_json_order = local_json_order || '  "Status" : "Completed"}';
               END;
            // Ferry
            WHEN 2 THEN
               set local_json_order = local_json_order || ' "FerryName" : "'|| local_r_Description || '",';
               set local_json_order = local_json_order || ' "ETD" : "'|| DateFormat(IFNULL(local_om_daterevised, local_om_DateOriginal+local_om_TimeOriginal, local_om_daterevised+local_om_timerevised), 'yyyy-mm-ddThh:mm:ss')  || '",';
               set local_json_order = local_json_order || ' "ETA" : "'|| Dateformat(local_om_Enddate+local_om_endtime, 'yyyy-mm-ddThh:mm:ss' ) || '",';
               set local_json_order = local_json_order || ' "LoadingPlace" : "'|| local_fp_name || '",';
               set local_json_order = local_json_order || ' "UnloadingPlace" : "'|| local_tp_name || '",';
               set local_json_order = local_json_order || ' "CurrentPosition" : "'|| local_om_position || '",';
               // Rail
               CASE local_om_status
               WHEN 3 THEN
                  set local_json_order = local_json_order || '  "Status" : "In progress"}';
               WHEN 4 THEN
                  set local_json_order = local_json_order || '  "Status" : "Completed"}';
               ELSE
                  set local_json_order = local_json_order || '  "Status" : "Planned"}';
               END;
            // Rail
            WHEN 3 THEN
               set local_json_order = local_json_order || ' "RailwayWagonNumber" : "'|| local_om_voyageNumber || '",';
               set local_json_order = local_json_order || ' "ETD" : "'|| DateFormat(IFNULL(local_om_daterevised, local_om_DateOriginal+local_om_TimeOriginal, local_om_daterevised+local_om_timerevised), 'yyyy-mm-ddThh:mm:ss')  || '",';
               set local_json_order = local_json_order || ' "ETA" : "'|| Dateformat(local_om_Enddate+local_om_endtime, 'yyyy-mm-ddThh:mm:ss' ) || '",';
               set local_json_order = local_json_order || ' "LoadingPlace" : "'|| local_fp_name || '",';
               set local_json_order = local_json_order || ' "UnloadingPlace" : "'|| local_tp_name || '",';
               set local_json_order = local_json_order || ' "CurrentPosition" : "'|| local_om_position || '",';
               CASE local_om_status
               WHEN 3 THEN
                  set local_json_order = local_json_order || '  "Status" : "In progress"}';
               WHEN 4 THEN
                  set local_json_order = local_json_order || '  "Status" : "Completed"}';
               ELSE
                  set local_json_order = local_json_order || '  "Status" : "Planned"}';
               END;
            ELSE
               
            END;
            if (local_last_modality_seq = local_om_browsersequence) then
            else 
               set local_json_order = local_json_order || ',';
            end if;
         end loop L2;
         set local_json_order = local_json_order || ']} ]}';
         if in_EnableLogging = 1 then 
            message '------------ order ' || local_json_order type info to console;
         end if;
         set local_json_all = local_json_all || local_json_order ;
         close cur_ordmods;
      END IF;
   end loop L1;
   close cur_Foamline;
   set local_json_all = local_json_all || ' ]}';
   -- set local_ToFileName  = 'C:\\temp\\foamline.json';
   -- set local_ToFileName  = 'C:\\xmloutbox\\ToSeeburgerCloudJson\\Test\\FoamLine.json';
   set local_ToFileName  = 'C:\\xmloutbox\\ToSeeburgerCloudJson\\Prod\\FoamLine.json';
   call xp_write_file(local_ToFileName, local_json_all); 
   if in_EnableLogging = 1 then 
      message 'ProcessFoamLineOrder ready....'||local_json_all type info to console;
   end if;
end;

GRANT EXECUTE ON usr.ProcessFoamLineOrders TO usr;