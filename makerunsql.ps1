﻿Out-File -FilePath run.sql -Encoding utf8
Add-Content -Path "run.sql" -Value "-- Database changes"
Get-ChildItem -Path "DatabaseChanges" | ForEach-Object {
    Add-Content -Path "run.sql" -Value "read DatabaseChanges\$_;"
} 
Add-Content -Path "run.sql" -Value "-- Triggers "
Get-ChildItem -Path "Triggers" | ForEach-Object {
    Add-Content -Path "run.sql" -Value "read Triggers\$_;"
}
Add-Content -Path "run.sql" -Value "-- Views "
Get-ChildItem -Path "Views" | ForEach-Object {
    Add-Content -Path "run.sql" -Value "read Views\$_;"
}
Add-Content -Path "run.sql" -Value "-- StoredProcedures "
Get-ChildItem -Path "StoredProcedures" | ForEach-Object {
    Add-Content -Path "run.sql" -Value "read StoredProcedures\$_;"
}
Add-Content -Path "run.sql" -Value "-- Other "
Get-ChildItem -Path "Other" | ForEach-Object {
    Add-Content -Path "run.sql" -Value "read Other\$_;"
} 