ALTER TABLE usr."Product"
ADD "IsControlledGood" INTEGER;

ALTER TABLE usr."Product"
ADD "ManufacturerName" VARCHAR(25);

ALTER TABLE usr."Product"
ADD "ManufacturerId" VARCHAR(30);

