ALTER TABLE usr."StandardPreOrderExecution"
ADD "PickupFromAddressId" integer;

ALTER TABLE "USR"."StandardPreOrderExecution" 
ADD CONSTRAINT "R_PickupFromAddress" 
FOREIGN KEY ( "PickupFromAddressId" ASC ) 
REFERENCES "USR"."Address" ( "AddressId" );
