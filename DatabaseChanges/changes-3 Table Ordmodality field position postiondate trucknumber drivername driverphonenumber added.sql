ALTER TABLE usr."OrdModality"
ADD "Position" VARCHAR(50);

ALTER TABLE usr."OrdModality"
ADD "PositionDate" date;

ALTER TABLE usr."OrdModality"
ADD "TruckNumber" VARCHAR(20);

ALTER TABLE usr."OrdModality"
ADD "DriverName" VARCHAR(50);

ALTER TABLE usr."OrdModality"
ADD "DriverPhoneNumber" VARCHAR(15);

