create or replace function usr.GetDistanceToTerminal(IN in_OrdModalityId INTEGER)
returns integer
/*
  Created  PvdL 10-09-2013
  Modified PvdL 30-03-2015 - When OrdModality ARRIVED or not yet departed, return 0
                           - Calculate distance to TOPLACE of ROUTE of OrdModality
           FV   06-11-2020 Added filter on E.OutOfFleet = 0
*/
BEGIN
    
    DECLARE l_OrdModalityId      INTEGER;
    DECLARE l_Status             INTEGER;
    DECLARE l_OrderByDate        DATE;
    DECLARE l_OrderByTime        TIME; 
    DECLARE l_CurrentLongitude   INTEGER;
    DECLARE l_CurrentLatitude    INTEGER;
    DECLARE l_TerminalLongitude  INTEGER;
    DECLARE l_TerminalLatitude   INTEGER;
    DECLARE r_DistanceToTerminal INTEGER;
	
	SET l_OrdModalityId = 0;
    SET r_DistanceToTerminal = 0;
    

    SELECT  OM.OrdModalityId
          , OM.Status
          , OM.OrderByDate
          , OM.OrderByTime
          , FLOOR(TXT.PositionLongitude * 10000000) AS CurrentLongitude
          , FLOOR(TXT.PositionLatitude * 10000000)  AS CurrentLatitude
          , PT.X
          , PT.Y
    INTO    l_OrdModalityId
          , l_Status 
          , l_OrderByDate
          , l_OrderByTime 
          , l_CurrentLongitude
          , l_CurrentLatitude
          , l_TerminalLongitude
          , l_TerminalLatitude
    FROM    usr.OrdModality OM 
    JOIN    usr.Orders O ON OM.OrderId = O.OrderId 
    JOIN    usr.Equipment E ON  O.EquipmentId = E.EquipmentId 
    JOIN    usr.TX_Tracer TXT ON TXT.EquipmentId = E.EquipmentId 
    JOIN    usr.Route R ON R.RouteId = OM.RouteId
    JOIN    usr.Place PT ON PT.PlaceId = R.PlaceToId 
    WHERE   OM.OrdModalityId = in_OrdModalityId and E.OutOfFleet = 0;

    IF  l_OrdModalityId = 0 THEN
        SET r_DistanceToTerminal = 0;
    ELSE
        IF  l_OrderByDate || ' ' ||  l_OrderByTime > NOW() THEN
            SET r_DistanceToTerminal = 0;
        ELSE 
            IF  l_Status = 4 THEN 
                SET r_DistanceToTerminal = 0;
            ELSE
                IF  l_TerminalLongitude <> 0
                AND l_TerminalLatitude <> 0
                AND l_CurrentLongitude <> 0
                AND l_CurrentLatitude <> 0 THEN
                    SET r_DistanceToTerminal = usr.CalculateAerialDistance(l_TerminalLongitude,l_TerminalLatitude,l_CurrentLongitude,l_CurrentLatitude);
                ELSE
                    SET r_DistanceToTerminal = 0;
                END IF;
            END IF;
        END IF;
    END IF;
	RETURN r_DistanceToTerminal;
END;

grant execute on usr.GetDistanceToTerminal to usr;