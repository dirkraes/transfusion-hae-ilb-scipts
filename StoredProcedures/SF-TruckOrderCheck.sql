Create or Replace FUNCTION "USR"."TruckOrderCheck"(in in_TransportUnitId integer,in in_OrdModalityIDs varchar(1024),in in_ActionDate date,in in_ActionTime time)
returns varchar(4096)
/*
  Returns a string with error @@ ErrorMessage
  Error 0 - No Error
        1 - Warning
        2 - Error

   Created  FV 02-11-2012 Check whether Order is valid for given TransportUnit(Truck/Driver)
   Modified FV 12-03-2012 Replaced some checks on OrdModality in stead of order
            PL 23-04-2013 Pump-check: If Truck has no pump
                                      - Check if TU has Chassis, if so check if chassis has pump
                                      - If chassis has pump, truck should have Hydro-Unit
                          ADR-check: - Seperate check of chassis/truck         
                                     - changed message
            PL 13-05-2013 Only Check ADR Chassis, when TU has Chassis      
            PL 20-09-2013 Added ObjectApprovedCleaningCheck (depending on db-setting)
            PL 25-09-2013 Added logging for ApprovedCleaningCheck (variable LOGGING)
            FV 27-09-2013 Added Previous product check for Chassis
            PL 02-10-2013 Added 'if local_ReturnValue < local_Warning then set local_ReturnValue = local_Warning end if' at Previous Product Check
            FV 10-10-2013 Skip TruckOrderCheck for H&S. Currently only PreviousProduct and previous cleaning check for Chassis is needed
            PL 27-11-2013 Use for each check a db-option to determine if check has to be done:
                          - TruckOrderCheck             : Inforit_EnableTruckOrderCheck
                          - Approved Cleaning Check     : Inforit_ObjectApprovedCleaningCheck
                          - PreviousProductChassisCheck : Inforit_PreviousProductChassisCheck
                          Skip TU where ExcludeFromChassisCheck is TRUE
            PL 29-11-2013 Always fetch TU-data   
            PL 05-12-2013 Moved adding local_OrderText to local_ReturnText outside 'if local_isPreviousProductChassisCheck = 1'-statement
            FV 16-01-2017 Added generator check on Equipment
            DR 07-12-2021 Cleared the TruckOrderCheck and rebuild it for Waste Permit check for Haesaerts
*/
begin
   declare CRLF varchar(2) = CHAR(13)||CHAR(10);
   declare err_notfound exception for sqlstate value '02000';
   //
   declare local_OK                          integer = 0;
   declare local_Warning                     integer = 1;
   declare local_Error                       integer = 2;
   declare LOGGING                           integer;

   declare local_isTruckOrderCheck           integer;
   declare local_missing_permits             varchar(1024);
   declare local_ReturnValue                 integer;
   declare local_ReturnText                  varchar(4096);
   declare local_TruckOrderCheckWastePermit  varchar(10);
   //
   // set initial values
   //
   set LOGGING = (select isNull(usr.GetOption('InforIT_TruckOrderCheck_Logging'),0)); 
   //
   set local_ReturnValue = local_OK;
   //
   //
   //
   if LOGGING = 1 then
      	set local_ReturnText = local_ReturnText || (select Current TimeStamp) || CRLF ;
      	set local_ReturnText = local_ReturnText || 'Parameters:' || CRLF;
      	set local_ReturnText = local_ReturnText || '---------------------------------------------------------------------------'|| CRLF ;
      	set local_ReturnText = local_ReturnText || 'in_TransportUnitId : ' || in_TransportUnitId || CRLF ;
      	set local_ReturnText = local_ReturnText || 'in_OrdModalityIDs  : ' || in_OrdModalityIDs  || CRLF ;
      	set local_ReturnText = local_ReturnText || 'in_ActionDate      : ' || in_ActionDate      || CRLF ;
      	set local_ReturnText = local_ReturnText || 'in_ActionTime      : ' || in_ActionTime      || CRLF ;
      	set local_ReturnText = local_ReturnText || '---------------------------------------------------------------------------'|| CRLF ;
   end if;  

   if not exists(select 1 from usr.OrdModProduct where OrdModalityId = in_OrdModalityIDs) then 
      set local_ReturnText = local_ReturnText || 'Ordmodality is        : Empty' || CRLF ;
      set local_ReturnText = local_ReturnText || '---------------------------------------------------------------------------'|| CRLF ;
      set local_ReturnValue = local_Ok;
      return local_ReturnValue ||'@@'|| local_ReturnText;
   end if;
  
   //
   // Check which checks have to be done
   //
   if usr.GetOption('InforIT_EnableTruckOrderCheck') = '1' then
      set local_isTruckOrderCheck             = 1;
      if LOGGING = 1 then
       	set local_ReturnText = local_ReturnText || 'TruckOrderCheck    : ENABLED' || CRLF ;
      end if;
   else
      set local_isTruckOrderCheck             = 0;
      if LOGGING = 1 then
       	set local_ReturnText = local_ReturnText || 'TruckOrderCheck    : DISABLED' || CRLF ;
      end if;
   end if; 
   //
   // if TruckOrder check is enabled
   //
   if local_isTruckOrderCheck = 1 then
      set local_TruckOrderCheckWastePermit = (select usr.GetOption('InforIT_TruckOrderCheck_WastePermit'));
      if LOGGING = 1 then
       	set local_ReturnText = local_ReturnText || 'TruckOrderCheck Req. certificate : ' || local_TruckOrderCheckWastePermit || CRLF ;
       	set local_ReturnText = local_ReturnText || '---------------------------------------------------------------------------'|| CRLF ;
      end if;
      //
      // Check if all req_certs(type waste permit) of the StandardPreOrder exists as req_certs on the address of the owner of the truck
      //
      select LIST(description) into local_missing_permits
      from StdPreOrdProRequiredCertificate 
      where type = local_TruckOrderCheckWastePermit and 
         StandardPreOrderProductid in 
            (select StandardPreOrderProductid 
             from StandardPreOrderProduct 
             where StandardPreOrderId in 
               (select  StandardPreOrderId 
                from preorder 
                where preorderid in 
                  (select orderid from ordmodality where ordmodalityid = in_OrdModalityIDs))) and 
         description not in 
            (select description 
             from AddRequiredCertificate 
             where type = local_TruckOrderCheckWastePermit and 
               addressid in 
                  (select ownerid 
                   from equipment 
                   where equipmentid in 
                     (select truckid from transportunit where transportunitid = in_TransportUnitId) and ContainerTruckObject = 'T')); 

      if local_missing_permits <> '' then 
	      set local_ReturnText = local_ReturnText || 'Missing waste permits for : '|| local_missing_permits || CRLF ;
         set local_ReturnValue = local_Error;
      end if;
   end if;

  if LOGGING = 1 then
   	 set local_ReturnText = local_ReturnText || '---------------------------------------------------------------------------'|| CRLF ;
  end if;
  return local_ReturnValue ||'@@'|| local_ReturnText;
end;

grant execute on "USR"."TruckOrderCheck" to usr;