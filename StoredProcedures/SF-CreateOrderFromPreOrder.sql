CREATE OR REPLACE FUNCTION "USR"."CreateOrderFromPreOrder"( in In_PreOrderIdentifier integer ) 
returns integer
/*
Created  NOV 2006 MvR 
modified MvR 17 JAN 2007 
         MvR Jan / Feb 2007 move / drop / swap
         MvR 13 Feb 2007 implement rate dimension
         MvR 22 feb 2007 changed modalities swap / route
         MvR created test scenarios
         MvR 26 feb 2007 if 2 routes if necessary add 'connect' modality between them
         MvR 27 feb 2007 plandates and times
         MvR 07 mar 2007 initialize nextfields and previousfields
         MvR 13 mar 2007 fill depotid from connection tricking between 2 routes
         MvR 11 apr 2007 added profitcentre
         MvR 17 apr 2007 generate 'Book' in timefields (Ltime and Dtime)
         MvR 17 apr 2007 bugfix line 588 changed toaction from 3 to 1
         MvR 19 apr 2007 set status to 9 when finished
         MvR 23 apr 2007 introducing tankplanning
         MvR 26 apr 2007 ordmodproduct
         MvR 27 apr 2007 route and so on
         MvR 01 may 2007 skip log mmp 
         MvR 03 May
                        0       1        2      3    4    5     6
                Action= Loading|Delivery|Pickup|Drop|Road|Clean|Depot 
         MvR 22 May
         MvR 29 May new method
         MvR 30 May time / distance
         MvR 01 Jun some orders only use MMP tankselection
         MvR 01 Jun some orders only use MMP tankselection
         MvR 04 Jun adjusted filling remark swap/dropfordelivery orders
         MvR 06 Jun drop drop remark
         MvR 08 Jun changed cursor and fill some depot ids
         MvR 12 Jun changed added date/time to pickup/drop mods in case of a swap
         MvR 14 Jun some changes
         MvR 19 Jun change planboardrule morning
         MvR 19 Jun change planboardrule afternoon
         MvR 20 Jun change filling remarks
         MvR 21 Jun intoduced status 7
                    status 9 order not through mmp or through mmp using mmp planning
                    status 7 order through mmp but using default executions because SPO is swap eso
         MvR 22 Jun fill order with equipmentid after creating the ordmodalities
         MvR 26 JUN 2007 set status 0 to status 2
         MvR 26 JUN 2007 only update single order
         MvR 27 JUN 2007 skip filling planboardrule for modalities referencing a route
         MvR 27 JUL 2007 changed parameters planboardrule
         MvR 01 AUG 2007 tunen
         FV  26 SEP 2007 No ABDistance in Route modalities 
         FV              Set End Date/Time always with Arrival Date/Time from NextNext card
         MVR 18 OCT 2007 cleanup - rearrange code
         MvR 16 NOV 2007 using executions - impmeneted route after delivery
                         use tankplanning records AFTER unload when order is send to mmp but using executions
         FV  20 NOV 2007 Added fetch first tankplanning from next preorder when next record is empty (not only nextnext empty) No trucking to next preorder
         MvR 23 NOV 2007 drop - delivery 
         FV  26 NOV 2007 Fetch arrival date/time from NextNext ordmodality 
         MvR 30 NOV 2007 do not use mmp tanplanningrecords when dropfordelivery-order
         FV  06 DEC 2007 Solve trucking - trucking tankplanning records with 2 modalities xx - road, road  - xx
         PL  13 DEC 2007 Get Order.Description from PreOrder instead of StandardPreOrder
         MvR 18 DEC 2007 DropFroDelivery orders also to MMP
         MvR 14 jan 2008 do not insert a drop / pickup at cleaning record
         MvR 14 jan 2008 or Local_FirstTankPlanningIdUnload is null to prevent both mmp records and premodrecords will be used
         FV  14 jan 2008 Clear mmp fields in Equipment to be refilled in mmp
         FV  24 jan 2008 Moved assign of equipment to order insert statement instead of update statement at the back of the script 
                         Force refresh LastOrdModalityId at the end
                         Only add product record when FromAction is 'load' or ToAction = 'delivery'
         MvR 06 feb 2008 adjust some timeformats within ordproduct
         MvR 08 feb 2008 fill Generatedby 1 = Premodalities ; 2 = Tankplanning
                         fill Debuginfo
         MvR 12 feb 2008 adjust some timeformats within ordproduct
         FV  15 feb 2008 Add filling of AddresTo and PlaceTo when no trucking
         FV  26 feb 2008 Changed Order by on TankPlanning, added arrival date/time and departure date/time
         FV  04 mar 2008 Next record filter added ID <> Prev_Id
         PL  17 mar 2008 Add PreOrderInvoice
         FV  20 mar 2008 When loading + wap then use Loading date/time from MMP if present
         PL  21 mar 2008 Remove ValidFrom and ValidUntil from PreOrderInvoice
         MvR 27 mar 2008 pickup / drop modality genereated from move must have 'loading datetime' and drop from loading must have loading datetime
         MvR 31 mar 2008 pickup / drop modality genereated from move must have 'loading datetime' and drop from loading must have loading datetime
         MvR 04 apr 2008 fill orderrequest and line(s) with orderid if created from preorder
         MvR 07 apr 2008 set equipment.skipfillmmp = 1 also when only tankselection because of swap , drop or move SPO
         MvR 16 may 2008 if route and move then set enddate to null
         FV  02-06-2008 Removed adding of UnloadDuration when toaction = delivery or drop for delivery. NOt neccesary anymore
         mvr 16-06-2008 for pickup-delivery or pickup-drop in case dropfordelivery modalities set pickup date onde day before delivery date and delivbery time to 18.00
         FV  16-06-2008 Clear distance local variable to prevent wrong kilometers when no distance
         PL  18-09-2008 Also set Local_OnlyUseMMPTankSelection=1 when SPO.UseExecutionActions is checked
         PL  19-09-2008 If swap, Use DaysDropBeforeLoading to determine drop-date
         PL  14-10-2008 Added Orders.RateDimensionClearingHouse
         FV  28-10-2008 Tankplanning startdate - Local_DaysDropBeforeLoading is used for drop-drop card from MMP
         PL  30-10-2008 DaysDropBeforeLoading: only count with workin-week (not saturday/sunday)
         MvR 26-11-2008 Fill orderexternalid preorderproduct - ordproduct
         PL  18-12-2008 Fill Orders.MMPEquipmentId with TankId from MMP (if exists)
         FV  14-01-2009 Added 'after' in variable for checking whether tankplanning records after unload should be added after using execution actions
         MvR 22-05-2009 FuelsurchargeType
         MvR 29-05-2009 PreOrderProductECTAcode
         PL  02-07-2009 Added IsNALiquidThermostatSetting
         MvR 08-07-2009 No longer copy ordinvoice lines from SPO - only freight line
         MvR 24-07-2009 fill cmr in ordinvoice
         PL  17-09-2009 Add prefix SPO to DaysDropBeforeLoading
         FV  02-11-2009 When Pla_RouteInstanceId < 0 means the instance was generated from a RouteInstanceException and we should use that table to fetch Route info
         PL  09-11-2009 Use PO.DaysDropBeforeloading instead of SPO.DaysDropBeforeLoading
         MvR 23-12-2009 use data tankplanning for first ordmodality if generated by executionactions
         FV  08-02-2010 Adjusted count of first ordmodality when generated with execution actions
         MvR  3-03-2010 added BL fields to OP
         MvR 18-03-2010 moveto / routes 
         MvR 23-03-2010 moveto / memo
         MvR 25-03-2010 moveto / memo when swap
         MvR 26-03-2010 moveto / memo meme overall
         MvR 30-03-2010 moveto / memo meme overall
         MvR 01-04-2010 moveto uitbreiding
         FV  13-04-2010 Replaced SPO.UseExecutionActions with PO.UseExecutionActions 
                        variable Local_StandardPreOrderUseExecutionActions renamed to Local_PreOrderUseExecutionActions
         FV  19-04-2010 Replaced Clean filling with Exe_MoveToIsclean to DropForClean and added CreateCleaningOrderRequest
         PL  27-04-2010 Only get info from p_mmpTankPlanning when DB-Option "InforIT_MMP_Active" = true
         FV  14-05-2010 Replaced DropFroClean with isDropForCleaning
         WvdS 31-05-2010 Added ClearingHouseRefuseReason and ClearingHouseRemark
         WvdS 01-06-2010 Changed Clearinghouse to check for ClearingHouseStatus
         FV  28-06-2010 Added LCompartment and CompartmentLoaded (default '' for 'DH' other customers stays '1`)
                        Needs to be filled with info from MMP in the future
         MvR 26-30 NOV 2010 
         MvR --  9 dec adding ordmodproducts to modality
         MvR -- 14 dec 
         MvR -- 15 dec 
         MvR -- 17 dec actions 
         FV  27-12-2010 Added copy MaxNumberOfCompartments
         MvR 28 DEC 2010 adjust inserting OMP
         MvR 06 JAN 2011 adjust inserting OMP - must use sequence and not the id's 
         MvR 20 JAN 2011 adjust inserting OMP - check number of SPOE withou a SPOEproduct
         MvR  7 FEB 2011 adding ordmodproducts..... the continueing story.....
         FV   8 feb 2011 Replaced exe_Action with exe_nextaction for delivery check!
         FV   9 feb 2011 Always check on delivery not only when exe_nextaction is delivery but always and ToAction OrdmOdality = 1 (delivery) 
         MF  08-03-2011 see //MF part. error in join solved.
         FV  10-03-2011 added creation OrdContractor record when clearinghouse option available
         FV  24-03-2011 fetch correct last sequence from SPOExecution 
         FV  25-03-2011 FIll correct dates when adding first OrdModality with Move checked (PlannedArrival.... was not filled)
         FV  30-03-2011 Temp disabled Ordcontractor record creation 
         FV  16-05-2011 'F'reight invoice line from Quote subtract other 'F'reight invoice lines 
                        from PreOrder for same OrdProduct if option InforIT_SplitFreightInvoice active
         WvdS 24-05-2011 Changed PumpRequired to PumpRequiredForDelivery and added PumpRequiredForLoading 
         FV  31-05-2011 When Pickup from Route and ABDistance > 600 (DB Option) then additional pickup day - 1
         FV  18-08-2011 SUM(AmountExcl) for Freight invoice lines (subquery cannot return more then 1 row)
         FV  01-09-2011 Added pickup in next OrdModality when drop for delivery in current OrdModality
         PvdL 28-09-2011 Replaced Mod_FromActionID = 0 by exe_action = 'Loading', so Preloaded Modalities (pickup) will be handled the same as Loading
         FV  07-10-2011 Added PreOrdBatchId in PreOrder to Orders; PreOrderProductId in PreOrderProduct -> OrdProduct
         FV  24-10-2011 Added SalesorderInvoiceToId
         MF  16-02-2012 Added workaround for sybase bug (search for this date to find changes).
         MF  29-02-2012 squash ??? bug 
         MvR 7 MAR 2012 add3ed 3 fields to ordproduct, copied from POP
         FV  12-03-2012 Added HarmonizedSystemCodeto OrdProduct, copied from PreOrderProduct
         FV  26-03-2012 Only use positive friehgt lines to solve issue with discount amounts in PreOrderInvoice freight lines
         FV  24-04-2012 Removed moved remarks and redesigned code
                        Added logging
         FV 08-05-2012  Added local_PrimitiveAdvancedContractorSelection to copy SPoContractor records to Ordcontractor records
         WvdS 25 jun 2012 Added FreightInvoiceToId
         FV 09-07-2012 Added contact fields for SPOPBillOfLading
         FV 25-09-2012 Added SalesOrderInvoiceOriginToId
         WvdS 25 sep 2012 Added Test on DEEPDEA - NoLoadingActuals/NoDeliveryActuals
         FV 26-09-2012 Added SwapOnChassis, DropOnChassis en replaced StandardPreOrderSwap with PreOrderSwap 
         FV 30-10-2012 With UseExecutionAction check for changed route in MMP planning
         FV 26-11-2012 Added check on PreOrder for changed route when writing 2 PreOrders the overrule failed
         WvdS 28-11-2012 Added PreferredDepotAddressId
         FV 10-12-2012 Added check on IsNull() for local_ReplaceRouteId
         FV 19-12-2012 Fill MMPReplaceRoute based on InforIT_MMP_Active DB option
         FV 20-03-2013 Copy isRouteLOFO from PreOrder to route OrdModality
         FV 26-03-2013 Drop-Drop card same start date/time and end date/time
         FV 25-06-2013 Added AddressFromId and filling of mod_Addressfrom
         FV 25-09-2013 Added Closing date/time calculation 
         PL 31-12-2013 Added AccountDestination
         FV 07-01-2014 Added AddressFromId when MoveTo OrdModality is created
         FV 17-02-2014 Added AddressFromId ate CreateOrderFromPreorder 6 / 7
         FV 11-08-2014 Changed AddressFromId at CreateOrderFromPreorder 3 from Mod_AddressTo into Exe_MoveToAddressId
         WvdS 28-08-2014 Werklijst 1554 Added MaxContactTemp
         FV 08-09-2014 Added MinmimumMAWP
         MvR 17 SEP 2014 implement local_invoicedepartment
         FV 16-01-2015 Added CallOff functionality (#1536)
         FV 21-01-2015 Added logging
         FV 29-01-2015 Added Stock for MoveTo at generatedby level 8; TankPlanning last added OrDModalityId Stock when PreOrder CallOff is checked
         FV 25-02-2015 Added additional check on Local_PreOrderCallOff when setting Stock in OrdModality
         FV 01-05-2015 Added OrdInvoice creation on SplitRate when PreRevisionSplitRate records available
         FV 06-07-2015 Added valid currency to calculation of split freight amount
         FV 28-08-2015 For split rate use SPO Freight text line when Quote currency is equal to split rate currency otherwise use 'Freight rate'
         FV 14-09-2015 Added check on quote_Rate = 0 for creation of Freight invoice line. No freight invoice lines were create due to last modification
         FV 23-09-2015 Replaced 1000 with 1000.0 due to 0 as result in stead of 0.0115
         FV 26-10-2016 Added OVeerulePlanboardId at start of Chain when applicable
         PL 19-01-2017 TRAN-367: When DB-option "INFORIT_HandlePreferredClearinghouseAsNormalOrder" is set: Don't check Clearinghouse when PreOrder.ClearinghouseStatus = Preferred
         FV 01-03-2017 TRAN-124: After move to Clean set for next OrdModalities Isclean = 1
         MvR 16-03-2017 type of account mngr invoiceline
         FV 10-04-2017 For PreOrder with PreOrdBatchId save additional fields in deepsea OrdModality
         FV 19-04-2017 Remove Lock from Equipment when order is created
         FV 02-05-2017 Added replacing DeepseaRoute when applicable
         FV 10-05-2017 Added IsNull for dos_DeepSeaRouteId
         FV 24-05-2017 Added spo_DeepSeaRouteId to be replaced dos_DeepSeaRouteId
         FV 29-06-2017 Changed determination of spo_DeepSeaRouteId
         FV 05-02-2018 Adjusted clean determination for TRAN-124. 
         FV 16-02-2018 Use Dos_DeparturePortId for Drop action if used
         FV 08-03-2018 Changed Dos_DeparturePortId usage.
         FV 03-04-2018 Changed spo_DeepSeaRouteId into dos_DeepSeaRouteId in compare statement
         FV 09-07-2018 Cchanged dos_routeStatus update after creation of all OrDModalities and adding the OrdModProduct records to determine whether deepsea OrdModality is Full/Empty
         FV 03-10-2018 Added EquipmentMMPInfo
         DR 06-02-2019 if order exists and is void then delete order and set preorder void = 'N'
         PL 18-03-2019 DEVTRAN-356: Fill OrdProduct.DeliveryTerms and Document1 from SPOP.INCOTerms and CustomsStatusGoods
         FV 25-03-2018 Adjusted SkipLogMMP
         PL 27-03-2019 DEVTRAN-356: Use new field OrdProduct.CustomsStatus
         FV 03-04-2019 Added option "INFORIT_MMP_Fill_Equipment_Active"
         DR 20-10-2020 Pickup Execution added
*/
begin atomic
  declare MustInsertOMP integer;
  declare time_begin timestamp;
  declare counter_modality integer;
  declare mmp_startdate date;
  declare mmp_starttime time;
  declare err_notfound exception for sqlstate value '02000';
  declare Local_OnlyUseMMPTankSelection integer;
  declare Local_FirstTankPlanningIdAfterUnload integer;
  declare Local_FirstTankPlanningIdUnload integer;
  declare Load_ModalityIdentifier integer;
  declare UnLoad_ModalityIdentifier integer;
  declare Local_Distance_ABdistance integer;
  declare Local_Distance_ABtime integer;
  declare First_LoadActionId integer;
  declare First_ActionId integer;
  declare Local_Sequence integer;
  declare TankPLanning_TankId integer;
  declare local_equipmentCodefld varchar(10);
  declare TankPlanning_StartDate date;
  declare TankPlanning_StartTime time;
  declare Pla_PreOrderProductId integer;
  declare Local_CheckLDate date;
  declare Local_StandardPreOrderIdentifier integer;
  declare local_PreOrdBatchId integer;
  declare Local_PreOrderCallOff integer;
  declare Local_PreOrderSwap integer;
  declare Local_PreOrderSwapOnChassis integer;
  declare Local_PreOrderUseExecutionActions integer;
  declare local_PreOrderIsRouteLOFO integer;
  declare Local_LatestDropTime time;
  declare Local_DaysDropBeforeLoading integer;
  declare local_OptionClearingHouse varchar(11);
  declare local_OrdContractorId integer;
  declare Local_PreOrderSwapRemark varchar(1023);
  declare Local_PreOrderPreLoaded integer;
  declare Local_PreOrderDropForDelivery integer;
  declare local_PreOrderDropOnChassis integer;
  declare local_PrimitiveAdvancedContractorSelection integer;
  declare local_ExternalClearingHouse integer;
  declare Local_PreOrder_Status integer;
  declare Local_PreOrder_EquipmentId integer;
  declare Quote_QuoteId integer;
  declare Quote_PreRevisionId integer;
  declare Quote_CurrencyId integer;
  declare Quote_RateDimension char(1);
  declare Quote_Rate numeric(10,2);
  declare Quote_MinimumRate numeric(10,2);
  declare Quote_CategoryId integer;
  declare Quote_TransportTermsId integer;
  declare PreOrderProduct_PlaceFromId integer;
  declare PreOrderProduct_PlaceToId integer;
  declare ToId integer;
  declare Local_EquateAddedBySystem integer;
  declare Exe_ActionId integer;
  declare Exe_Sequence integer;
  declare Exe_Action varchar(21);
  declare Exe_MoveActionFrom varchar(21); //added FV 01-09-2011
  declare Exe_MoveActionFromId integer; //added FV 01-09-2011
  declare Exe_MoveToAction varchar(21);
  declare Exe_MoveToActionInt integer;
  declare Exe_MoveToIsclean integer;
  declare Exe_MoveToInternalCleaningrequest integer;
  declare Exe_MoveMemo varchar(1024);
  declare Exe_PickupFromAddressId integer;
  //  declare Exe_Swap                           integer;
  //  declare Exe_PreLoaded                      integer;
  //  declare Exe_DropForDelivery                integer;
  declare Exe_RouteId integer;
  declare Exe_Move integer;
  declare Exe_MoveFromAddressId integer;
  declare Exe_MoveFromLocationPlaceId integer;
  declare Exe_MoveToAddressId integer;
  declare Exe_MoveToLocationPlaceId integer;
  declare FirstOrdProductId integer;
  declare FirstOrdProductCustomerId integer;
  declare FirstOrdProductQuantity integer;
  declare Exe_NextActionId integer;
  declare Exe_NextSequence integer;
  declare Exe_NextAction varchar(21);
  //  declare Exe_NextSwap                       integer;
  //  declare Exe_NextPreLoaded                  integer;
  //  declare Exe_NextDropForDelivery            integer;
  declare Exe_NextRouteId integer;
  declare Exe_NextMove integer;
  declare Exe_NextMoveToAddressId integer;
  declare Exe_PreviousActionId integer;
  declare Exe_PreviousSequence integer;
  declare Exe_PreviousAction varchar(21);
  //  declare Exe_PreviousSwap                   integer;
  //  declare Exe_PreviousPreLoaded              integer;
  //  declare Exe_PreviousDropForDelivery        integer;
  declare Exe_PreviousRouteId integer;
  declare Exe_PreviousMove integer;
  declare Exe_PreviousMoveToAddressId integer;
  declare Exe_PreviousMoveToAction varchar(21);
  declare Pla_Id integer;
  declare Pla_TankId integer;
  declare Pla_PreOrderId integer;
  declare Pla_ActionType varchar(20);
  declare Pla_ArrivalDate date;
  declare Pla_ArrivalTime time;
  declare Pla_StartDate date;
  declare Pla_StartTime time;
  declare Pla_EndDate date;
  declare Pla_EndTime time;
  declare Pla_DepartureDate date;
  declare Pla_DepartureTime time;
  declare Pla_StartAddressId integer;
  declare Pla_EndAddressId integer;
  declare Pla_RouteInstanceId integer;
  declare Pla_Costs numeric(7,2);
  declare Pla_TruckingKm integer;
  declare Pla_NextId integer;
  declare Pla_NextTankId integer;
  declare Pla_NextPreOrderId integer;
  declare Pla_NextActionType varchar(20);
  declare Pla_NextArrivalDate date;
  declare Pla_NextArrivalTime time;
  declare Pla_NextStartDate date;
  declare Pla_NextStartTime time;
  declare Pla_NextEndDate date;
  declare Pla_NextEndTime time;
  declare Pla_NextDepartureDate date;
  declare Pla_NextDepartureTime time;
  declare Pla_NextStartAddressId integer;
  declare Pla_NextEndAddressId integer;
  declare Pla_NextRouteInstanceId integer;
  declare Pla_NextCosts numeric(7,2);
  declare Pla_NextTruckingKm integer;
  declare Pla_NextNextId integer;
  declare Pla_NextNextTankId integer;
  declare Pla_NextNextPreOrderId integer;
  declare Pla_NextNextActionType varchar(20);
  declare Pla_NextNextArrivalDate date;
  declare Pla_NextNextArrivalTime time;
  declare Pla_NextNextStartDate date;
  declare Pla_NextNextStartTime time;
  declare Pla_NextNextEndDate date;
  declare Pla_NextNextEndTime time;
  declare Pla_NextNextDepartureDate date;
  declare Pla_NextNextDepartureTime time;
  declare Pla_NextNextStartAddressId integer;
  declare Pla_NextNextEndAddressId integer;
  declare Pla_NextNextRouteInstanceId integer;
  declare Pla_NextNextCosts numeric(7,2);
  declare Pla_NextNextTruckingKm integer;
  declare Pla_NextOrderActionType varchar(20);
  declare Pla_NextOrderActionTypeId varchar(20);
  declare MustInsertMod integer;
  declare Mod_FromActionID integer;
  declare Mod_FromActionDescription varchar(21);
  declare Mod_ToActionID integer;
  declare Mod_ToActionDescription varchar(21);
  declare Mod_RouteId integer;
  declare Mod_Modalityid integer;
  declare Mod_Creditorid integer;
  declare Mod_Addressfrom integer;
  declare Mod_Addressto integer;
  declare Mod_Placefrom integer;
  declare Mod_Placeto integer;
  declare mod_ClosingDate date;
  declare mod_ClosingTime time;
  declare Mod_StartDate date;
  declare Mod_StartTime time;
  declare Mod_DropStartDate date;
  declare Mod_DropStartTime time;
  declare Mod_EndDate date;
  declare Mod_EndTime time;
  declare Mod_Duration integer;
  declare Mod_ABtime integer;
  declare Mod_ABdistance integer;
  declare Mod_TruckingDuration integer;
  declare Mod_isWithChassisFromAction integer;
  declare Mod_isWithChassisToAction integer;
  declare Mod_isClean integer;
  declare Local_OverrulePlanboardid integer;
  declare pbr_ordmodalityid integer;
  declare pbr_isintermodal integer;
  declare pbr_principalid integer;
  declare pbr_PFcountryid integer;
  declare pbr_PTcountryid integer;
  declare pbr_PFpostalcode varchar(21);
  declare pbr_pumprequired integer;
  declare pbr_TippingChassisRequired integer;
  declare pbr_ProductCategoryid integer;
  declare pbr_ProductId integer;
  declare Mod_PlannedFromActionArrivalDate date;
  declare Mod_PlannedFromActionArrivalTime time;
  declare Mod_PlannedFromActionStartDate date;
  declare Mod_PlannedFromActionStartTime time;
  declare Mod_PlannedFromActionEndDate date;
  declare Mod_PlannedFromActionEndTime time;
  declare Mod_PlannedFromActionDepartureDate date;
  declare Mod_PlannedFromActionDepartureTime time;
  declare Mod_PlannedToActionArrivalDate date;
  declare Mod_PlannedToActionArrivalTime time;
  declare Mod_PlannedToActionStartDate date;
  declare Mod_PlannedToActionStartTime time;
  declare Mod_PlannedToActionEndDate date;
  declare Mod_PlannedToActionEndTime time;
  declare Mod_PlannedToActionDepartureDate date;
  declare Mod_PlannedToActionDepartureTime time;
  declare NextRoute_PlaceFromID integer;
  declare NextRoute_AddressFromID integer;
  declare rou_ClosingTimeRegular integer;
  declare rou_ClosingTimeHazardous integer;
  declare Local_CustomsRemarks long varchar;
  declare Local_MessageToDriver long varchar;
  declare Local_MessageToPlanner long varchar;
  declare Local_ordproductidentifier integer;
  declare Local_NewOrdmodalityIdentifier integer;
  declare Local_FirstPickup_OrdmodalityIdentifier integer;
  declare Local_LastDrop_OrdmodalityIdentifier integer;
  declare LastSPOEAwithProductidentifier integer;
  declare LastSPOEAwithProductSequence integer;
  declare LastSPOEAwithProductFromaction varchar(20);
  declare LastSPOEAwithProductToaction varchar(20);
  declare LastSPOEAwithProductMove integer;
  declare local_CategoryCodeFld varchar(10);
  declare Local_DepartureDateTime datetime; //MF 16-02-2012
  declare local_EnableLogging integer;
  declare Local_FirstUnloadDateTime datetime;
  declare local_FreightInvoiceToId integer;
  declare local_hasHazardousProduct integer;
  declare local_IsDeliveryNoActuals integer;
  declare local_IsLoadNoActuals integer;
  declare Local_MaxArrivalDateTime datetime;
  declare Local_Last_OrdmodalityIdentifier integer;
  declare Local_NextDepartureDateTime datetime; //MF 16-02-2012
  declare Local_OrdmodalityIdentifier integer;
  declare Local_OrdmodalityFromAction integer;
  declare Local_OrdmodalityRouteIdentifier integer;
  declare Local_OrdmodalityToAction integer;
  declare Local_PreOrderProductIdentifier integer;
  declare local_ReplaceRouteId integer; //FV 30-10-2012
  declare local_ReplaceRouteSequence integer; //FV 30-10-2012
  declare Local_SPOExecutionIdentifier integer;
  declare Local_SPOExecutionSequence integer;
  declare Local_TankPlanningActionType varchar(20);
  declare Local_TankPlanningID integer;
  declare local_TransportTermsCodeFld varchar(10);
  declare local_invoicedepartment integer;
  //
  declare dos_ClosingDate date;
  declare dos_ClosingTime time;
  declare dos_VesselId integer;
declare dos_VesselName varchar(100);
  declare dos_VoyageNumber varchar(100);
  declare dos_BookingReference varchar(100);
  declare dos_PickUpReference varchar(100);
  declare dos_DepartureDate date;
  declare dos_DepartureTime time;
  declare dos_ArrivalDate date;
  declare dos_ArrivalTime time;
  declare dos_DeepseaRouteId integer;
  declare dos_DeparturePortId integer;
  declare dos_RouteStatus integer;
  declare spo_DeepseaRouteId integer;
  //
  declare local temporary table OrdModalityActions(
    OrdmodalityIdentifier integer null,
    SPOExecutionIdentifier integer null,
    recordnumber integer null default autoincrement,
    ) on commit delete rows;
  declare local temporary table OrdModalityTankPlanning(
    OrdmodalityIdentifier integer null,
    PreOrderProductIdentifier integer null,
    TankPLanningID integer null,
    TankPlanningActionType varchar(20) null,
    recordnumber integer null default autoincrement,
    ) on commit delete rows;
  declare local temporary table MMPReplaceRoutes(
    Sequence integer null default autoincrement,
    RouteId integer null,
    ReplaceRoute integer null,
    ) on commit delete rows;
  declare local temporary table tmp_OrdModalityStatus(
    OrdModalityId integer,
    Status integer
    ) on commit delete rows;
  //
  declare Cursor_NewModality dynamic scroll cursor for select OrdmodalityIdentifier,SPOExecutionIdentifier
      from OrdModalityActions
        join ordmodality on ordmodality.ordmodalityid = OrdModalityActions.OrdmodalityIdentifier
      order by Browsesequence asc;
  declare Cursor_Modality dynamic scroll cursor for select OrdmodalityId,FromAction,ToAction,RouteId from OrdModality where OrderID = ToID;
  declare Cursor_PreOrderProduct dynamic scroll cursor for select PreOrderProductId from PreOrderProduct where PreOrderID = In_PreOrderIdentifier;
  declare Cursor_Execution dynamic scroll cursor for select StandardPreOrderExecutionId,(Sequence*10),Action,RouteId,Move,MoveToAddressId,MoveToAction,
      (case MoveToAction 
       when 'Loading' then 0
       when 'Delivery' then 1 
       when 'Pickup' then 2 
       when 'Drop' then 3 
       when 'Road' then 4 
       when 'Clean' then 5 
       when 'Depot' then 6 
       end),MoveToIsclean,MoveMemo, PickupFromAddressId 
      from usr.StandardPreOrderExecution
      where StandardPreOrderId = Local_StandardPreOrderIdentifier
      order by sequence asc;
  declare Cursor_TankPlanning dynamic scroll cursor for select Id,TankId,PreOrderId,ActionType,ArrivalDate,ArrivalTime,StartDate,StartTime,
      EndDate,EndTime,DepartureDate,DepartureTime,StartAddressId,EndAddressId,RouteInstanceId,
      Costs,TruckingKm from p_mmptankPlanning where PreOrderId = In_PreOrderIdentifier
      order by ArrivalDate asc,ArrivalTime asc,DepartureDate asc,DepartureTime asc,id asc;
  declare ThisCompany varchar(20);
  //
  select getoption('Inforit_Company') into ThisCompany;
  //
  set temporary option time_format = 'hh:nn';
  set counter_modality = 0;
  set time_begin = current timestamp;
  set Local_EquateAddedBySystem = 2;
  set Local_OnlyUseMMPTankSelection = 0;
  set local_EnableLogging = 1;
  //
  if local_EnableLogging = 1 then
    message '**- NEW -** PreOrder: ' || In_PreOrderIdentifier type info to console
  end if;
  //
  select 
     preorder.StandardPreOrderID
    ,preorder.status
    ,Preorder.swap
    ,Preorder.IsCallOff
    ,Preorder.isSwapOnChassis
    ,Preorder.swapremark
    ,preorder.preloaded
    ,preorder.dropfordelivery
    ,PreOrder.isDropOnChassis
    ,Preorder.EquipmentId
    ,PreOrder.UseExecutionActions
    ,PreOrder.DaysDropBeforeLoading
    ,LatestDropTime
    ,PrimitiveAdvancedContractorSelection
    ,Department.ExternalClearingHouse
    ,StandardPreOrder.FreightInvoiceToId
    ,PreOrder.isRouteLOFO
    ,IsNull(PreOrder.PreOrdBatchId,0)
  into 
     Local_StandardPreOrderIdentifier
    ,Local_PreOrder_Status
    ,Local_PreOrderSwap
    ,Local_PreOrderCallOff
    ,Local_PreOrderSwapOnChassis
    ,Local_PreOrderSwapRemark
    ,Local_PreOrderPreLoaded
    ,Local_PreOrderDropForDelivery
    ,local_PreOrderDropOnChassis
    ,Local_PreOrder_EquipmentId
    ,Local_PreOrderUseExecutionActions
    ,Local_DaysDropBeforeLoading
    ,Local_LatestDropTime
    ,local_PrimitiveAdvancedContractorSelection
    ,local_ExternalClearingHouse
    ,local_FreightInvoiceToId
    ,local_PreOrderIsRouteLOFO
    ,local_PreOrdBatchId
  from usr.PreOrder
       join usr.StandardPreOrder
       join usr.Department on Department.DepartmentId = PreOrder.OperationalDepartmentId
  where PreOrderId = In_PreOrderIdentifier;
  //
  if Local_PreOrder_Status = 0 then
    set Local_PreOrder_Status = 2
  end if;
  //
  if exists(select first StandardPreOrderExecutionid from StandardPreOrderExecution where standardpreorderid = Local_StandardPreOrderIdentifier and move = 1)
    or Local_PreOrderSwap = 1
    or Local_PreOrderUseExecutionActions = 1 then
    set Local_OnlyUseMMPTankSelection = 1
  end if;
  //
  if Local_PreOrder_Status = 3 then
    set TankPlanning_TankId = (select first TankId from p_mmpTankPlanning where preorderid = In_PreOrderIdentifier and tankid is not null order by ArrivalDate asc,ArrivalTime asc,DepartureDate asc,DepartureTime asc,id asc);
    select first ArrivalDate,ArrivalTime
      into TankPlanning_StartDate,TankPlanning_StartTime
      from usr.p_mmpTankPlanning
      where preorderid = In_PreOrderIdentifier and ActionType = 'Load'
      order by ArrivalDate asc,ArrivalTime asc,DepartureDate asc,DepartureTime asc,id asc
  end if;
  //
  select first isnull(LoadingEarliestDate,LoadingLatestDate)
    into Local_CheckLDate
    from usr.PreOrderProduct
    where PreOrderId = In_PreOrderIdentifier and isnull(LoadingEarliestDate,LoadingLatestDate) is not null
    order by isnull(LoadingEarliestDate,LoadingLatestDate) asc;
  //
  select Quote.QuoteId,Quote.RateDimension,Quote.PreRevisionId,GetValidCurrencyId(Quote.CurrencyId),Quote.Rate,Quote.MinimumRate,Quote.CategoryId,Quote.TransportTermsId
    into Quote_QuoteId,Quote_RateDimension,Quote_PreRevisionId,Quote_CurrencyId,Quote_Rate,Quote_MinimumRate,Quote_CategoryId,Quote_TransportTermsId
    from usr.Quote
    where quote.quoteid = (select GetValidQuoteIdForPreOrder(In_PreOrderIdentifier));
  if IsNull(Quote_QuoteId,0) = 0 then
    select first Quote.RateDimension,GetValidCurrencyId(Quote.CurrencyId)
      into Quote_RateDimension,Quote_CurrencyId
      from usr.standardpreorderquote
        join usr.quote
      where standardpreorderid = Local_StandardPreOrderIdentifier
      order by Quote.ValidFrom desc
  end if;
  set local_IsDeliveryNoActuals = 0;
  set local_IsLoadNoActuals = 0;
  select codefld into local_CategoryCodeFld from generalpurpose where generalpurpose.generalpurposeid = Quote_CategoryId;
  if UPPER(local_CategoryCodeFld) = 'DEEPSEA' then
    select GeneralPurpose.CodeFld into local_TransportTermsCodeFld from GeneralPurpose where GeneralPurpose.GeneralPurposeId = Quote_TransportTermsId;
    if UPPER(local_TransportTermsCodeFld) = 'PIER-DOOR'
      or UPPER(local_TransportTermsCodeFld) = 'PIER-PIER' then
      set local_IsLoadNoActuals = 1
    end if;
    if UPPER(local_TransportTermsCodeFld) = 'PIER-PIER'
      or UPPER(local_TransportTermsCodeFld) = 'DOOR-PIER' then
      set local_IsDeliveryNoActuals = 1
    end if
  end if;
  //
  // Fetch Dossier info for deepsea route
  //
  if local_PreOrdBatchId > 0 then 
     select
        LatestClosingDate
       ,LatestClosingTime
       ,VesselId        
       ,VesselName      
       ,VoyageNumber    
       ,BookingReference
       ,PickUpReference 
       ,DepartureDate   
       ,DepartureTime   
       ,ArrivalDate     
       ,ArrivalTime     
       ,DeepseaRouteId  
       ,DeparturePortId 
       ,IsNull(RouteStatus,0)
     into
        dos_ClosingDate     
       ,dos_ClosingTime     
       ,dos_VesselId        
       ,dos_VesselName      
       ,dos_VoyageNumber    
       ,dos_BookingReference
       ,dos_PickUpReference 
       ,dos_DepartureDate   
       ,dos_DepartureTime   
       ,dos_ArrivalDate     
       ,dos_ArrivalTime     
       ,dos_DeepseaRouteId  
       ,dos_DeparturePortId 
       ,dos_RouteStatus     
     from usr.PreOrdBatch
     where PreOrdBatchId = local_PreOrdBatchId          
  else
     set dos_ClosingDate      = null;
     set dos_ClosingTime      = null;
     set dos_VesselId         = null;
     set dos_VesselName       = '';
     set dos_VoyageNumber     = '';
     set dos_BookingReference = '';
     set dos_PickUpReference  = '';
     set dos_DepartureDate    = null;
     set dos_DepartureTime    = null;
     set dos_ArrivalDate      = null;
     set dos_ArrivalTime      = null;
     set dos_DeepseaRouteId   = null;
     set dos_DeparturePortId  = null;
     set dos_RouteStatus      = 0;    //Todo
  end if;
  //
  // fetch first full deepsea route
  //
  select usr.GetSPODeepseaRouteId(Local_StandardPreOrderIdentifier) into spo_DeepSeaRouteId;


  // if order exists and void = Y then delete order and update void to 0 of the preorder
  if exists(select 1 from usr.orders where orderid = In_PreOrderIdentifier and void = 'Y') then
     delete from usr.orders where orderid = In_PreOrderIdentifier;  
     update usr.preorder set void = 0, VoidReason = '' where PreOrderId = In_PreOrderIdentifier;
  end if;


  //
  // create order record
  //
  insert into usr.Orders
    ( RateDimension,
    Description,
    OriginatorId,
    ContactId,
    CurrencyId,
    Rate,
    Remarks,
    QuoteId,
    PreRevisionId,
    PrincipalId,
    PlaceFromId,
    PlaceToId,
    Neutral,
    Account,
    DepartmentId,
    InvoiceDepartment,
    OnlyISOTank,
    MinimumRate,
    Void,
    Categoryid,
    EquipmentId,
    MMPEquipmentId,
    PreAllocateEquipment,
    ClearingHouse,
    ContractorId,
    AgreedRate,
    RateDimensionClearingHouse,
    ContractorCurrencyId,
    NotYetConfirmed,
    CriticalMonitoring,
    PreOrderId,
    FuelSurcharge,
    FuelsurchargeType,
    ClearingHouseRefuseReasonId,
    ClearingHouseRemark,
    MaxNumberOfCompartments,
    PreOrdBatchId,
    SalesorderInvoiceToId, //added FV 24-10
    SalesOrderInvoiceOriginToId, //added FV 25-09
    PreferredDepotAddressId, //Added WvdS 28-11-2012
    AccountDestination )  // 31-12 added
    select Quote_RateDimension,
      Preorder.Description,
      PreOrder.OriginatorId,
      PreOrder.ContactId,
      Quote_CurrencyId,
      Quote_Rate,
      PreOrder.Remarks,
      Quote_QuoteId,
      Quote_PreRevisionId,
      preorder.PrincipalId,
      PreOrder.PlaceFromId,
      PreOrder.PlaceToId,
      standardpreorder.Neutral,
      PreOrder.Account,
      PreOrder.OperationalDepartmentId,
      PreOrder.InvoiceDepartmentId,
      null,
      Quote_MinimumRate,'N',
      null,
      isnull(TankPLanning_TankId,Local_PreOrder_EquipmentId),
      TankPLanning_TankId,
      (if TankPLanning_TankId is not null then 1 else PreOrder.PreAllocateEquipment endif),
//      (if PreOrder.ClearingHouseStatus in( 1,2 ) then 1 else 0 endif),
      (if usr.GetOption('INFORIT_HandlePreferredClearinghouseAsNormalOrder') = 1 then
          (if PreOrder.ClearingHouseStatus = 1 then 1 else 0 endif)
       else
          (if PreOrder.ClearingHouseStatus in (1,2) then 1 else 0 endif)
       end if),
      PreOrder.ContractorId,
      PreOrder.AgreedRate,
      PreOrder.RateDimensionClearingHouse,
      PreOrder.ContractorCurrencyId,
      PreOrder.NotYetConfirmed,
      CriticalMonitoring,
      PreOrder.PreOrderId,
      standardpreorder.FuelSurcharge,preorder.FuelsurchargeType,
      PreOrder.ClearingHouseRefuseReasonId,
      PreOrder.ClearingHouseRemark,
      PreOrder.MaxNumberOfCompartments,
      PreOrder.PreOrdBatchId,
      PreOrder.SalesorderInvoiceToId, //added FV 24-10
      PreORder.SalesOrderInvoiceOriginToId, //added FV 25-09
      PreOrder.PreferredDepotAddressId, //added WvdS 28-11-2012
      PreOrder.AccountDestination // 31-12 added
      from usr.preorder
        join usr.standardpreorder
      where PreOrder.PreOrderId = In_PreOrderIdentifier;
  set ToId = (select @@Identity);
  if IsNull(ToId,0) = 0 then
    return 0
  end if;
  //
  set local_equipmentCodefld = (select codefld from usr.equipment where equipmentid = isnull(TankPLanning_TankId,Local_PreOrder_EquipmentId));
  //
  // Create OrdProduct records
  //
  insert into OrdProduct
    ( OrderId,
    Sequence,
    ProductId,
    CustomerId,
    LDate,
    LTime,
    LTimeLatest,
    LAddressId,
    DDate,
    DTime,
    DTimeLatest,
    DAddressId,
    ExporterId,
    ImporterId,
    ExportClrId,
    ImportClrId,
    Quantity,
    CustomerReference,
    LoadingReference,
    DeliveryReference,
    LoadingTemp,
    AbsoluteMinTemp,
    AbsoluteMaxTemp,
    DeliveryTemp,
    LiquidThermostatSetting,
    MinimumLoadingDate,
    MinimumLoadingTime,
    MaximumLoadingDate,
    MaximumLoadingTime,
    MinimumDeliveryDate,
    MinimumDeliveryTime,
    MaximumDeliveryDate,
    MaximumDeliveryTime,
    PumpRequiredForLoading,
    PumpRequiredForDelivery,
    GeneratorRequired,
    NitrogenUnitRequired,
    TippingChassisRequired,
    ProfitCentre,
    OrderExternalidentifier,
    IsNALiquidThermostatSetting,
    AddressNotify1Id,
    AddressNotify2Id,
    AddressNotify3Id,
    AddressBillOfLadingId,
    BillOfLadingInstructions,
    BillOfLadingType,
    BillOfLadingOriginals,
    BillOfLadingCopies,
    BillOfLadingFreight,
    BillOfLadingPrintIncoterms,
    BillOfLadingProductname,
    ImportRemark,
    ExportRemark,
    LCompartment,
    CompartmentLoaded,
    PreOrderProductId,
    IsplugAtPortOfLoadingCode,
    IsplugOnVesselCode,
    IsplugAtPortOfDischargeCode,
    HarmonizedSystemCode,
    IsLoadNoActuals,
    IsDeliveryNoActuals,
    MaxContactTemp,
    MinimumMAWP , 
    DeliveryTerms, 
    CustomsStatus ) 
    select ToId,
      pop.Sequence,
      pop.ProductId,
      pop.CustomerAddressId,
      pop.LoadingEarliestDate,
      (if pop.LoadingBook = 1 then 'Book' else cast(dateformat(pop.LoadingEarliestTime,'hh:nn') as varchar(255)) endif),
      (if(pop.LoadingLatestTime = pop.LoadingEarliestTime) or(pop.LoadingBook = 1) then null else dateformat(pop.LoadingLatestTime,'hh:nn') endif),
      pop.LoadingAddressId,
      pop.DeliveryEarliestDate,
      (if pop.DeliveryBook = 1 then 'Book' else cast(dateformat(pop.DeliveryEarliestTime,'hh:nn') as varchar(255)) endif),
      (if(pop.DeliveryLatestTime = pop.DeliveryEarliestTime) or(pop.DeliveryBook = 1) then null else dateformat(pop.DeliveryLatestTime,'hh:nn') endif),
      pop.DeliveryAddressId,
      pop.ExporterAddressId,
      pop.ImporterAddressId,
      pop.AddressExportClrId,
      pop.AddressImportClrId,
      pop.Weight,
      pop.CustomerReference,
      pop.LoadingReference,
      pop.DeliveryReference,
      pop.LoadingTemperature,
      pop.AbsoluteMinTemp,
      pop.AbsoluteMaxTemp,
      pop.DeliveryTemperature,
      pop.LiquidThermostatSetting,
      pop.LoadingEarliestDate,
      pop.LoadingEarliestTime,
      pop.LoadingLatestDate,
      pop.LoadingLatestTime,
      pop.DeliveryEarliestDate,
      pop.DeliveryEarliestTime,
      pop.DeliveryLatestDate,
      pop.DeliveryLatestTime,
      pop.PumpRequiredForLoading,
      pop.PumpRequiredForDelivery,
      pop.GeneratorRequired,
      pop.NitrogenUnitRequired,
      pop.TippingChassisRequired,
      pop.ProfitCentre,
      pop.OrderExternalidentifier,
      pop.IsNALiquidThermostatSetting,
      pop.AddressNotify1Id,
      pop.AddressNotify2Id,
      pop.AddressNotify3Id,
      pop.AddressBillOfLadingId,
      pop.BillOfLadingInstructions,
      pop.BillOfLadingType,
      pop.BillOfLadingOriginals,
      pop.BillOfLadingCopies,
      pop.BillOfLadingFreight,
      pop.BillOfLadingPrintIncoterms,
      pop.BillOfLadingProductname,
      pop.ImportRemark,
      pop.ExportRemark,
      if ThisCompany = 'DH' then '' else '1' endif,
      if ThisCompany = 'DH' then '' else '1' endif,
      pop.PreOrderProductId,
      pop.IsplugAtPortOfLoadingCode,
      pop.IsplugOnVesselCode,
      pop.IsplugAtPortOfDischargeCode,
      pop.HarmonizedSystemCode,
      local_IsLoadNoActuals,
      local_IsDeliveryNoActuals,
      pop.MaxContactTemp,
      pop.MinimumMAWP,
      i.Description,
      c.Description 
      from usr.PreOrderProduct pop
      join usr.PreOrder po on po.PreOrderId = pop.PreOrderId 
      join usr.StandardPreOrderProduct spop on spop.StandardPreOrderId = po.StandardPreOrderId 
                                           and spop.Sequence = pop.Sequence
      left outer join INCOTerms i on i.Id = spop.IncotermsId 
      left outer join CustomsStatusGoods c on c.Id = spop.CustomsStatusGoodsId                                            
      where po.PreOrderId = In_PreOrderIdentifier;
  //
  insert into usr.OrdProRequiredCertificate
    ( OrdProductId,
    Type,
    Description ) 
    select(select OrdProductId from usr.OrdProduct where OrderId = ToId and Sequence = PreOrdPro.Sequence),
      Type,
      Description
      from usr.PreOrdProRequiredCertificate as POPReqCer
        join usr.PreOrderProduct as PreOrdPro on PreOrdPro.PreOrderProductId = POPReqCer.PreOrderProductId
      where PreOrdPro.PreOrderId = In_PreOrderIdentifier;
  //
  // Added FV 24-10
  //
  insert into usr.OrdProBillOfLading
    ( OrdProductId,
    Type,
    BillOfLadingType,
    BillOfLadingNumber,
    BillOfLadingOriginals,
    BillOfLadingCopies,
    BillOfLadingFreight,
    BillOfLadingPrintIncoterms,
    ExporterId,
    ContactExporterId,
    ImporterId,
    ContactImporterId,
    AddressNotify1Id,
    ContactNotify1Id,
    AddressNotify2Id,
    ContactNotify2Id,
    AddressNotify3Id,
    ContactNotify3Id,
    AddressBillOfLadingId,
    ContactBillOfLadingId,
    BillOfLadingProductname,
    BillOfLadingInstructions,
    ExportClrId,
    ExportRemark,
    ImportClrId,
    ImportRemark ) 
    select(select OrdProductId from usr.OrdProduct where PreOrderProductId = POPBOL.PreOrderProductId),
      POPBOL.Type,
      POPBOL.BillOfLadingType,
      POPBOL.BillOfLadingNumber,
      POPBOL.BillOfLadingOriginals,
      POPBOL.BillOfLadingCopies,
      POPBOL.BillOfLadingFreight,
      POPBOL.BillOfLadingPrintIncoterms,
      POPBOL.ExporterId,
      POPBOL.ContactExporterId,
      POPBOL.ImporterId,
      POPBOL.ContactImporterId,
      POPBOL.AddressNotify1Id,
      POPBOL.ContactNotify1Id,
      POPBOL.AddressNotify2Id,
      POPBOL.ContactNotify2Id,
      POPBOL.AddressNotify3Id,
      POPBOL.ContactNotify3Id,
      POPBOL.AddressBillOfLadingId,
      POPBOL.ContactBillOfLadingId,
      POPBOL.BillOfLadingProductname,
      POPBOL.BillOfLadingInstructions,
      POPBOL.ExportClrId,
      POPBOL.ExportRemark,
      POPBOL.ImportClrId,
      POPBOL.ImportRemark
      from usr.POPBillOfLading as POPBOL
        join usr.PreOrderProduct as POP on POP.PreOrderProductId = POPBOL.PreOrderProductId
      where POP.PreOrderId = In_PreOrderIdentifier;
  //
  insert into ordproECTAcode( OrdProductId,
    ECTAcodeWhichId,
    ECTAcodeWhatId,
    ECTAcodeWhoId,
    ECTAcodeWhyId,
    ReportToCustomer,
    Remark,
    "Date" ) 
    select(select ordproductid from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = POP.Sequence),
      ECTAcodeWhichId,
      ECTAcodeWhatId,
      ECTAcodeWhoId,
      ECTAcodeWhyId,
      ReportToCustomer,
      Remark,
      "Date"
      from usr.PreOrdProECTAcode as POPE
        join usr.PreOrderProduct as POP on POP.PreOrderProductId = POPE.PreOrderProductId
      where POP.PreOrderId = In_PreOrderIdentifier;
  //
  insert into OrdCost( OrderId,
    CostItemId,
    CreditorId,
    CurrencyId,
    FCosts,
    Price,
    Quantity,
    VatId,
    Status,
    AddedBy,
    Remarks ) 
    select ToId,
      CostItemId,
      CreditorId,
      GetValidCurrencyId(CurrencyId),
      AmountExcl,
      AmountExcl,
      1,
      VatId,'U',
      Local_EquateAddedBySystem,
      Remarks
      from usr.StandardPreOrderCost
      where StandardPreOrderId = Local_StandardPreOrderIdentifier and Local_CheckLDate between ValidFrom and ValidUntil;
  //
  select first OrdProductId,CustomerId,Quantity
    into FirstOrdProductId,FirstOrdProductCustomerId,FirstOrdProductQuantity
    from usr.ordproduct
    where ordproduct.orderid = toid
    order by ordproductid desc;
  //
  // Create OrdInvoice record for Freight from Quote
  //
  set local_invoicedepartment = (select PreOrder.InvoiceDepartmentId from PreOrder where preorderid = In_PreOrderIdentifier);
  //
  // Check for SplitRate solution. FV 30-04-2015
  //
  if exists(select 1 from usr.PreRevisionSplitRate where PreRevisionId = Quote_PreRevisionId) then
     //
     // Add OrdInvoice lines based on SplitRate values
     //
     insert into usr.OrdInvoice(
        OrderId
       ,OrdProductId
       ,CustomerId
       ,Text
       ,CurrencyId
       ,VatId
       ,Amountexcl
       ,Type
       ,NominalId
       ,CMR)
     select
        ToId
       ,FirstOrdProductId
       ,IsNull(PSR.CustomerId,local_FreightInvoiceToId,FirstOrdProductCustomerId)
       ,(if GetValidCurrencyId(PSR.CurrencyId) = Quote_CurrencyId then 
            (select InvoiceTextFreightLine from standardpreorder where standardpreorderid = Local_StandardPreOrderIdentifier)
         else
           'Freight rate'
         endif)
       ,GetValidCurrencyId(PSR.CurrencyId)
       ,(select GetRVATForOrdProduct(local_invoicedepartment,FirstOrdProductCustomerId,FirstOrdProductId))
       ,(if Quote_RateDimension = 'T' then (PSR.Rate*(FirstOrdProductQuantity/1000.0)) else PSR.Rate endif)
       ,'F'
       ,usr.GetOption('InforIT_FreightNominalId')
       ,(select cmr from usr.StandardPreOrder where StandardPreOrderId = Local_StandardPreOrderIdentifier)
     from
       usr.PreRevisionSplitRate PSR
     where
       PreRevisionId = Quote_PreRevisionId;
  else        
     //FV 06-07-2015 When total amount of freight lines on SPO > total amount of quote then dont create an OrdInvoice line for Quote rate
     if Quote_Rate = 0 or (if Quote_RateDimension = 'T' then (Quote_Rate*(FirstOrdProductQuantity/1000.0)) else Quote_Rate endif) - IsNull((select sum(AmountExcl * usr.ConvertCurrencyRate(CurrencyId,Quote_CurrencyId)) from usr.PreOrderInvoice where usr.GetOption('InforIT_SplitFreightInvoice') = 1 and Type = 'F' and PreOrderId = ToId and AmountExcl > 0),0) > 0 then
        insert into usr.OrdInvoice(
           OrderId
          ,OrdProductId
          ,CustomerId
          ,Text
          ,CurrencyId
          ,VatId
          ,Amountexcl
          ,Type
          ,NominalId
          ,CMR)
        values (
           ToId
          ,FirstOrdProductId
          ,IsNull(local_FreightInvoiceToId,FirstOrdProductCustomerId)
          ,(select InvoiceTextFreightLine from standardpreorder where standardpreorderid = Local_StandardPreOrderIdentifier)
          ,Quote_CurrencyId
          ,(select GetRVATForOrdProduct(local_invoicedepartment,FirstOrdProductCustomerId,FirstOrdProductId))
          ,(if Quote_RateDimension = 'T' then(Quote_Rate*(FirstOrdProductQuantity/1000.0)) else Quote_Rate endif)-IsNull((select sum(AmountExcl * usr.ConvertCurrencyRate(CurrencyId,Quote_CurrencyId)) from usr.PreOrderInvoice where usr.GetOption('InforIT_SplitFreightInvoice') = 1 and Type = 'F' and PreOrderId = ToId and AmountExcl > 0),0)
          ,'F'
          ,(select GetOption('InforIT_FreightNominalId'))
          ,(select cmr from StandardPreOrder where StandardPreOrderId = Local_StandardPreOrderIdentifier)
          );
      end if
  end if;
  //
  // Create OrdInvoice records from PreOrder
  //
  insert into OrdInvoice
    ( OrderId,
    OrdProductId,
    CustomerId,
    Text,
    CurrencyId,
    VatId,
    Amountexcl,
    Type,
    NominalId,
    CMR,
    InvoiceSpecificationId,TypeOfAccountManager ) 
    select ToId,
      (select ordproductid from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = PreOrderProduct.Sequence),
      CustomerId,
      Text,
      GetValidCurrencyId(CurrencyId),
      VatId,
      AmountExcl,
      Type,
      NominalId,
      CMR,
      InvoiceSpecificationId,
      TypeOfAccountManager
      from usr.PreOrderInvoice
        left outer join usr.PreOrderProduct
      where PreOrderInvoice.PreOrderId = In_PreOrderIdentifier;
  //
  // create OrdContractor records when applicable (Operational Department should have ExternalClearingHouse checked)
  //
  if local_PrimitiveAdvancedContractorSelection <> 0 and local_ExternalClearingHouse = 1 then
    insert into usr.Ordcontractor
      ( OrderId,
      AddressId,
      ContactId,
      Rate,
      CurrencyId,
      FuelSurchargePercentage) 
      select ToId,
        AddressId,
        ContactId,
        Rate,
        CurrencyId,
        FuelSurchargePercentage
        from SPOContractor
        where StandardPreOrderId = Local_StandardPreOrderIdentifier
  end if;
  //
  if local_EnableLogging = 1 then
    message 'PreOrder: ' || ToId || ', status: ' || local_PreORder_Status type info to console;
    message 'OnlyUseMMPTankSelection: ' || Local_OnlyUseMMPTankSelection type info to console
  end if;
  //
  // Use MMP only for tank selection. OrdModalities created using exectuion actions
  //
  if Local_PreOrder_Status = 2 or IsNull(TankPlanning_TankId,0) = 0 or Local_OnlyUseMMPTankSelection = 1 then
    if Local_PreOrder_Status = 3 and Local_OnlyUseMMPTankSelection = 1 then
       if IsNull(TankPLanning_TankId,Local_PreOrder_EquipmentId,0) > 0 and usr.GetOption('INFORIT_MMP_Fill_Equipment_Active') <> 1 then
          update usr.Equipment
          set SkipLogMMP = 1
             ,mmpPlannedPreOrderId = null
             ,mmpPlannedActionDate = null
             ,mmpPlannedActionTime = null
             ,mmpPlannedActionDescription = null
             ,mmpPlannedAddressId = null
             ,mmpTankIsLocked = null
           where EquipmentId = isnull(TankPLanning_TankId,Local_PreOrder_EquipmentId);
           //
           if not exists(select 1 from usr.EquipmentMMPInfo where EquipmentId = IsNull(TankPLanning_TankId,Local_PreOrder_EquipmentId)) then 
              insert into usr.EquipmentMMPInfo (EquipmentId,SkipLogMMP) 
              select IsNull(TankPLanning_TankId,Local_PreOrder_EquipmentId),1;
           else
              update EquipmentMMPInfo
              set SkipLogMMP = 1
              where EquipmentId = isnull(TankPLanning_TankId,Local_PreOrder_EquipmentId);
           end if;
       end if;
    end if;
    //
    select first StandardPreOrderExecutionID
      into First_LoadActionId
      from usr.StandardPreOrderExecution
      where standardpreorderid = Local_StandardPreOrderIdentifier and Action = 'Loading'
      order by sequence asc;
    //
    // FV 30-10-2012 Fill temp table with Route from MMP
    // FV 19-12-2012 Added check on mmp active
    //
    if usr.GetOption('InforIT_MMP_Active') = '1' then
      insert into MMPReplaceRoutes( RouteId,ReplaceRoute ) 
        select RouteId,IsNull(TCPReplaceRoute,0)
          from usr.p_mmpTankPlanning
          where PreOrderId = In_PreOrderIdentifier and RouteId > 0 and isEmpty = 0
    end if;
    // FV 30-10-2012 set sequence to start replacing route with
    set local_ReplaceRouteSequence = 1;
    //
    if local_EnableLogging = 1 then
      message 'Start looping through SPO executions' type info to console
    end if;
    //
    set Mod_IsClean = 0;
    //
    open Cursor_Execution;
    //
ExecutionLoop: 
    loop
      fetch next Cursor_Execution into Exe_ActionId,Exe_Sequence,Exe_Action,Exe_RouteId,Exe_Move,Exe_MoveToAddressId,Exe_MoveToAction,Exe_MoveToActionInt,Exe_MoveToIsclean,Exe_MoveMemo, Exe_PickupFromAddressId;
      if sqlstate = err_notfound then
        leave ExecutionLoop
      end if;
      //
      if local_EnableLogging = 1 then
        message 'Action: ' || Exe_Action || ', RouteId: ' || Exe_RouteId || ', Move2: ' || Exe_Move || ', Move2Address: ' || Exe_MoveToAddressId || ', Move2Action: ' || Exe_MoveToAction || ', Move2Isclean: ' || Exe_MoveToIsclean ||', Clean: '|| Mod_IsClean ||', PickupFromAddressId: '|| Exe_PickupFromAddressId type info to console
      end if;
      
      //
      set mod_modalityid = null;
      set mod_startdate = null;
      set mod_starttime = null;
      set Mod_DropStartDate = null;
      set Mod_DropStartTime = null;
      set mod_enddate = null;
      set mod_endtime = null;
      set mod_addressfrom = null;
      set mod_addressto = null;
      set mod_routeid = null;
      set mod_placefrom = null;
      set mod_placeto = null;
      set Mod_isWithChassisFromAction = 0;
      set Mod_isWithChassisToAction = 0;
      set Exe_NextActionId = null;
      set Exe_NextSequence = null;
      set Exe_NextAction = null;
      set Exe_NextRouteId = null;
      set Exe_PreviousActionId = null;
      set Exe_PreviousSequence = null;
      set Exe_PreviousAction = null;
      set Exe_PreviousRouteId = null;
      set Exe_PreviousMoveToaction = null;
      //
      if Exe_Move = 1 and Exe_MoveToIsclean = 1 then
        select IsNull(intercompany,0) into Exe_MoveToInternalCleaningRequest from Address where AddressId = Exe_MoveToAddressId
      else
        set Exe_MoveToInternalCleaningRequest = 0
      end if;
      //
      select first B.StandardPreOrderExecutionId,(B.Sequence*10),B.Action,B.RouteId,B.Move,B.MoveToAddressId
        into Exe_NextActionId,Exe_NextSequence,Exe_NextAction,Exe_NextRouteId,Exe_NextMove,Exe_NextMoveToAddressId
        from usr.StandardPreOrderExecution as B
        where B.standardpreorderid = Local_StandardPreOrderIdentifier and(B.sequence*10) > Exe_Sequence
        order by B.sequence asc;
      //
      select first B.StandardPreOrderExecutionId,(B.Sequence*10),B.Action,B.RouteId,B.Move,B.MoveToAddressId,B.MovetoAction
        into Exe_PreviousActionId,Exe_PreviousSequence,Exe_PreviousAction,Exe_PreviousRouteId,Exe_PreviousMove,Exe_PreviousMoveToAddressId,Exe_PreviousMoveToAction
        from usr.StandardPreOrderExecution as B
        where B.standardpreorderid = Local_StandardPreOrderIdentifier and(B.sequence*10) < Exe_Sequence
        order by B.sequence desc;
      //
      if Exe_Action = 'Loading' or Exe_Action = 'Route' then
        set MustInsertMod = 1
      else
        set MustInsertMod = 0
      end if;
      //
      if Exe_Action = 'Delivery' and(Exe_PreviousAction = 'Delivery' or Exe_PreviousAction = 'Route') then
        set MustInsertMod = 1
      end if;
      //
      if Exe_Action = 'Loading' and Exe_NextAction = 'Route' and Local_PreOrderSwap = 1 then
        set MustInsertMod = 0
      end if;
      // DR 20-10-2020 Pickup Execution added
      // ***********************************************************************************
      //
      case Exe_Action
      when 'Pickup' then
         // get first action of preorderexecution
         select first StandardPreOrderExecutionID
           into First_ActionId
         from usr.StandardPreOrderExecution
         where standardpreorderid = Local_StandardPreOrderIdentifier 
         order by sequence asc;
      
         if local_EnableLogging = 1 then message '** Action Pickup : ' || Exe_Action || ' ' || First_ActionId || ' ' || exe_ActionId type info to console  end if;
         // if it is the first action of preorderexecution then create ordmodalities
         if First_ActionId = exe_ActionId then
            if local_EnableLogging = 1 then message '**Next Action  : ' || Exe_NextAction type info to console  end if;
            if local_EnableLogging = 1 then message '**exe_move:' || exe_Move || ' exe_action:' || Exe_Action type info to console  end if;
            // if checkbox Move is checked and Exe_Action = 'clean' then create ordmodality Load - Clean ordmodality and set clean info to from fields
            if exe_Move = 1 then
               Case Exe_MoveToAction 
               when 'Clean' then
                  select LocationPlaceId into Exe_MoveFromLocationPlaceId from address where addressid = Exe_PickupFromAddressId;
                  select locationplaceid into Exe_MoveToLocationPlaceId   from address where addressid = Exe_MoveToAddressId;
                  insert into OrdModality
                     ( OrderId,           BrowseSequence,               ModalityId,                   FromPlaceId,                  ToPlaceId,                    FromAction,                FromActionDescription,
                       ToAction,          ToActionDescription,          AddressFromId,                DepotId,                      DateOriginal,                 TimeOriginal,              Enddate,
                       EndTime,           PlannedFromActionArrivalDate, PlannedFromActionArrivalTime, PlannedFromActionStartDate,   PlannedFromActionStartTime,   PlannedFromActionEndDate,  PlannedFromActionEndTime,
                       PlannedFromActionDepartureDate,   PlannedFromActionDepartureTime,              PlannedToActionArrivalDate,   PlannedToActionArrivalTime,   PlannedToActionStartDate,  PlannedToActionStartTime,
                       PlannedToActionEndDate,           PlannedToActionEndTime,                      PlannedToActionDepartureDate, PlannedToActionDepartureTime, Clean,                     GeneratedBy,
                       Debuginfo,                                       Instructions ) 
                    values
                     ( ToId,              Exe_Sequence+1,               1,                            Exe_MoveFromLocationPlaceId,  Exe_MoveToLocationPlaceId,    2,                         'Pickup',
                       5,                 'Clean',                      Exe_PickupFromAddressId,      Exe_MoveToAddressId,          Mod_DropStartDate,            Mod_DropStartTime,         Mod_DropStartdate,
                       Mod_DropStartTime, Mod_DropStartDate,            Mod_DropStartTime,            Mod_DropStartDate,            Mod_DropStartTime,            Mod_DropStartDate,         Mod_DropStartTime,
                       Mod_DropStartDate,                Mod_DropStartTime,                           Mod_EndDate,                  Mod_EndTime,                  Mod_EndDate,               Mod_EndTime,
                       Mod_EndDate,                      Mod_EndTime,                                 Mod_EndDate,                  Mod_EndTime,                  Mod_isClean,               1,
                       'SP - CreateOrderFromPreorder pickup-clean',     Exe_MoveMemo );
                  // Whats the next action
                  case Exe_NextAction
                  when 'Loading' then
                     set Mod_FromActionID = 5;
                     set Mod_FromActionDescription = 'Clean';
                     set Mod_ToActionID = 0;
                     set Mod_ToActionDescription = 'Loading';
                     
                     // ophalen loadaddres info
                     select first LoadingAddressId,Address.LocationPlaceId,LoadingEarliestDate,LoadingEarliestTime,LoadDuration
                        into Mod_Addressto,Mod_Placeto,Mod_EndDate,Mod_EndTime,Mod_Duration
                     from usr.preorderproduct
                     left outer join usr.Address on preorderproduct.Loadingaddressid = address.addressid
                     where preorderid = In_PreOrderIdentifier
                     and sequence = any(select sequence from standardpreorderproduct join StandardPreOrderExecutionProduct where StandardPreOrderExecutionId = Exe_NextActionId)
                     order by LoadingEarliestDate asc,LoadingEarliestTime asc;


                     select LocationPlaceId into Exe_MoveFromLocationPlaceId from address where addressid = Exe_MoveToAddressId;
                     select locationplaceid into Exe_MoveToLocationPlaceId   from address where addressid = Mod_Addressto;

                     insert into OrdModality
                        ( OrderId,           BrowseSequence,               ModalityId,                   FromPlaceId,                  ToPlaceId,                    FromAction,                FromActionDescription,
                          ToAction,          ToActionDescription,          AddressFromId,                DepotId,                      DateOriginal,                 TimeOriginal,              Enddate,
                          EndTime,           PlannedFromActionArrivalDate, PlannedFromActionArrivalTime, PlannedFromActionStartDate,   PlannedFromActionStartTime,   PlannedFromActionEndDate,  PlannedFromActionEndTime,
                          PlannedFromActionDepartureDate,   PlannedFromActionDepartureTime,              PlannedToActionArrivalDate,   PlannedToActionArrivalTime,   PlannedToActionStartDate,  PlannedToActionStartTime,
                          PlannedToActionEndDate,           PlannedToActionEndTime,                      PlannedToActionDepartureDate, PlannedToActionDepartureTime, Clean,                     GeneratedBy,
                          Debuginfo,         Instructions ) 
                       values
                        ( ToId,              Exe_Sequence+2,               1,                            Exe_MoveFromLocationPlaceId,  Exe_MoveToLocationPlaceId,    Mod_FromActionID,          Mod_FromActionDescription,
                          Mod_ToActionID,    Mod_ToActionDescription,      Exe_PickupFromAddressId,      Exe_PickupFromAddressId,      Mod_DropStartDate,            Mod_DropStartTime,         Mod_DropStartdate,
                          Mod_DropStartTime, Mod_DropStartDate,            Mod_DropStartTime,            Mod_DropStartDate,            Mod_DropStartTime,            Mod_DropStartDate,         Mod_DropStartTime,
                          Mod_DropStartDate,                Mod_DropStartTime,                           Mod_EndDate,                  Mod_EndTime,                  Mod_EndDate,               Mod_EndTime,
                          Mod_EndDate,                      Mod_EndTime,                                 Mod_EndDate,                  Mod_EndTime,                  Mod_isClean,               1,
                          'SP - CreateOrderFromPreorder clean-loading',    Exe_MoveMemo );

                  when 'Delivery' then
                  when 'Route' then
                     set Mod_FromActionID = 5;
                     set Mod_FromActionDescription = 'Clean';
                     set Mod_Modalityid = 3;
                     set Mod_ToActionID = 3;
                     set Mod_ToActionDescription = 'Drop';
                     
                     select routeid,modalityid,creditorid,AddressFrom,placefromid,addressto,placetoid
                        into mod_routeid,mod_modalityid,mod_creditorid,mod_AddressFrom,mod_placefrom,mod_addressto,mod_placeto
                     from usr.route
                     where routeid = Exe_NextRouteId;
                     if local_EnableLogging = 1 then message '** Next Action Route : ' || Exe_NextRouteId || ' ' || mod_AddressFrom || ' ' || mod_placefrom type info to console  end if;
                     
                     //
                     // Replace deepsearoute when different FV 02-05-2017
                     //
                     if IsNull(dos_DeepseaRouteId,0) > 0 and mod_ModalityId = 5 then
                        if Exe_NextRouteId = spo_DeepSeaRouteId and IsNull(dos_DeepseaRouteId,0) <> exe_routeid then   
                           set Exe_NextRouteId = dos_DeepseaRouteId;
                           //
                           select routeid,modalityid,creditorid,addressto,placetoid
                             into mod_routeid,mod_modalityid,mod_creditorid,mod_addressto,mod_placeto
                           from usr.route
                           where routeid = Exe_NextRouteId;
                        end if;
                        //
                        // FV 16-02-2018 Dos_DeparturePortId if filled
                        //
                        set mod_AddressFrom = IsNull(Dos_DeparturePortId,mod_AddressFrom);
                        set mod_PlaceFrom   = (select LocationPlaceId from usr.Address where AddressId = mod_AddressFrom);
                     end if;                  
                     
                     if local_EnableLogging = 1 then message '** Next Action Route : ' || Exe_NextRouteId || ' ' || mod_AddressFrom || ' ' || mod_placefrom type info to console  end if;
                     
                     select LocationPlaceId into Exe_MoveFromLocationPlaceId from address where addressid = Exe_PickupFromAddressId;
                     select locationplaceid into Exe_MoveToLocationPlaceId   from address where addressid = Mod_AddressFrom;

                     insert into OrdModality
                        ( OrderId,           BrowseSequence,               ModalityId,                   FromPlaceId,                  ToPlaceId,                    FromAction,                FromActionDescription,
                          ToAction,          ToActionDescription,          AddressFromId,                DepotId,                      DateOriginal,                 TimeOriginal,              Enddate,
                          EndTime,           PlannedFromActionArrivalDate, PlannedFromActionArrivalTime, PlannedFromActionStartDate,   PlannedFromActionStartTime,   PlannedFromActionEndDate,  PlannedFromActionEndTime,
                          PlannedFromActionDepartureDate,   PlannedFromActionDepartureTime,              PlannedToActionArrivalDate,   PlannedToActionArrivalTime,   PlannedToActionStartDate,  PlannedToActionStartTime,
                          PlannedToActionEndDate,           PlannedToActionEndTime,                      PlannedToActionDepartureDate, PlannedToActionDepartureTime, Clean,                     GeneratedBy,
                          Debuginfo,                        Instructions ) 
                       values
                        ( ToId,              Exe_Sequence+1,               1,                            Exe_MoveFromLocationPlaceId,  Exe_MoveToLocationPlaceId,    Mod_FromActionId,          Mod_FromActionDescription,
                          Mod_ToActionID,    Mod_ToActionDescription,      Exe_PickupFromAddressId,      Exe_PickupFromAddressId,      Mod_DropStartDate,            Mod_DropStartTime,         Mod_DropStartdate,
                          Mod_DropStartTime, Mod_DropStartDate,            Mod_DropStartTime,            Mod_DropStartDate,            Mod_DropStartTime,            Mod_DropStartDate,         Mod_DropStartTime,
                          Mod_DropStartDate,                Mod_DropStartTime,                           Mod_EndDate,                  Mod_EndTime,                  Mod_EndDate,               Mod_EndTime,
                          Mod_EndDate,                      Mod_EndTime,                                 Mod_EndDate,                  Mod_EndTime,                  Mod_isClean,               1,
                          'SP - CreateOrderFromPreorder clean-route', Exe_MoveMemo );
                  
                  end case;
               end case;
            else
               // if checkbox Move is not checked 
               case Exe_NextAction
               when 'Loading' then
                  set Mod_FromActionID = 2;
                  set Mod_FromActionDescription = 'Pickup';
                  set Mod_Modalityid = 1;
                  
                  // ophalen loadaddres info
                  select first LoadingAddressId,Address.LocationPlaceId,LoadingEarliestDate,LoadingEarliestTime,LoadDuration
                     into Mod_Addressto,Mod_Placeto,Mod_EndDate,Mod_EndTime,Mod_Duration
                  from usr.preorderproduct
                  left outer join usr.Address on preorderproduct.Loadingaddressid = address.addressid
                  where preorderid = In_PreOrderIdentifier
                  and sequence = any(select sequence from standardpreorderproduct join StandardPreOrderExecutionProduct where StandardPreOrderExecutionId = Exe_NextActionId)
                  order by LoadingEarliestDate asc,LoadingEarliestTime asc;

                  select LocationPlaceId into Exe_MoveFromLocationPlaceId from address where addressid = Exe_PickupFromAddressId;
                  select locationplaceid into Exe_MoveToLocationPlaceId   from address where addressid = Mod_Addressto;

                  insert into OrdModality
                     ( OrderId,           BrowseSequence,               ModalityId,                   FromPlaceId                ,  ToPlaceId,                    FromAction,                FromActionDescription,
                       ToAction,          ToActionDescription,          AddressFromId,                DepotId,                      DateOriginal,                 TimeOriginal,              Enddate,
                       EndTime,           PlannedFromActionArrivalDate, PlannedFromActionArrivalTime, PlannedFromActionStartDate,   PlannedFromActionStartTime,   PlannedFromActionEndDate,  PlannedFromActionEndTime,
                       PlannedFromActionDepartureDate,   PlannedFromActionDepartureTime,              PlannedToActionArrivalDate,   PlannedToActionArrivalTime,   PlannedToActionStartDate,  PlannedToActionStartTime,
                       PlannedToActionEndDate,           PlannedToActionEndTime,                      PlannedToActionDepartureDate, PlannedToActionDepartureTime, Clean,                     GeneratedBy,
                       Debuginfo,                        Instructions ) 
                    values
                     ( ToId,              Exe_Sequence+1,               1,                            Exe_MoveFromLocationPlaceId,  Exe_MoveToLocationPlaceId,    2,                         'Pickup',
                       0,                 'Loading',                    Exe_PickupFromAddressId,      Exe_PickupFromAddressId,      Mod_DropStartDate,            Mod_DropStartTime,         Mod_DropStartdate,
                       Mod_DropStartTime, Mod_DropStartDate,            Mod_DropStartTime,            Mod_DropStartDate,            Mod_DropStartTime,            Mod_DropStartDate,         Mod_DropStartTime,
                       Mod_DropStartDate,                Mod_DropStartTime,                           Mod_EndDate,                  Mod_EndTime,                  Mod_EndDate,               Mod_EndTime,
                       Mod_EndDate,                      Mod_EndTime,                                 Mod_EndDate,                  Mod_EndTime,                  Mod_isClean,               1,
                       'SP - CreateOrderFromPreorder pickup-loading', Exe_MoveMemo );
               when 'Delivery' then
               when 'Route' then
                  set Mod_FromActionID = 2;
                  set Mod_FromActionDescription = 'Pickup';
                  set Mod_Modalityid = 3;
                  set Mod_ToActionDescription = 'Drop';
                  
                  select routeid,modalityid,creditorid,AddressFrom,placefromid,addressto,placetoid
                     into mod_routeid,mod_modalityid,mod_creditorid,mod_AddressFrom,mod_placefrom,mod_addressto,mod_placeto
                  from usr.route
                  where routeid = Exe_NextRouteId;
                  if local_EnableLogging = 1 then message '** Next Action Route : ' || Exe_NextRouteId || ' ' || mod_AddressFrom || ' ' || mod_placefrom type info to console  end if;
                  
                  //
                  // Replace deepsearoute when different FV 02-05-2017
                  //
                  if IsNull(dos_DeepseaRouteId,0) > 0 and mod_ModalityId = 5 then
                     if Exe_NextRouteId = spo_DeepSeaRouteId and IsNull(dos_DeepseaRouteId,0) <> exe_routeid then   
                        set Exe_NextRouteId = dos_DeepseaRouteId;
                        //
                        select routeid,modalityid,creditorid,addressto,placetoid
                          into mod_routeid,mod_modalityid,mod_creditorid,mod_addressto,mod_placeto
                        from usr.route
                        where routeid = Exe_NextRouteId;
                     end if;
                     //
                     // FV 16-02-2018 Dos_DeparturePortId if filled
                     //
                     set mod_AddressFrom = IsNull(Dos_DeparturePortId,mod_AddressFrom);
                     set mod_PlaceFrom   = (select LocationPlaceId from usr.Address where AddressId = mod_AddressFrom);
                  end if;                  
                  
                  if local_EnableLogging = 1 then message '** Next Action Route : ' || Exe_NextRouteId || ' ' || mod_AddressFrom || ' ' || mod_placefrom type info to console  end if;
                  
                  select LocationPlaceId into Exe_MoveFromLocationPlaceId from address where addressid = Exe_PickupFromAddressId;
                  select locationplaceid into Exe_MoveToLocationPlaceId   from address where addressid = Mod_AddressFrom;

                  insert into OrdModality
                     ( OrderId,           BrowseSequence,               ModalityId,                   FromPlaceId,                  ToPlaceId,                    FromAction,                FromActionDescription,
                       ToAction,          ToActionDescription,          AddressFromId,                DepotId,                      DateOriginal,                 TimeOriginal,              Enddate,
                       EndTime,           PlannedFromActionArrivalDate, PlannedFromActionArrivalTime, PlannedFromActionStartDate,   PlannedFromActionStartTime,   PlannedFromActionEndDate,  PlannedFromActionEndTime,
                       PlannedFromActionDepartureDate,   PlannedFromActionDepartureTime,              PlannedToActionArrivalDate,   PlannedToActionArrivalTime,   PlannedToActionStartDate,  PlannedToActionStartTime,
                       PlannedToActionEndDate,           PlannedToActionEndTime,                      PlannedToActionDepartureDate, PlannedToActionDepartureTime, Clean,                     GeneratedBy,
                       Debuginfo,                        Instructions ) 
                    values
                     ( ToId,              Exe_Sequence+1,               1,                            Exe_MoveFromLocationPlaceId,  Exe_MoveToLocationPlaceId,    2,                         'Pickup',
                       3,                 'Drop',                       Exe_PickupFromAddressId,      Exe_PickupFromAddressId,      Mod_DropStartDate,            Mod_DropStartTime,         Mod_DropStartdate,
                       Mod_DropStartTime, Mod_DropStartDate,            Mod_DropStartTime,            Mod_DropStartDate,            Mod_DropStartTime,            Mod_DropStartDate,         Mod_DropStartTime,
                       Mod_DropStartDate,                Mod_DropStartTime,                           Mod_EndDate,                  Mod_EndTime,                  Mod_EndDate,               Mod_EndTime,
                       Mod_EndDate,                      Mod_EndTime,                                 Mod_EndDate,                  Mod_EndTime,                  Mod_isClean,               1,
                       'SP - CreateOrderFromPreorder pickup-route',     Exe_MoveMemo );
               end case;
            
            end if;
            
         else
            // als pickup voorafgegaan is door iets anders.
         end if;
         
      when 'Loading' then
        select first LoadingAddressId,Address.LocationPlaceId,LoadingEarliestDate,LoadingEarliestTime,LoadDuration
          into Mod_Addressfrom,Mod_PlaceFrom,Mod_StartDate,Mod_StartTime,Mod_Duration
          from usr.preorderproduct
            left outer join usr.Address on preorderproduct.Loadingaddressid = address.addressid
          where preorderid = In_PreOrderIdentifier
          and sequence = any(select sequence from standardpreorderproduct join StandardPreOrderExecutionProduct where StandardPreOrderExecutionId = Exe_ActionId)
          order by LoadingEarliestDate asc,LoadingEarliestTime asc;
        //
        set Mod_FromActionID = 0;
        set Mod_FromActionDescription = 'Loading';
        set Mod_Modalityid = 1;
        //
        if First_LoadActionId = exe_ActionId then
          if Local_PreOrderSwap = 1 then
            set Mod_DropStartDate = usr.CalculatedPrevWorkingDay(Mod_StartDate,Local_DaysDropBeforeLoading);
            //
            if Local_LatestDropTime < '18:00' then
              set Mod_DropStartTime = Local_LatestDropTime
            else
              set Mod_DropStartTime = '18:00'
            end if;
            //
            if Local_PreOrder_Status = 3 then
              if TankPlanning_StartDate is not null then
                set mod_startDate = TankPlanning_StartDate;
                set mod_startTime = TankPlanning_StartTime;
                set Mod_DropStartDate = usr.CalculatedPrevWorkingDay(TankPlanning_StartDate,Local_DaysDropBeforeLoading);
                //
                if Local_LatestDropTime < '18:00' then
                  set Mod_DropStartTime = Local_LatestDropTime
                else
                  set Mod_DropStartTime = '18:00'
                end if
              end if 
            end if;
            //
            insert into OrdModality
              ( OrderId,BrowseSequence,ModalityId,FromPlaceId,ToPlaceId,FromAction,FromActionDescription,ToAction,ToActionDescription,AddressFromId,DepotId,
              DateOriginal,TimeOriginal,Enddate,EndTime,PlannedFromActionArrivalDate,PlannedFromActionArrivalTime,PlannedFromActionStartDate,PlannedFromActionStartTime,
              PlannedFromActionEndDate,PlannedFromActionEndTime,PlannedFromActionDepartureDate,PlannedFromActionDepartureTime,
              PlannedToActionArrivalDate,PlannedToActionArrivalTime,PlannedToActionStartDate,PlannedToActionStartTime,
              PlannedToActionEndDate,PlannedToActionEndTime,PlannedToActionDepartureDate,PlannedToActionDepartureTime,Clean,
              GeneratedBy,Debuginfo ) values
              ( ToId,1,1,Mod_PlaceFrom,Mod_PlaceFrom,3,'Drop',3,'Drop',Mod_AddressFrom,Mod_AddressFrom,
              Mod_DropStartDate,Mod_DropStartTime,Mod_DropStartdate,Mod_DropStartTime,
              Mod_DropStartDate,Mod_DropStartTime,Mod_DropStartDate,Mod_DropStartTime,
              Mod_DropStartDate,Mod_DropStartTime,Mod_DropStartDate,Mod_DropStartTime,
              Mod_EndDate,Mod_EndTime,Mod_EndDate,Mod_EndTime,
              Mod_EndDate,Mod_EndTime,Mod_EndDate,Mod_EndTime,Mod_isClean,
              1,'SP - CreateOrderFromPreorder 1' ) ;
            //
            set Local_NewOrdmodalityIdentifier = (select @@Identity);
            set counter_modality = counter_modality+1;
            //
            insert into OrdModalityActions
              ( OrdmodalityIdentifier,SPOExecutionIdentifier ) values
              ( Local_NewOrdmodalityIdentifier,Exe_ActionId ) 
          end if;
          //
          if Local_PreOrderSwap = 1 then
            set Mod_FromActionID = 2;
            set Mod_FromActionDescription = 'Pickup';
            set Mod_Modalityid = 1;
            set Mod_isWithChassisFromAction = local_PreOrderSwapOnChassis
          end if;
          //
          if Local_PreOrderPreLoaded = 1 then
            set Mod_FromActionID = 2;
            set Mod_FromActionDescription = 'Pickup';
            set Mod_Modalityid = 1
          end if
        end if;
        //
        case Exe_NextAction
        when 'Loading' then
          set Mod_ToActionID = 0;
          set Mod_ToActionDescription = 'Loading';
          //
          select first LoadingAddressId,Address.LocationPlaceId,LoadingEarliestDate,LoadingEarliestTime,LoadDuration
            into Mod_Addressto,Mod_Placeto,Mod_EndDate,Mod_EndTime,Mod_Duration
            from usr.preorderproduct
              left outer join usr.Address on preorderproduct.Loadingaddressid = address.addressid
            where preorderid = In_PreOrderIdentifier
            and sequence = any(select sequence from standardpreorderproduct join StandardPreOrderExecutionProduct where StandardPreOrderExecutionId = Exe_NextActionId)
            order by LoadingEarliestDate asc,LoadingEarliestTime asc;
          //
          if exe_move = 1 then
            set Exe_MoveToLocationPlaceId = (select locationplaceid from address where addressid = Exe_NextMoveToAddressId);
            //
            // FV 07-01-2014 Added AddressFromId
            //
            insert into OrdModality
              ( OrderId,BrowseSequence,ModalityId,CreditorId,AddressFromId,FromPlaceId,ToPlaceId,FromAction,FromActionDescription,ToAction,ToActionDescription,DepotId,GeneratedBy,Debuginfo,isDropForCleaning,CreateCleaningOrderRequest,InstructionGeneral,PlannerMemoGeneral,Clean ) values
              ( ToId,Exe_Sequence+1,1,null,Mod_AddressTo,Mod_PlaceTo,Exe_MoveToLocationPlaceId,2,'Pickup',Exe_MoveToActionInt,Exe_MovetoAction,Exe_NextMoveToAddressId,1,'SP - CreateOrderFromPreorder 2',Exe_MoveToIsclean,Exe_MoveToInternalCleaningRequest,Exe_movememo,Exe_movememo,mod_IsClean ) ;
            //
            set Local_NewOrdmodalityIdentifier = (select @@Identity);
            set counter_modality = counter_modality+1;
            //
            insert into OrdModalityActions
              ( OrdmodalityIdentifier,SPOExecutionIdentifier ) values
              ( Local_NewOrdmodalityIdentifier,Exe_PreviousActionId ) ;
            //
            set mod_Addressfrom = Exe_NextMoveToAddressId;
            set mod_placefrom = Exe_MoveToLocationPlaceId;
            //
            if Exe_MovetoAction = 'Road' then
              set Mod_FromActionID = Exe_MoveToActionInt;
              set Mod_FromActionDescription = Exe_MovetoAction
            else
              set Mod_FromActionID = 2;
              set Mod_FromActionDescription = 'Pickup'
            end if
          end if 
        when 'Delivery' then
          //Different settings for Call off
          if Local_PreOrderCallOff = 1 then
             set Mod_ToActionID = 3;
             set Mod_ToActionDescription = 'Drop';
             set Mod_AddressTo = Mod_AddressFrom;
             set Mod_Placeto   = Mod_PlaceFrom;
             set Mod_EndDate   = null;
             set Mod_EndTime   = null;
             set Mod_Duration  = 0;
          else
             set Mod_ToActionID = 1;
             set Mod_ToActionDescription = 'Delivery';
             //
             select first DeliveryAddressId,Address.LocationPlaceId,DeliveryEarliestDate,DeliveryEarliestTime,UnLoadDuration
               into Mod_Addressto,Mod_Placeto,Mod_EndDate,Mod_EndTime,Mod_Duration
             from usr.preorderproduct join 
                  usr.Address on preorderproduct.Deliveryaddressid = address.addressid
             where preorderid = In_PreOrderIdentifier and 
                   sequence = any(select sequence from standardpreorderproduct join StandardPreOrderExecutionProduct where StandardPreOrderExecutionId = Exe_NextActionId)
             order by DeliveryEarliestDate asc,DeliveryEarliestTime asc
          end if;
          //
          if Local_PreOrderDropForDelivery = 1 then
            set Mod_ToActionID = 3;
            set Mod_ToActionDescription = 'Drop';
            set Mod_Modalityid = 1;
            set Mod_isWithChassisToAction = local_PreOrderDropOnChassis
          end if;
          //
        when 'Route' then
          //
          // FV 30-10-2012 Check whether Route should be replaced
          //
          set local_ReplaceRouteId = 0;
          //
          select(if ReplaceRoute = 1 then RouteId else 0 endif)
            into local_ReplaceRouteId from MMPReplaceRoutes
            where Sequence = local_ReplaceRouteSequence;
          //
          // FV 30-10-2012 if local_ReplaceRouteId found then replace the route
          //
          if IsNull(local_ReplaceRouteId,0) <> 0 then
            //
            if local_EnableLogging = 1 then
              message 'Replace PreviousRoute ' || exe_nextrouteid || ' with route ' || local_ReplaceRouteId || ', sequence: ' || local_ReplaceRouteSequence type info to console
            end if;
            //
            set exe_nextrouteid = local_ReplaceRouteId
          end if;
          //
          select modalityid,creditorid,addressfrom,placefromid
            into mod_modalityid,mod_creditorid,mod_addressto,mod_placeto
          from usr.route
          where routeid = exe_nextrouteid;
          //
          if Local_PreOrderSwap = 1 then
            if exe_move = 1 then
              if Exe_MovetoAction = 'Road' then
                set Mod_FromActionID = Exe_MoveToActionInt;
                set Mod_FromActionDescription = Exe_MovetoAction
              else
                set Mod_FromActionID = 2;
                set Mod_FromActionDescription = 'Pickup'
              end if;
              //
              set Exe_MoveToLocationPlaceId = (select locationplaceid from address where addressid = Exe_MoveToAddressId);
              //
              insert into OrdModality
                ( OrderId,BrowseSequence,ModalityId,FromPlaceId,ToPlaceId,FromAction,FromActionDescription,ToAction,ToActionDescription,AddressFromId,DepotId,
                DateOriginal,TimeOriginal,PlannedFromActionArrivalDate,PlannedFromActionArrivalTime,PlannedFromActionStartDate,PlannedFromActionStartTime,
                PlannedFromActionEndDate,PlannedFromActionEndTime,PlannedFromActionDepartureDate,PlannedFromActionDepartureTime,GeneratedBy,DebugInfo,
                isDropForCleaning,CreateCleaningOrderRequest,InstructionGeneral,PlannerMemoGeneral,Clean ) values
                ( ToId,Exe_Sequence+1,1,Exe_MoveToLocationPlaceId,Mod_PlaceTo,Mod_FromActionID,Mod_FromActionDescription,3,'Drop',Exe_MoveToAddressId,Mod_AddressTo,
                Mod_Startdate,Mod_StartTime,mod_startDate,mod_startTime,mod_startDate,mod_startTime,
                mod_startDate,mod_startTime,mod_startDate,mod_startTime,1,'SP - CreateOrderFromPreorder 3',
                Exe_MoveToIsclean,Exe_MoveToInternalCleaningRequest,exe_movememo,exe_movememo,Mod_IsClean );
              //
            else
              insert into OrdModality
                ( OrderId,BrowseSequence,ModalityId,FromPlaceId,ToPlaceId,FromAction,FromActionDescription,ToAction,ToActionDescription,AddressFromId,DepotId,
                DateOriginal,TimeOriginal,PlannedFromActionArrivalDate,PlannedFromActionArrivalTime,PlannedFromActionStartDate,PlannedFromActionStartTime,
                PlannedFromActionEndDate,PlannedFromActionEndTime,PlannedFromActionDepartureDate,PlannedFromActionDepartureTime,GeneratedBy,DebugInfo,isWithChassisFromAction,Clean ) values
                ( ToId,Exe_Sequence+1,1,Mod_PlaceFrom,Mod_PlaceTo,2,'Pickup',3,'Drop',Mod_AddressFrom,Mod_AddressTo,
                Mod_Startdate,Mod_StartTime,mod_startDate,mod_startTime,mod_startDate,mod_startTime,
                mod_startDate,mod_startTime,mod_startDate,mod_startTime,1,'SP - CreateOrderFromPreorder 4',Mod_isWithChassisFromAction,Mod_IsClean ) 
            end if;
            //
            set Local_NewOrdmodalityIdentifier = (select @@Identity);
            set counter_modality = counter_modality+1;
            //
            insert into OrdModalityActions
              ( OrdmodalityIdentifier,SPOExecutionIdentifier ) values
              ( Local_NewOrdmodalityIdentifier,Exe_ActionId ) ;
            //
            set Mod_PlaceTo   = Mod_PlaceFrom;
            set Mod_AddressTo = Mod_AddressFrom
          end if;
          //
          // Replace deepsearoute when different FV 02-05-2017
          //
          if IsNull(dos_DeepseaRouteId,0) > 0 then
             if mod_modalityid = 5 and exe_nextrouteid = spo_DeepSeaRouteId and IsNull(dos_DeepseaRouteId,0) <> exe_nextrouteid then   
                set exe_nextrouteid = dos_DeepseaRouteId;
                //
                select modalityid,creditorid
                  into mod_modalityid,mod_creditorid
                from usr.route
                where routeid = exe_nextrouteid
             end if;
             //
             // FV 16-02-2018 Dos_DeparturePortId if filled
             // FV 03-04-2018 Changed spo_DeepSeaRouteId into dos_DeepSeaRouteId
             //
             if exe_nextrouteid = dos_DeepSeaRouteId then
                set mod_addressto = IsNull(Dos_DeparturePortId,mod_AddressFrom);
                set mod_PlaceTo   = (select LocationPlaceId from usr.Address where AddressId = mod_AddressTo);
             end if;
          end if;
          //
          set Mod_ToActionID = 3;
          set Mod_ToActionDescription = 'Drop'
        end case; //case Exe_NextAction 
        //
        if exe_move = 1 then
          set Exe_MoveToLocationPlaceId = (select locationplaceid from address where addressid = Exe_MoveToAddressId);
          //
          insert into OrdModality
            ( OrderId,
            BrowseSequence,
            ModalityId,
            DateOriginal,
            TimeOriginal,
            EndDate,
            EndTime,
            FromPlaceId,
            ToPlaceId,
            FromAction,
            FromActionDescription,
            ToAction,
            ToActionDescription,
            AddressFromId,
            DepotId,
            PlannedFromActionArrivalDate,
            PlannedFromActionArrivalTime,
            PlannedFromActionStartDate,
            PlannedFromActionStartTime,
            PlannedFromActionEndDate,
            PlannedFromActionEndTime,
            PlannedFromActionDepartureDate,
            PlannedFromActionDepartureTime,
            PlannedToActionArrivalDate,
            PlannedToActionArrivalTime,
            PlannedToActionStartDate,
            PlannedToActionStartTime,
            PlannedToActionEndDate,
            PlannedToActionEndTime,
            PlannedToActionDepartureDate,
            PlannedToActionDepartureTime,
            GeneratedBy,Debuginfo,isDropForCleaning,CreateCleaningOrderRequest,Clean ) values
            ( ToId,
            Exe_Sequence-1,
            1,
            Mod_StartDate,
            Mod_StartTime,
            Mod_StartDate,
            Mod_StartTime,
            Mod_PlaceFrom,
            Exe_MoveToLocationPlaceId,
            Mod_FromActionID,
            Mod_FromActionDescription,
            Exe_MoveToActionInt,
            Exe_MovetoAction,
            Mod_AddressFrom,
            Exe_MoveToAddressId,
            (if Mod_FromActionID = 0 then mod_startDate else null endif),
            (if Mod_FromActionID = 0 then mod_startTime else null endif),
            (if Mod_FromActionID = 0 then mod_startDate else null endif),
            (if Mod_FromActionID = 0 then mod_startTime else null endif),
            (if Mod_FromActionID = 0 then "date"(dateadd(minute,Mod_Duration,mod_startDate || ' ' || isnull(mod_startTime,'00:00'))) else null endif),
            (if Mod_FromActionID = 0 then dateformat(dateadd(minute,Mod_Duration,mod_startDate || ' ' || isnull(mod_startTime,'00:00')),'hh:nn') else null endif),
            (if Mod_FromActionID = 0 then "date"(dateadd(minute,Mod_Duration,mod_startDate || ' ' || isnull(mod_startTime,'00:00'))) else null endif),
            (if Mod_FromActionID = 0 then dateformat(dateadd(minute,Mod_Duration,mod_startDate || ' ' || isnull(mod_startTime,'00:00')),'hh:nn') else null endif),
            (if Mod_ToActionID = 1 then mod_endDate else null endif),
            (if Mod_ToActionID = 1 then mod_endTime else null endif),
            (if Mod_ToActionID = 1 then mod_endDate else null endif),
            (if Mod_ToActionID = 1 then mod_endTime else null endif),
            (if Mod_ToActionID = 1 then "date"(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00'))) else null endif),
            (if Mod_ToActionID = 1 then dateformat(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00')),'hh:nn') else null endif),
            (if Mod_ToActionID = 1 then "date"(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00'))) else null endif),
            (if Mod_ToActionID = 1 then dateformat(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00')),'hh:nn') else null endif),
            1,'SP - CreateOrderFromPreorder 5',Exe_MoveToIsclean,Exe_MoveToInternalCleaningRequest,Mod_IsClean ) ;
          //
          set Local_NewOrdmodalityIdentifier = (select @@Identity);
          //
          set counter_modality = counter_modality+1;
          //
          insert into OrdModalityActions
            ( OrdmodalityIdentifier,SPOExecutionIdentifier ) values
            ( Local_NewOrdmodalityIdentifier,Exe_ActionId ) ;
          //
          set mod_placefrom = Exe_MoveToLocationPlaceId;
          set mod_addressfrom = Exe_MoveToAddressId;
          if Exe_MovetoAction = 'Road' then
            set Mod_FromActionID = Exe_MoveToActionInt;
            set Mod_FromActionDescription = Exe_MovetoAction
          else
            set Mod_FromActionID = 2;
            set Mod_FromActionDescription = 'Pickup'
          end if
        end if 
      when 'Delivery' then //case Exe_Action
        //
        // Call off quit here
        //
        if Local_PreOrderCallOff = 1 then
           leave ExecutionLoop
        end if;
        //
        set Mod_ToActionID = 1;
        set Mod_ToActionDescription = 'Delivery';
        //
        if Local_PreOrderDropForDelivery = 1 then
          set Mod_ToActionID = 3;
          set Mod_ToActionDescription = 'Drop';
          set Mod_Modalityid = 1;
          set Mod_isWithChassisToAction = Local_PreOrderDropOnChassis
        end if;
        //
        select first DeliveryAddressId,Address.LocationPlaceId,DeliveryEarliestDate,DeliveryEarliestTime,UnLoadDuration
          into Mod_Addressto,Mod_Placeto,Mod_EndDate,Mod_EndTime,Mod_Duration
          from usr.preorderproduct
            join usr.Address on preorderproduct.Deliveryaddressid = address.addressid
          where preorderid = In_PreOrderIdentifier
          and sequence = any(select sequence from standardpreorderproduct join StandardPreOrderExecutionProduct where StandardPreOrderExecutionId = Exe_ActionId)
          order by DeliveryEarliestDate asc,DeliveryEarliestTime asc;
        //
        if Exe_PreviousAction = 'Route' then
          set Mod_Startdate = Mod_Enddate-1;
          set Mod_StartTime = '18:00'
        end if;
        //
        if Exe_NextAction = 'Route' then
          select placefromid,addressfrom
            into NextRoute_PlaceFromID,NextRoute_AddressFromID
            from usr.route
            where routeid = exe_nextrouteid;
          //
          if NextRoute_PlaceFromID <> mod_placeto then
            //FV 05-02-2018 TRAN-149 extension
            if exe_Move = 1 and exe_MovetoAction = 'Clean' then
               set Mod_IsClean = 1
            end if;
            //
            insert into OrdModality
              ( OrderId,
              BrowseSequence,
              ModalityId,
              CreditorID,
              AddressFromId, //FV 17-02-2014
              FromPlaceId,
              ToPlaceId,
              FromAction,
              FromActionDescription,
              ToAction,
              ToActionDescription,
              DepotID,
              Clean,
              GeneratedBy,Debuginfo ) values
              ( ToId,
              (if exe_move = 1 then(Exe_Sequence+2) else(Exe_Sequence+1) endif),
              1,
              null,
              (if exe_move = 1 then Exe_MoveToAddressId else Mod_AddressTo endif), //FV 17-02-2014
              (if exe_move = 1 then(select locationplaceid from address where addressid = Exe_MoveToAddressId) else mod_placeto endif),
              NextRoute_PlaceFromID,
              (if exe_move = 1 then 2 else 1 endif),
              (if exe_move = 1 then 'Pickup' else 'Delivery' endif),
              3,
              'Drop',
              NextRoute_AddressFromID,
              Mod_IsClean,
              1,'SP - CreateOrderFromPreorder 6' ) ;
            //
            set Local_NewOrdmodalityIdentifier = (select @@Identity);
            set counter_modality = counter_modality+1;
            //
            insert into OrdModalityActions
              ( OrdmodalityIdentifier,SPOExecutionIdentifier ) values
              ( Local_NewOrdmodalityIdentifier,Exe_ActionId ) 
          end if end if;
        //
        case Exe_PreviousAction
        when 'Delivery' then
          set Mod_FromActionID = 1;
          set Mod_FromActionDescription = 'Delivery';
          //
          select first DeliveryAddressId,Address.LocationPlaceId,DeliveryEarliestDate,DeliveryEarliestTime,UnLoadDuration
            into Mod_AddressFrom,Mod_PlaceFrom,Mod_StartDate,Mod_StartTime,Mod_Duration
            from usr.preorderproduct
              join usr.Address on preorderproduct.Deliveryaddressid = address.addressid
            where preorderid = In_PreOrderIdentifier
            and sequence = any(select sequence from standardpreorderproduct join StandardPreOrderExecutionProduct where StandardPreOrderExecutionId = Exe_PreviousActionId)
            order by DeliveryEarliestDate asc,DeliveryEarliestTime asc
        when 'Route' then
          set Mod_FromActionID = 2;
          set Mod_FromActionDescription = 'Pickup';
          //
          if exe_previousmove = 1 then
            set mod_placefrom = (select locationplaceid from address where addressid = Exe_PreviousMoveToAddressId);
            set mod_Addressfrom = Exe_PreviousMoveToAddressId
          else
            //
            // FV 30-10-2012 Check whether Route should be replaced
            //
            set local_ReplaceRouteId = 0;
            //
            select(if ReplaceRoute = 1 then RouteId else 0 endif)
              into local_ReplaceRouteId from MMPReplaceRoutes
              where Sequence = local_ReplaceRouteSequence-1;
            //
            // FV 30-10-2012 if local_ReplaceRouteId found then replace the route
            //
            if IsNull(local_ReplaceRouteId,0) <> 0 then
              //
              if local_EnableLogging = 1 then
                message 'Replace PreviousRoute ' || exe_nextrouteid || ' with route ' || local_ReplaceRouteId || ', sequence: ' || local_ReplaceRouteSequence type info to console
              end if;
              //
              set exe_previousrouteid = local_ReplaceRouteId
            end if;
            //
            select modalityid,creditorid,placetoid,addressto
              into mod_modalityid,mod_creditorid,mod_placefrom,mod_addressfrom
            from usr.route
            where routeid = exe_previousrouteid;
            //
            // Replace deepsearoute when different FV 02-05-2017
            //
            if IsNull(dos_DeepseaRouteId,0) > 0 then
               if mod_modalityid = 5 and exe_previousrouteid = spo_DeepSeaRouteId and IsNull(dos_DeepseaRouteId,0) <> exe_previousrouteid then
                  set exe_previousRouteid = dos_DeepseaRouteId;
                  //
                  select modalityid,creditorid,placetoid,addressto
                    into mod_modalityid,mod_creditorid,mod_placefrom,mod_addressfrom
                  from usr.route
                  where routeid = exe_previousrouteid
               end if;
            end if;
          end if;
          //
          if exe_previousmove = 1 and exe_previousmovetoaction = 'Road' then
            set Mod_FromActionID = 4;
            set Mod_FromActionDescription = 'Road'
          end if;
          //
          // check whether distance > 600 then StartDate is additional -1    Added FV 31-05-2011
          //
          select first abdistance
            into mod_ABdistance
          from usr.distance
          where placefrom = mod_placefrom and placeto = mod_placeto
          order by distanceid asc;
          //
          if IsNull(mod_ABDistance,0) > GetOption('Inforit_PickupAdditionalDayKms') and GetOption('Inforit_PickupAdditionalDayKms') > 0 then
            set Mod_Startdate = Mod_Startdate-1
          end if;
          //
          set MustInsertMod = 1
        end case;
        //
        if exe_move = 1 then
          //
          // FV 01-09-2011 when drop for delivery create pickup modality
          //
          if Mod_ToActionId = 3 then
            set Exe_MoveActionFromId = 2;
            set Exe_MoveActionFrom = 'Pickup'
          else
            set Exe_MoveActionFromId = Mod_ToActionID;
            set Exe_MoveActionFrom = Mod_ToActionDescription
          end if;
          //
          set Exe_MoveToLocationPlaceId = (select locationplaceid from address where addressid = Exe_MoveToAddressId);
          //
          // Added FV 01-09-2011 
          //
          insert into OrdModality
            ( OrderId,
            BrowseSequence,
            ModalityId,
            CreditorId, //FV 17-02-2014
            AddressFromId,
            FromPlaceId,
            ToPlaceId,
            FromAction,
            FromActionDescription,
            ToAction,
            ToActionDescription,
            DateOriginal,
            TimeOriginal,
            PlannedFromActionArrivalDate,
            PlannedFromActionArrivalTime,
            PlannedFromActionStartDate,
            PlannedFromActionStartTime,
            PlannedFromActionEndDate,
            PlannedFromActionEndTime,
            PlannedFromActionDepartureDate,
            PlannedFromActionDepartureTime,
            DepotId,
            GeneratedBy,
            Debuginfo,
            isDropForCleaning,
            CreateCleaningOrderRequest,
            Clean,
            InstructionGeneral,PlannerMemoGeneral ) values
            ( ToId,
            Exe_Sequence+1,
            1,
            null,
            Mod_addressto, //FV 17-02-2014
            Mod_PlaceTo,
            Exe_MoveToLocationPlaceId,
            Exe_MoveActionFromId,
            Exe_MoveActionFrom,
            Exe_MoveToActionInt,
            Exe_MovetoAction,
            "date"(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00'))),
            dateformat(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00')),'hh:nn'),
            "date"(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00'))),
            dateformat(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00')),'hh:nn'),
            "date"(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00'))),
            dateformat(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00')),'hh:nn'),
            "date"(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00'))),
            dateformat(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00')),'hh:nn'),
            "date"(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00'))),
            dateformat(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00')),'hh:nn'),
            Exe_MoveToAddressId,
            1,
            'SP - CreateOrderFromPreorder 7',
            Exe_MoveToIsclean,
            Exe_MoveToInternalCleaningRequest,
            Mod_IsClean,
            Exe_Movememo,
            Exe_Movememo ) ;
          //
          set Local_NewOrdmodalityIdentifier = (select @@Identity);
          set counter_modality = counter_modality+1;
          //
        end if
      when 'Route' then //case Exe_Action
        set Mod_FromActionID = 2;
        set Mod_FromActionDescription = 'Pickup';
        set Mod_ToActionID = 3;
        set Mod_ToActionDescription = 'Drop';
        //
        // FV 30-10-2012 Check whether Route should be replaced
        //
        set local_ReplaceRouteId = 0;
        //
        select(if ReplaceRoute = 1 then RouteId else 0 endif)
          into local_ReplaceRouteId from MMPReplaceRoutes
          where Sequence = local_ReplaceRouteSequence;
        //
        set local_ReplaceRouteSequence = local_ReplaceRouteSequence+1;
        //
        // FV 30-10-2012 if local_ReplaceRouteId found then replace the route
        //
        if IsNull(local_ReplaceRouteId,0) <> 0 then
          //
          if local_EnableLogging = 1 then
            message 'Replace Route ' || exe_nextrouteid || ' with route ' || local_ReplaceRouteId || ', sequence: ' || local_ReplaceRouteSequence type info to console
          end if;
          //
          set exe_routeid = local_ReplaceRouteId
        end if;
        //
        select routeid,modalityid,creditorid,AddressFrom,placefromid,addressto,placetoid
          into mod_routeid,mod_modalityid,mod_creditorid,mod_AddressFrom,mod_placefrom,mod_addressto,mod_placeto
        from usr.route
        where routeid = exe_routeid;
        //
        // Replace deepsearoute when different FV 02-05-2017
        //
        if IsNull(dos_DeepseaRouteId,0) > 0 and mod_ModalityId = 5 then
           if exe_routeid = spo_DeepSeaRouteId and IsNull(dos_DeepseaRouteId,0) <> exe_routeid then   
              set exe_routeid = dos_DeepseaRouteId;
              //
              select routeid,modalityid,creditorid,addressto,placetoid
                into mod_routeid,mod_modalityid,mod_creditorid,mod_addressto,mod_placeto
              from usr.route
              where routeid = exe_routeid;
           end if;
           //
           // FV 16-02-2018 Dos_DeparturePortId if filled
           //
           set mod_AddressFrom = IsNull(Dos_DeparturePortId,mod_AddressFrom);
           set mod_PlaceFrom   = (select LocationPlaceId from usr.Address where AddressId = mod_AddressFrom);
        end if;
        //
        if exe_move = 1 then
          set Exe_MoveToLocationPlaceId = (select locationplaceid from address where addressid = Exe_MoveToAddressId);
          //
          // FV 07-01-2014 Added AddressFromId
          //
          insert into OrdModality
            ( OrderId,BrowseSequence,ModalityId,CreditorId,AddressFromId,FromPlaceId,ToPlaceId,FromAction,FromActionDescription,ToAction,ToActionDescription,DepotId,Stock,
            GeneratedBy,Debuginfo,isDropForCleaning,CreateCleaningOrderRequest,Clean,InstructionGeneral,PlannerMemoGeneral ) values
            ( ToId,Exe_Sequence+2,1,null,Mod_AddressTo,Mod_PlaceTo,Exe_MoveToLocationPlaceId,2,'Pickup',Exe_MoveToActionInt,Exe_MovetoAction,Exe_MoveToAddressId,(if Local_PreOrderCallOff = 1 and exe_nextAction = 'Delivery' then 1 else 0 endif),
            1,'SP - CreateOrderFromPreorder 8',Exe_MoveToIsclean,Exe_MoveToInternalCleaningRequest,Mod_IsClean,exe_movememo,exe_movememo ) ;
          //
          set Local_NewOrdmodalityIdentifier = (select @@Identity);
          set counter_modality = counter_modality+1;
          //
          insert into OrdModalityActions
            ( OrdmodalityIdentifier,SPOExecutionIdentifier ) values
            ( Local_NewOrdmodalityIdentifier,Exe_ActionId ) ;
          //
          set Mod_FromActionID = 2;
          set Mod_FromActionDescription = 'Pickup'
        end if;
        //
        if Exe_NextAction = 'Route' then
          //
          // FV 30-10-2012 Check whether Route should be replaced. local_ReplaceRouteSequence already hs been increased with 1
          //
          set local_ReplaceRouteId = 0;
          //
          select(if ReplaceRoute = 1 then RouteId else 0 endif)
            into local_ReplaceRouteId from MMPReplaceRoutes
            where Sequence = local_ReplaceRouteSequence;
          //
          // FV 30-10-2012 if local_ReplaceRouteId found then replace the route
          //
          if IsNull(local_ReplaceRouteId,0) <> 0 then
            //
            if local_EnableLogging = 1 then
              message 'Replace NextRoute ' || exe_nextrouteid || ' with route ' || local_ReplaceRouteId || ', sequence: ' || local_ReplaceRouteSequence type info to console
            end if;
            //
            set exe_nextrouteid = local_ReplaceRouteId
          end if;
          //
          select placefromid,addressfrom
            into NextRoute_PlaceFromID,NextRoute_AddressFromID
          from route 
          where routeid = exe_nextrouteid;
          //
          // Replace deepsearoute when different FV 02-05-2017
          //
          if IsNull(dos_DeepseaRouteId,0) > 0 then
             if mod_modalityid = 5 and exe_nextrouteid = spo_DeepSeaRouteId and IsNull(dos_DeepseaRouteId,0) <> exe_nextrouteid then
                set exe_nextrouteid = dos_DeepseaRouteId;
                //
                select placefromid,addressfrom
                  into NextRoute_PlaceFromID,NextRoute_AddressFromID
                from route 
                where routeid = exe_nextrouteid;
             end if;
          end if;
          //
          // FV 07-01-2014 Added exe_move check and second part with or included
          //
          if(exe_move = 0 and NextRoute_PlaceFromID <> mod_placeto) or(exe_move = 1 and NextRoute_PlaceFromID <> Exe_MOveToLocationPlaceId) then
            if exe_move = 1 and Exe_MovetoAction = 'Road' then
              insert into OrdModality
                ( OrderId,
                BrowseSequence,
                ModalityId,
                CreditorID,
                AddressFromId,
                FromPlaceId,
                ToPlaceId,
                FromAction,
                FromActionDescription,
                ToAction,
                ToActionDescription,
                DepotID,
                Clean,
                GeneratedBy,Debuginfo ) values
                ( ToId,
                (Exe_Sequence+5),
                1,
                null,
                (if exe_move = 1 then Exe_MoveToAddressId else mod_AddressTo endif), //FV 07-01-2014
                (if exe_move = 1 then Exe_MoveToLocationPlaceId else mod_placeto endif),
                NextRoute_PlaceFromID,
                4,
                'Road',
                3,
                'Drop',
                NextRoute_AddressFromID,
                Mod_IsClean,
                1,'SP - CreateOrderFromPreorder 9' ) 
            else
              insert into OrdModality
                ( OrderId,
                BrowseSequence,
                ModalityId,
                CreditorID,
                AddressFromId,
                FromPlaceId,
                ToPlaceId,
                FromAction,
                FromActionDescription,
                ToAction,
                ToActionDescription,
                DepotID,
                Clean,
                GeneratedBy,Debuginfo ) values
                ( ToId,
                (Exe_Sequence+5),
                1,
                null,
                (if exe_move = 1 then Exe_MoveToAddressId else mod_AddressTo endif), //FV 07-01-2014
                (if exe_move = 1 then Exe_MoveToLocationPlaceId else mod_placeto endif),
                NextRoute_PlaceFromID,
                2,
                'Pickup',
                3,
                'Drop',
                NextRoute_AddressFromID,
                Mod_IsClean,
                1,'SP - CreateOrderFromPreorder 10' ) 
            end if;
            //
            set Local_NewOrdmodalityIdentifier = (select @@Identity);
            set counter_modality = counter_modality+1;
            //
            insert into OrdModalityActions
              ( OrdmodalityIdentifier,SPOExecutionIdentifier ) values
              ( Local_NewOrdmodalityIdentifier,Exe_ActionId ) 
          end if
        end if 
      end case; //case Exe_Action
      // ***********************************************************************************
      //
      if MustInsertMod = 1 then
        set counter_modality = counter_modality+1;
        //
        if counter_modality = 1 and usr.GetOption('InforIT_MMP_Active') = 1 then
          select first arrivaldate,dateformat(arrivaltime,'hh:nn')
            into mmp_startdate,mmp_starttime
            from usr.p_mmptankplanning
            where p_mmptankplanning.preorderid = ToId
            order by p_mmptankplanning.preorderid asc,p_mmptankplanning.id asc;
          //
          if mmp_startdate is not null then
            set mod_startdate = mmp_startdate;
            set mod_starttime = mmp_starttime
          end if end if;
        //
        if local_EnableLogging = 1 then
          message 'Insert OM: ' || Mod_FromActionDescription || '-' || Mod_ToActionDescription || ', ' || mod_startdate || ' / ' || mod_starttime type info to console
        end if;
        //
        // determine mod_enddate
        //
        set mod_enddate = (if (exe_action = 'Delivery' or exe_move = 1) then
                              if Local_PreOrderDropForDelivery = 1 then
                                 mod_enddate
                              else
                                 if mod_routeid is not null then
                                    null
                                 else
                                    mod_endDate
                                 endif
                              endif
                           else
                              if exe_action = 'Loading' and exe_nextaction = 'Delivery' then
                                 mod_enddate
                              else
                                 null
                              endif
                           endif);
        set mod_endtime = (if (exe_action = 'Delivery' or exe_move = 1) then
                              if Local_PreOrderDropForDelivery = 1 then
                                 mod_endtime
                              else
                                 mod_endtime
                              endif
                           else
                              if exe_action = 'Loading' and exe_nextaction = 'Delivery' then
                                 mod_endtime
                              else
                                 null
                              endif
                           endif);
        //
        insert into OrdModality(
          OrderId,
          ModalityId,
          CreditorId,
          FromPlaceId,
          ToPlaceId,
          RouteId,
          isRouteLOFO, //FV 20-03-2013 MMP 1269
          FromAction,
          FromActionDescription,
          ToAction,   //10
          ToActionDescription,
          Status,
          browsesequence,
          DateOriginal,
          TimeOriginal,
          EndDate,
          EndTime,
          AddressFromId,
          DepotId,
          Stock,   //20
          PlannedFromActionArrivalDate,
          PlannedFromActionArrivalTime,
          PlannedFromActionStartDate,
          PlannedFromActionStartTime,
          PlannedFromActionEndDate,
          PlannedFromActionEndTime,
          PlannedFromActionDepartureDate,
          PlannedFromActionDepartureTime,
          PlannedToActionArrivalDate,
          PlannedToActionArrivalTime,  //30
          PlannedToActionStartDate,
          PlannedToActionStartTime,
          PlannedToActionEndDate,
          PlannedToActionEndTime,
          PlannedToActionDepartureDate,
          PlannedToActionDepartureTime,
          Clean,
          GeneratedBy,
          Debuginfo,
          InstructionGeneral,  //40
          PlannerMemoGeneral,
          isWithChassisFromAction,
          isWithChassisToAction,
          ClosingDate,
          ClosingTime,
          VesselId,
          VesselName,
          VoyageNumber,
          BookingReference,
          PickupReference  //50
          ) 
        values(
          ToId,
          (if mod_routeid is not null then mod_modalityid else 1 endif),
          (if mod_routeid is not null then mod_creditorid else null endif),
          mod_placefrom,
          mod_placeto,
          mod_routeid,
          (if IsNull(mod_RouteId,0) > 0 then local_PreOrderIsRouteLOFO else 0 endif), //FV 20-03-2013 MMP 1269
          Mod_FromActionID,
          Mod_FromActionDescription,
          Mod_ToActionID,    //10
          Mod_ToActionDescription,
          0,
          exe_sequence,
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then IsNull(dos_DepartureDate,mod_startdate) else mod_startdate endif),
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then IsNull(dos_DepartureTime,mod_starttime) else mod_starttime endif),
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then IsNull(dos_ArrivalDate  ,mod_enddate) else mod_enddate endif),
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then IsNull(dos_ArrivalTime  ,mod_endtime) else mod_endtime endif),
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then IsNull(Dos_DeparturePortId,mod_AddressFrom) else mod_AddressFrom endif),
          mod_addressto,
          (if Local_PreOrderCallOff = 1 and exe_nextAction = 'Delivery' then 1 else 0 endif),    //20
          (if exe_action = 'Loading' then mod_startDate else null endif),
          (if exe_action = 'Loading' then mod_startTime else null endif),
          (if exe_action = 'Loading' then mod_startDate else null endif),
          (if exe_action = 'Loading' then mod_startTime else null endif),
          (if exe_action = 'Loading' then "date"(dateadd(minute,Mod_Duration,mod_startDate || ' ' || isnull(mod_startTime,'00:00'))) else null endif),
          (if exe_action = 'Loading' then dateformat(dateadd(minute,Mod_Duration,mod_startDate || ' ' || isnull(mod_startTime,'00:00')),'hh:nn') else null endif),
          (if exe_action = 'Loading' then "date"(dateadd(minute,Mod_Duration,mod_startDate || ' ' || isnull(mod_startTime,'00:00'))) else null endif),
          (if exe_action = 'Loading' then dateformat(dateadd(minute,Mod_Duration,mod_startDate || ' ' || isnull(mod_startTime,'00:00')),'hh:nn') else null endif),
          (if Mod_ToActionID = 1 then mod_endDate else null endif),
          (if Mod_ToActionID = 1 then mod_endTime else null endif),
          (if Mod_ToActionID = 1 then mod_endDate else null endif),
          (if Mod_ToActionID = 1 then mod_endTime else null endif),
          (if Mod_ToActionID = 1 then "date"(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00'))) else null endif),
          (if Mod_ToActionID = 1 then dateformat(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00')),'hh:nn') else null endif),
          (if Mod_ToActionID = 1 then "date"(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00'))) else null endif),
          (if Mod_ToActionID = 1 then dateformat(dateadd(minute,Mod_Duration,mod_endDate || ' ' || isnull(mod_endTime,'00:00')),'hh:nn') else null endif),
          Mod_IsClean,
          1,
          'SP - CreateOrderFromPreorder 11 - ' || local_equipmentCodefld,
          exe_movememo,
          exe_movememo,
          Mod_isWithChassisFromAction,
          Mod_isWithChassisToAction,
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then Dos_ClosingDate      else null endif),
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then Dos_ClosingTime      else null endif),
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then Dos_VesselId         else null endif),
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then Dos_VesselName       else ''   endif),
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then Dos_VoyageNumber     else ''   endif),
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then Dos_BookingReference else ''   endif),
          (if IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then Dos_PickupReference  else ''   endif)
          ) ;
        //
        set Local_NewOrdmodalityIdentifier = (select @@Identity);
        //
        if Dos_RouteStatus > 0 and IsNull(mod_RouteId,0) > 0 and IsNull(mod_routeId,0) = IsNull(Dos_DeepseaRouteId,0) then 
           insert into tmp_OrdModalityStatus (OrdModalityId,Status) values (Local_NewOrdmodalityIdentifier,dos_RouteStatus)
         end if;
        //
        if Exe_ActionId is not null then
          insert into OrdModalityActions( OrdmodalityIdentifier,SPOExecutionIdentifier ) values( Local_NewOrdmodalityIdentifier,Exe_ActionId ) 
        end if;
        //
        //FV 01-03-2017 TRAN-149
        if exe_Move = 1 and exe_MovetoAction = 'Clean' then
           set Mod_IsClean = 1
        end if;
        //
        if Exe_NextActionId is not null then
          if Mod_FromActionID = 0 and Mod_ToActionID <> 0 then
            insert into OrdModalityActions
              ( OrdmodalityIdentifier,SPOExecutionIdentifier ) values
              ( Local_NewOrdmodalityIdentifier,Exe_NextActionId ) 
          end if
        end if //if MustInsertMod = 1
      end if
    end loop ExecutionLoop; 
    //
    close Cursor_Execution;
    //
    // FV changed 24-03-2011
    //    set LastSPOEAwithProductidentifier=(select max(StandardPreOrderExecution.StandardPreOrderExecutionid) from StandardPreOrderExecution join StandardPreOrderExecutionProduct where standardpreorderid = Local_StandardPreOrderIdentifier);
    //    select StandardPreOrderExecution.Sequence,StandardPreOrderExecution.Move into LastSPOEAwithProductSequence,LastSPOEAwithProductMove from StandardPreOrderExecution where StandardPreOrderExecutionid = LastSPOEAwithProductidentifier;
    select first StandardPreOrderExecution.Sequence,StandardPreOrderExecution.Move,StandardPreOrderExecution.StandardPreOrderExecutionid
      into LastSPOEAwithProductSequence,LastSPOEAwithProductMove,LastSPOEAwithProductidentifier
      from usr.StandardPreOrderExecution
        join usr.StandardPreOrderExecutionProduct
      where standardpreorderid = Local_StandardPreOrderIdentifier
      order by StandardPreOrderExecution.Sequence desc;
    // END FV changed 24-03-2011
    //
    set MustInsertOMP = 0;
    //
    open Cursor_NewModality;
    //
NewModalityLoop: 
    loop
      fetch next Cursor_NewModality into Local_OrdmodalityIdentifier,Local_SPOExecutionIdentifier;
      if sqlstate = err_notfound then
        leave NewModalityLoop
      end if;
      //
      set Local_SPOExecutionsequence = (select sequence from standardpreorderexecution where standardpreorderexecutionid = Local_SPOExecutionIdentifier);
      //
      select fromactiondescription,toactiondescription
        into LastSPOEAwithProductFromaction,LastSPOEAwithProductToaction
        from usr.ordmodality
        where ordmodalityid = Local_OrdmodalityIdentifier;
      //
      if LastSPOEAwithProductFromaction = 'Delivery' and LastSPOEAwithProductMove = 1 then
        set MustInsertOMP = 2
      end if;
      //
      insert into ordmodproduct
        ( OrdModalityId,
        OrdProductId,
        ExporterId,
        ImporterId,
        ExportClrId,
        ImportClrId,owner ) 
        select Local_OrdmodalityIdentifier,
          (select ordproductid from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = StandardPreOrderProduct.Sequence),
          (select ExporterId from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = StandardPreOrderProduct.Sequence),
          (select ImporterId from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = StandardPreOrderProduct.Sequence),
          (select ExportClrId from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = StandardPreOrderProduct.Sequence),
          (select ImportClrId from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = StandardPreOrderProduct.Sequence),
          Local_SPOExecutionIdentifier || '#' || LastSPOEAwithProductidentifier || '##' || MustInsertOMP || '###' || LastSPOEAwithProductFromaction
          from usr.StandardPreOrderExecutionProduct
            left outer join usr.StandardPreOrderProduct
          where StandardPreOrderExecutionId = Local_SPOExecutionIdentifier
          and not exists(select 1 from ordmodproduct
            where OrdModalityId = Local_OrdmodalityIdentifier
            and OrdProductId = (select ordproductid from ordproduct where ordproduct.orderid = toid
              and ordproduct.sequence = StandardPreOrderProduct.Sequence))
          and((Local_SPOExecutionSequence < LastSPOEAwithProductSequence) or(Local_SPOExecutionSequence = LastSPOEAwithProductSequence and MustInsertOMP = 0));
      //
      if(select count() from usr.standardpreorderexecution as SPOE
          where SPOE.standardpreorderid = Local_StandardPreOrderIdentifier
          and not SPOE.standardpreorderexecutionid = any(select standardpreorderexecutionid from standardpreorderexecutionproduct)) > 0 then
        if Local_SPOExecutionsequence = LastSPOEAwithProductsequence and LastSPOEAwithProductFromaction not in( 'Delivery','Loading' ) then
          set MustInsertOMP = 1
        end if
      //
      end if
    end loop NewModalityLoop; 
    //
    close Cursor_NewModality
  end if; //  if Local_PreOrder_Status = 2 or IsNull(TankPlanning_TankId,0) = 0 or Local_OnlyUseMMPTankSelection = 1 then
  //
  if Local_PreOrder_Status = 3 then
     if IsNull(TankPLanning_TankId,Local_PreOrder_EquipmentId,0) > 0 and usr.GetOption('INFORIT_MMP_Fill_Equipment_Active') <> 1 then
        update usr.Equipment
        set SkipLogMMP = 1
           ,mmpPlannedPreOrderId = null
           ,mmpPlannedActionDate = null
           ,mmpPlannedActionTime = null
           ,mmpPlannedActionDescription = null
           ,mmpPlannedAddressId = null
           ,mmpTankIsLocked = null
         where EquipmentId = isnull(TankPLanning_TankId,Local_PreOrder_EquipmentId);
        //
        if not exists(select 1 from usr.EquipmentMMPInfo where EquipmentId = IsNull(TankPLanning_TankId,Local_PreOrder_EquipmentId)) then 
           insert into usr.EquipmentMMPInfo (EquipmentId,SkipLogMMP) 
           select IsNull(TankPLanning_TankId,Local_PreOrder_EquipmentId),1;
        else
           update usr.EquipmentMMPInfo
           set SkipLogMMP = 1
           where EquipmentId = isnull(TankPLanning_TankId,Local_PreOrder_EquipmentId);
        end if;
     end if;
  end if;
  //
  if Local_PreOrder_Status = 3 then
    set Local_MaxArrivalDateTime = (select max(ArrivalDate+IsNull(ArrivalTime,'00:00:00')) from p_mmpTankplanning where preorderid = In_PreOrderIdentifier); //MF 16-02-2012
    set Pla_NextOrderActionType = (select first ActionType from usr.p_mmpTankPlanning
        where TankId = TankPlanning_TankId and Preorderid <> In_PreOrderIdentifier and(ArrivalDate+IsNull(ArrivalTime,'00:00:00')) > (Local_MaxArrivalDateTime)
        order by ArrivalDate asc,ArrivalTime asc,DepartureDate asc,DepartureTime asc,Id asc);
    //
    case Pla_NextOrderActionType
    when 'Load' then
      set Pla_NextOrderActionTypeId = 0
    when 'Unload' then
      set Pla_NextOrderActionTypeId = 1
    when 'Pickup' then
      set Pla_NextOrderActionTypeId = 2
    when 'Drop' then
      set Pla_NextOrderActionTypeId = 3
    when 'Cleaning' then
      set Pla_NextOrderActionTypeId = 5
    when 'Depot' then
      set Pla_NextOrderActionTypeId = 6
    end case;
    //
    if Local_OnlyUseMMPTankSelection = 1 then //MF 16-02-2012
      set Local_FirstUnloadDateTime = (select(DepartureDate+IsNull(DepartureTime,'00:00:00')) from p_mmpTankPlanning where Id = Local_FirstTankPlanningIdUnload);
      set Local_FirstTankPlanningIdUnload = (select first id from p_mmpTankPlanning where ActionType = 'Unload' and TankId = TankPlanning_TankId and Preorderid = In_PreOrderIdentifier order by ArrivalDate desc,ArrivalTime desc,DepartureDate desc,DepartureTime desc,id desc);
      set Local_FirstTankPlanningIdAfterUnload = (select first id from p_mmpTankPlanning
          where Id <> Local_FirstTankPlanningIdUnload and(ArrivalDate+IsNull(ArrivalTime,'00:00:00')) >= (Local_FirstUnloadDateTime)
          and TankId = TankPlanning_TankId and Preorderid = In_PreOrderIdentifier
          order by ArrivalDate asc,ArrivalTime asc,DepartureDate asc,DepartureTime asc,id asc)
    end if;
    //
    if local_EnableLogging = 1 then
      message 'Loop through tankplanning records' type info to console
    end if;
    //
    // FV 21-01-2015 Log tankplanning records in TF
    //
    insert into usr.Write2TCPLog (
      TankId,PreOrderId,ActionType,ArrivalDate,ArrivalTime,StartDate,StartTime,EndDate,EndTime,DepartureDate,DepartureTime,StartAddressId,EndAddressId,RouteInstanceId,Costs,TruckingKm,IsEmpty,DropForCleaning,RouteId,IsRecleaning)
    select 
      TankId,PreOrderId,ActionType,ArrivalDate,ArrivalTime,StartDate,StartTime,EndDate,EndTime,DepartureDate,DepartureTime,StartAddressId,EndAddressId,RouteInstanceId,Costs,TruckingKm,IsEmpty,DropForCleaning,RouteId,IsRecleaning
    from usr.p_mmptankPlanning 
    where PreOrderId = In_PreOrderIdentifier
    order by ArrivalDate,ArrivalTime,DepartureDate,DepartureTime,id;
    //
    open Cursor_TankPlanning;
    //
TankPlanningLoop: 
    loop
      fetch next Cursor_TankPlanning into Pla_Id,Pla_TankId,Pla_PreOrderId,Pla_ActionType,Pla_ArrivalDate,Pla_ArrivalTime,
        Pla_StartDate,Pla_StartTime,Pla_EndDate,Pla_EndTime,Pla_DepartureDate,Pla_DepartureTime,
        Pla_StartAddressId,Pla_EndAddressId,Pla_RouteInstanceId,Pla_Costs,Pla_TruckingKm;
      if sqlstate = err_notfound then
        leave TankPlanningLoop
      end if;
      //
      set mod_modalityid = null;
      set mod_startdate = null;
      set mod_starttime = null;
      set mod_enddate = null;
      set mod_endtime = null;
      set mod_addressfrom = null;
      set mod_addressto = null;
      set mod_routeid = null;
      set mod_placefrom = null;
      set mod_placeto = null;
      set Mod_ABtime = null;
      set Mod_ABdistance = null;
      set Pla_NextId = null;
      set Pla_NextTankId = null;
      set Pla_NextPreOrderId = null;
      set Pla_NextActionType = null;
      set Pla_NextArrivalDate = null;
      set Pla_NextArrivalTime = null;
      set Pla_NextStartDate = null;
      set Pla_NextStartTime = null;
      set Pla_NextEndDate = null;
      set Pla_NextEndTime = null;
      set Pla_NextDepartureDate = null;
      set Pla_NextDepartureTime = null;
      set Pla_NextStartAddressId = null;
      set Pla_NextEndAddressId = null;
      set Pla_NextRouteInstanceId = null;
      set Pla_NextCosts = null;
      set Pla_NextTruckingKm = null;
      set Pla_NextNextId = null;
      set Pla_NextNextTankId = null;
      set Pla_NextNextPreOrderId = null;
      set Pla_NextNextActionType = null;
      set Pla_NextNextArrivalDate = null;
      set Pla_NextNextArrivalTime = null;
      set Pla_NextNextStartDate = null;
      set Pla_NextNextStartTime = null;
      set Pla_NextNextEndDate = null;
      set Pla_NextNextEndTime = null;
      set Pla_NextNextDepartureDate = null;
      set Pla_NextNextDepartureTime = null;
      set Pla_NextNextStartAddressId = null;
      set Pla_NextNextEndAddressId = null;
      set Pla_NextNextRouteInstanceId = null;
      set Pla_NextNextCosts = null;
      set Pla_NextNextTruckingKm = null;
      set Local_DepartureDateTime = (Pla_DepartureDate+IsNull(Pla_DepartureTime,'00:00:00')); //MF 16-02-2012
      //
      select first Id,TankId,PreOrderId,ActionType,ArrivalDate,ArrivalTime,StartDate,StartTime,EndDate,EndTime,DepartureDate,DepartureTime,
        StartAddressId,EndAddressId,RouteInstanceId,Costs,TruckingKm
        into Pla_NextId,Pla_NextTankId,Pla_NextPreOrderId,Pla_NextActionType,Pla_NextArrivalDate,Pla_NextArrivalTime,Pla_NextStartDate,Pla_NextStartTime,
        Pla_NextEndDate,Pla_NextEndTime,Pla_NextDepartureDate,Pla_NextDepartureTime,Pla_NextStartAddressId,Pla_NextEndAddressId,
        Pla_NextRouteInstanceId,Pla_NextCosts,
        Pla_NextTruckingKm
        from usr.p_mmpTankPlanning as pA
        where pA.preorderid = in_PreOrderIdentifier and pA.Id <> Pla_Id and(pA.ArrivalDate+IsNull(pA.ArrivalTime,'00:00:00')) >= (Local_DepartureDateTime)
        //MF 16-02-2012
        order by pA.ArrivalDate asc,pA.ArrivalTime asc,pA.DepartureDate asc,pA.DepartureTime asc,pA.id asc;
      //
      set Local_NextDepartureDateTime = (Pla_NextDepartureDate+IsNull(Pla_NextDepartureTime,'00:00:00')); //MF 16-02-2012
      //
      select first Id,TankId,PreOrderId,ActionType,ArrivalDate,ArrivalTime,StartDate,StartTime,EndDate,EndTime,DepartureDate,DepartureTime,
        StartAddressId,EndAddressId,RouteInstanceId,Costs,TruckingKm
        into Pla_NextNextId,Pla_NextNextTankId,Pla_NextNextPreOrderId,Pla_NextNextActionType,Pla_NextNextArrivalDate,Pla_NextNextArrivalTime,
        Pla_NextNextStartDate,Pla_NextNextStartTime,Pla_NextNextEndDate,Pla_NextNextEndTime,Pla_NextNextDepartureDate,Pla_NextNextDepartureTime,
        Pla_NextNextStartAddressId,Pla_NextNextEndAddressId,Pla_NextNextRouteInstanceId,Pla_NextNextCosts,
        Pla_NextNextTruckingKm
        from usr.p_mmpTankPlanning as pB
        where pB.preorderid = in_PreOrderIdentifier and pB.Id <> Pla_NextId
        and(pB.ArrivalDate+IsNull(pB.ArrivalTime,'00:00:00')) >= (Local_NextDepartureDateTime)
        //MF 16-02-2012
        order by pB.ArrivalDate asc,pB.ArrivalTime asc,pB.DepartureDate asc,pB.DepartureTime asc,pB.id asc;
      //
      if local_EnableLogging = 1 then
        message 'Current record : ' || Pla_ActionType || ' ' || Pla_StartDate || '/' || Pla_StartTime || ' - ' || Pla_EndDate || '/' || Pla_Endtime type info to console;
        message '    Next record    : ' || Pla_NextActionType || ' ' || Pla_NextStartDate || '/' || Pla_NextStartTime || ' - ' || Pla_NextEndDate || '/' || Pla_NextEndtime type info to console;
        message '    NextNext record: ' || Pla_NextNextActionType || ' ' || Pla_NextNextStartDate || '/' || Pla_NextNextStartTime || ' - ' || Pla_NextNextEndDate || '/' || Pla_NextNextEndtime type info to console
      end if;
      //
      if Pla_ActionType = 'Route' or Pla_ActionType = 'Pickup' or Pla_ActionType = 'Cleaning'
        or(Pla_ActionType = 'Load' and Pla_NextActionType <> 'Load')
        or(Pla_ActionType = 'Unload' and Pla_NextActionType <> 'Unload' and Pla_NextActionType is not null)
        or(Pla_ActionType = 'Trucking' and Pla_NextActionType = 'Trucking') then
        set MustInsertMod = 1
      else
        set MustInsertMod = 0
      end if;
      //
      if Pla_ActionType = 'Cleaning' and Pla_NextActionType = 'Pickup' then
        set MustInsertMod = 0
      end if;
      //
      if IsNull(Pla_NextId,0) = 0 then
        set Local_NextDepartureDateTime = (Pla_NextDepartureDate+IsNull(Pla_NextDepartureTime,'00:00:00')); //MF 16-02-2012
        set Local_DepartureDateTime = (Pla_DepartureDate+IsNull(Pla_DepartureTime,'00:00:00')); //MF 16-02-2012
        //
        select first Id,TankId,PreOrderId,ActionType,ArrivalDate,ArrivalTime,StartDate,StartTime,EndDate,EndTime,DepartureDate,DepartureTime,
          StartAddressId,EndAddressId,RouteInstanceId,Costs,TruckingKm
          into Pla_NextId,Pla_NextTankId,Pla_NextPreOrderId,Pla_NextActionType,Pla_NextArrivalDate,Pla_NextArrivalTime,Pla_NextStartDate,
          Pla_NextStartTime,Pla_NextEndDate,Pla_NextEndTime,Pla_NextDepartureDate,Pla_NextDepartureTime,Pla_NextStartAddressId,Pla_NextEndAddressId,
          Pla_NextRouteInstanceId,Pla_NextCosts,
          Pla_NextTruckingKm
          from usr.p_mmpTankPlanning as pA
          where pA.preorderid <> in_PreOrderIdentifier and pA.Id <> Pla_Id
          and pA.ArrivalDate+IsNull(pA.ArrivalTime,'00:00:00') >= Local_DepartureDateTime
          //MF 16-02-2012
          order by pA.ArrivalDate asc,pA.ArrivalTime asc,pA.DepartureDate asc,pA.DepartureTime asc,pA.id asc;
        //
        if local_EnableLogging = 1 then
          message 'No Next record: ' || Pla_NextActionType || ' ' || Pla_NextStartDate || '/' || Pla_NextStartTime || ' - ' || Pla_NextEndDate || '/' || Pla_NextEndtime type info to console
        end if
      else
        if IsNull(Pla_NextNextId,0) = 0 then
          select first Id,TankId,PreOrderId,ActionType,ArrivalDate,ArrivalTime,StartDate,StartTime,EndDate,EndTime,DepartureDate,DepartureTime,StartAddressId,EndAddressId,RouteInstanceId,Costs,TruckingKm
            into Pla_NextNextId,Pla_NextNextTankId,Pla_NextNextPreOrderId,Pla_NextNextActionType,Pla_NextNextArrivalDate,Pla_NextNextArrivalTime,
            Pla_NextNextStartDate,Pla_NextNextStartTime,Pla_NextNextEndDate,Pla_NextNextEndTime,Pla_NextNextDepartureDate,Pla_NextNextDepartureTime,
            Pla_NextNextStartAddressId,Pla_NextNextEndAddressId,Pla_NextNextRouteInstanceId,Pla_NextNextCosts,
            Pla_NextNextTruckingKm
            from usr.p_mmpTankPlanning as pB
            where pB.preorderid <> in_PreOrderIdentifier and pB.Id <> Pla_NextId
            and pB.ArrivalDate+IsNull(pB.ArrivalTime,'00:00:00') >= Local_NextDepartureDateTime
            //MF 16-02-2012
            order by pB.ArrivalDate asc,pB.ArrivalTime asc,pB.DepartureDate asc,pB.DepartureTime asc,pB.id asc;
          //
          if local_EnableLogging = 1 then
            message 'No NextNext record: ' || Pla_NextActionType || ' ' || Pla_NextStartDate || '/' || Pla_NextStartTime || ' - ' || Pla_NextEndDate || '/' || Pla_NextEndtime type info to console
          end if
        end if end if;
      //
      set mod_ClosingDate = null;
      set mod_ClosingTime = null;
      set mod_PlannedFromActionArrivalDate = Pla_ArrivalDate;
      set mod_PlannedFromActionArrivalTime = Pla_ArrivalTime;
      set mod_PlannedFromActionStartDate = Pla_StartDate;
      set mod_PlannedFromActionStartTime = Pla_StartTime;
      set mod_PlannedFromActionEndDate = Pla_EndDate;
      set mod_PlannedFromActionEndTime = Pla_EndTime;
      set mod_PlannedFromActionDepartureDate = Pla_DepartureDate;
      set mod_PlannedFromActionDepartureTime = Pla_DepartureTime;
      set mod_startdate = Pla_StartDate;
      set mod_starttime = Pla_StartTime;
      set mod_addressfrom = Pla_StartAddressId;
      set mod_placefrom = (select first Address.LocationPlaceId from Address where addressid = Pla_StartAddressId);
      //
      if Pla_NextActionType = 'Trucking' then
        if Pla_ActionType = 'Trucking' then
          set mod_PlannedFromActionArrivalDate = Pla_NextArrivalDate;
          set mod_PlannedFromActionArrivalTime = Pla_NextArrivalTime;
          set mod_PlannedFromActionStartDate = Pla_NextStartDate;
          set mod_PlannedFromActionStartTime = Pla_NextStartTime;
          set mod_PlannedFromActionEndDate = Pla_NextEndDate;
          set mod_PlannedFromActionEndTime = Pla_NextEndTime;
          set mod_PlannedFromActionDepartureDate = Pla_NextDepartureDate;
          set mod_PlannedFromActionDepartureTime = Pla_NextDepartureTime;
          set mod_startdate = Pla_NextStartDate;
          set mod_starttime = Pla_NextStartTime;
          set mod_addressfrom = Pla_NextStartAddressId;
          set mod_placefrom = (select first Address.LocationPlaceId from Address where addressid = Pla_NextStartAddressId)
        end if;
        //
        set mod_PlannedToActionArrivalDate = Pla_NextNextArrivalDate;
        set mod_PlannedToActionArrivalTime = Pla_NextNextArrivalTime;
        set mod_PlannedToActionStartDate = Pla_NextNextStartDate;
        set mod_PlannedToActionStartTime = Pla_NextNextStartTime;
        set mod_PlannedToActionEndDate = Pla_NextNextEndDate;
        set mod_PlannedToActionEndTime = Pla_NextNextEndTime;
        set mod_PlannedToActionDepartureDate = Pla_NextNextDepartureDate;
        set mod_PlannedToActionDepartureTime = Pla_NextNextDepartureTime;
        set mod_enddate = Pla_NextNextArrivalDate;
        set mod_endtime = Pla_NextNextArrivalTime;
        set mod_placeto = (select first Address.LocationPlaceId from Address where addressid = Pla_NextEndAddressId);
        set Mod_AddressTo = Pla_NextEndAddressId;
        set mod_TruckingDuration = DATEDIFF(minute,Pla_Nextstartdate+Pla_Nextstarttime,Pla_Nextenddate+Pla_Nextendtime);
        set Mod_ABdistance = Pla_NextTruckingKM;
        //
        case Pla_NextNextActionType
        when 'Load' then
          set Mod_ToActionID = 0;
          set Mod_ToActionDescription = 'Loading'
        when 'Unload' then
          set Mod_ToActionID = 1;
          set Mod_ToActionDescription = 'Delivery'
        when 'Pickup' then
          set Mod_ToActionID = 2;
          set Mod_ToActionDescription = 'Pickup'
        when 'Drop' then
          set Mod_ToActionID = 3;
          set Mod_ToActionDescription = 'Drop'
        when 'Trucking' then
          set Mod_ToActionID = 4;
          set Mod_ToActionDescription = 'Road'
        when 'Cleaning' then
          set Mod_ToActionID = 5;
          set Mod_ToActionDescription = 'Clean'
        when 'Depot' then
          set Mod_ToActionID = 6;
          set Mod_ToActionDescription = 'Depot'
        when null then
          set Mod_ToActionID = 0;
          set Mod_ToActionDescription = '?????xx'
        end case
      else
        set mod_PlannedToActionArrivalDate = Pla_NextArrivalDate;
        set mod_PlannedToActionArrivalTime = Pla_NextArrivalTime;
        set mod_PlannedToActionStartDate = Pla_NextStartDate;
        set mod_PlannedToActionStartTime = Pla_NextStartTime;
        set mod_PlannedToActionEndDate = Pla_NextEndDate;
        set mod_PlannedToActionEndTime = Pla_NextEndTime;
        set mod_PlannedToActionDepartureDate = Pla_NextDepartureDate;
        set mod_PlannedToActionDepartureTime = Pla_NextDepartureTime;
        set mod_enddate = Pla_NextArrivalDate;
        set mod_endtime = Pla_NextArrivalTime;
        set mod_placeto = (select first Address.LocationPlaceId from Address where addressid = Pla_NextEndAddressId);
        set Mod_AddressTo = Pla_NextEndAddressId;
        set Mod_ABtime = DATEDIFF(minute,Pla_Nextstartdate+Pla_Nextstarttime,Pla_Nextenddate+Pla_Nextendtime);
        set Mod_ABdistance = 0;
        //
        case Pla_NextActionType
        when 'Load' then
          set Mod_ToActionID = 0;
          set Mod_ToActionDescription = 'Loading'
        when 'Unload' then
          set Mod_ToActionID = 1;
          set Mod_ToActionDescription = 'Delivery'
        when 'Pickup' then
          set Mod_ToActionID = 2;
          set Mod_ToActionDescription = 'Pickup'
        when 'Drop' then
          set Mod_ToActionID = 3;
          set Mod_ToActionDescription = 'Drop'
        when 'Cleaning' then
          set Mod_ToActionID = 5;
          set Mod_ToActionDescription = 'Clean'
        when 'Depot' then
          set Mod_ToActionID = 6;
          set Mod_ToActionDescription = 'Depot'
        when null then
          set Mod_ToActionID = 0;
          set Mod_ToActionDescription = '?????x'
        end case
      end if;
      //
      case Pla_ActionType
      when 'Load' then
        set Mod_FromActionID = 0;
        set Mod_FromActionDescription = 'Loading';
        set Mod_Modalityid = 1
      when 'Unload' then
        set Mod_FromActionID = 1;
        set Mod_FromActionDescription = 'Delivery';
        set Mod_Modalityid = 1
      when 'Pickup' then
        set Mod_FromActionID = 2;
        set Mod_FromActionDescription = 'Pickup';
        set Mod_Modalityid = 1
      when 'Route' then
        if Pla_RouteInstanceId < 0 then
          select route.routeid,route.modalityid,route.creditorid,ClosingTimeRegular,ClosingTimeHazardous
            into mod_routeid,mod_modalityid,mod_creditorid,rou_ClosingTimeRegular,rou_ClosingTimeHazardous
            from usr.RouteInstanceException
              join usr.Route
            where RouteInstanceExceptionId = ABS(Pla_RouteInstanceId)
        else
          select route.routeid,route.modalityid,route.creditorid,ClosingTimeRegular,ClosingTimeHazardous
            into mod_routeid,mod_modalityid,mod_creditorid,rou_ClosingTimeRegular,rou_ClosingTimeHazardous
            from usr.routeinstance
              join usr.route on routeinstance.routeid = route.routeid
            where RouteInstanceId = Pla_RouteInstanceId
        end if;
        //
        set Mod_FromActionID = 2;
        set Mod_FromActionDescription = 'Pickup';
        set Mod_ToActionID = 3;
        set Mod_ToActionDescription = 'Drop';
        set mod_PlannedToActionArrivalDate = Pla_ArrivalDate;
        set mod_PlannedToActionArrivalTime = Pla_ArrivalTime;
        set mod_PlannedToActionStartDate = Pla_StartDate;
        set mod_PlannedToActionStartTime = Pla_StartTime;
        set mod_PlannedToActionEndDate = Pla_EndDate;
        set mod_PlannedToActionEndTime = Pla_EndTime;
        set mod_PlannedToActionDepartureDate = Pla_DepartureDate;
        set mod_PlannedToActionDepartureTime = Pla_DepartureTime;
        set mod_enddate = Pla_DepartureDate;
        set mod_endtime = Pla_DepartureTime;
        set mod_placeto = (select first Address.LocationPlaceId from Address where addressid = Pla_EndAddressId);
        set mod_addressto = Pla_EndAddressId;
        //
        // check whether closing date/time should be filled
        //
        if rou_ClosingTimeRegular > 0 or rou_ClosingTimeHazardous > 0 then
          if exists(select 1 from usr.OrdProduct as OP join usr.Product as P on OP.ProductId = P.ProductId where OrderId = in_PreOrderIdentifier and IsNull(P.ADRClass,'') <> '') then
            set mod_ClosingDate = "Date"(DateAdd(minute,-1*rou_ClosingTimeHazardous,mod_StartDate+mod_StartTime));
            set mod_ClosingTime = DateFormat(DateAdd(minute,-1*rou_ClosingTimeHazardous,mod_StartDate+mod_StartTime),'hh:nn')
          else
            set mod_ClosingDate = "Date"(DateAdd(minute,-1*rou_ClosingTimeRegular,mod_StartDate+mod_StartTime));
            set mod_ClosingTime = DateFormat(DateAdd(minute,-1*rou_ClosingTimeRegular,mod_StartDate+mod_StartTime),'hh:nn')
          end if
        end if
      when 'Trucking' then
        set Mod_FromActionID = 4;
        set Mod_FromActionDescription = 'Road';
        set Mod_Modalityid = 1 when 'Cleaning' then
        set Mod_FromActionID = 5;
        set Mod_FromActionDescription = 'Clean';
        set Mod_Modalityid = 1
      end case; //case Pla_ActionType 
      //
      if MustInsertMod = 1 and Local_OnlyUseMMPTankSelection = 1
        and(pla_id < isnull(Local_FirstTankPlanningIdAfterUnload,0) or Local_FirstTankPlanningIdAfterUnload is null) then
        set MustInsertMod = 0
      end if;
      //
      if MustInsertMod = 1 then
        set Local_Distance_ABdistance = 0;
        set Local_Distance_ABtime = null;
        //
        select first abdistance,abtime
          into Local_Distance_ABdistance,Local_Distance_ABtime
          from usr.distance
          where placefrom = mod_placefrom and placeto = mod_placeto
          order by distanceid asc;
        //
        if local_EnableLogging = 1 then
          message 'Insert OM: ' || Mod_FromActionDescription || '-' || Mod_ToActionDescription || ', ' || mod_startdate || '/' || mod_starttime || ' - ' || mod_enddate || '/' || mod_endtime type info to console
        end if;
        //
        insert into OrdModality
          ( OrderId,
          ModalityId,
          CreditorId,
          FromPlaceId,
          ToPlaceId,
          RouteId,
          isRouteLOFO, //FV 06-06-2013 MMP 1269
          FromAction,
          FromActionDescription,
          ToAction,
          ToActionDescription,
          Status,
          browsesequence,
          ClosingDate,
          ClosingTime,
          DateOriginal,
          TimeOriginal,
          EndDate,
          EndTime,
          ABTime,
          ABDistance,
          AddressFromId,
          DepotId,
          TruckingDuration,
          PlannedFromActionArrivalDate,
          PlannedFromActionArrivalTime,
          PlannedFromActionStartDate,
          PlannedFromActionStartTime,
          PlannedFromActionEndDate,
          PlannedFromActionEndTime,
          PlannedFromActionDepartureDate,
          PlannedFromActionDepartureTime,
          PlannedToActionArrivalDate,
          PlannedToActionArrivalTime,
          PlannedToActionStartDate,
          PlannedToActionStartTime,
          PlannedToActionEndDate,
          PlannedToActionEndTime,
          PlannedToActionDepartureDate,
          PlannedToActionDepartureTime,GeneratedBy,DebugInfo ) values
          ( ToId,
          (if mod_routeid is not null then mod_modalityid else 1 endif),
          (if mod_routeid is not null then mod_creditorid else null endif),
          mod_placefrom,
          mod_placeto,
          mod_routeid,
          (if IsNull(mod_RouteId,0) > 0 then local_PreOrderIsRouteLOFO else 0 endif), //FV 06-06-2013 MMP 1269
          Mod_FromActionID,
          Mod_FromActionDescription,
          Mod_ToActionID,
          Mod_ToActionDescription,
          0,
          null,
          mod_ClosingDate,
          mod_ClosingTime,
          mod_startdate,
          mod_starttime,
          mod_enddate,
          mod_endtime,
          (if mod_routeid is null then(if Local_Distance_ABdistance is not null then Local_Distance_ABtime else null endif) endif),
          (if mod_routeid is null then(if Local_Distance_ABdistance is not null then Local_Distance_ABdistance else mod_ABDistance endif) endif),
          mod_addressfrom,
          mod_addressto,
          mod_TruckingDuration,
          mod_PlannedFromActionArrivalDate,
          mod_PlannedFromActionArrivalTime,
          mod_PlannedFromActionStartDate,
          mod_PlannedFromActionStartTime,
          mod_PlannedFromActionEndDate,
          mod_PlannedFromActionEndTime,
          mod_PlannedFromActionDepartureDate,
          mod_PlannedFromActionDepartureTime,
          mod_PlannedToActionArrivalDate,
          mod_PlannedToActionArrivalTime,
          mod_PlannedToActionStartDate,
          mod_PlannedToActionStartTime,
          mod_PlannedToActionEndDate,
          mod_PlannedToActionEndTime,
          mod_PlannedToActionDepartureDate,
          mod_PlannedToActionDepartureTime,2,'SP - CreateOrderFromPreorder 12 - ' || local_equipmentCodefld ) ;
        //
        set Local_NewOrdmodalityIdentifier = (select @@Identity);
        set Pla_PreOrderProductId = (select first preorderproductid from p_mmpOrderProductPlanning where tankplanningid = pla_id order by id asc);
        //
        if Mod_FromActionDescription = 'Loading' then
          insert into OrdModalityTankPlanning
            ( OrdmodalityIdentifier,PreOrderProductIdentifier,TankPLanningID,TankPlanningActionType ) values
            ( Local_NewOrdmodalityIdentifier,Pla_PreOrderProductId,pla_id,'Load' ) 
        end if;
        //
        if Mod_ToActionDescription = 'Delivery' then
          insert into OrdModalityTankPlanning
            ( OrdmodalityIdentifier,PreOrderProductIdentifier,TankPLanningID,TankPlanningActionType ) values
            ( Local_NewOrdmodalityIdentifier,Pla_PreOrderProductId,pla_id,'Unload' ) 
        end if
      end if
    end loop TankPlanningLoop; //
    close Cursor_TankPlanning
  end if; //if Local_PreOrder_Status = 3 then
  //
  if Local_PreOrderSwap = 1 then
    select first ordmodalityid
      into Local_FirstPickup_OrdmodalityIdentifier
      from usr.ordmodality
      where orderid = toid and fromaction = 2
      order by BrowseSequence asc
  end if;
  //
  if Local_PreOrderDropForDelivery = 1 then
    select first ordmodalityid
      into Local_LastDrop_OrdmodalityIdentifier
      from usr.ordmodality
      where orderid = toid and routeid is null and toaction = 3
      order by BrowseSequence desc;
    //
    // When DropForDelivery maybe we need to set isWithChassisToAction FV 18-12-2013  
    //
    if IsNull(Local_LastDrop_OrdmodalityIdentifier,0) > 0 and local_PreOrderDropOnChassis > 0 then
      update usr.OrdModality
        set isWithChassisToAction = 1
        where OrdModalityId = Local_LastDrop_OrdmodalityIdentifier
    end if
  end if;
  //
  if Local_PreOrderCallOff = 1 then
     select first ordmodalityid
       into Local_Last_OrdmodalityIdentifier
     from usr.ordmodality
     where orderid = toid
     order by BrowseSequence desc;
     //
     update usr.OrdModality
     set Stock = 1
     where OrdModalityId = Local_Last_OrdmodalityIdentifier
  end if;       
  //
  open Cursor_Modality;
  //
  ModalityLoop: loop
    fetch next Cursor_Modality into Local_OrdmodalityIdentifier,Local_OrdmodalityFromAction,Local_OrdmodalityToAction,Local_OrdmodalityRouteIdentifier;
    if sqlstate = err_notfound then
      leave ModalityLoop
    end if;
    //
    if Local_OrdmodalityIdentifier = Local_FirstPickup_OrdmodalityIdentifier then
      set Local_OrdmodalityFromAction = 0
    end if;
    //
    if Local_OrdmodalityIdentifier = Local_LastDrop_OrdmodalityIdentifier then
      set Local_OrdmodalityToAction = 1
    end if;
    //
    open Cursor_PreOrderProduct;
    //
    PreOrderProductLoop: loop
      fetch next Cursor_PreOrderProduct into Local_PreOrderProductIdentifier;
      if sqlstate = err_notfound then
        leave PreOrderProductLoop
      end if;
      //
      set Load_ModalityIdentifier = null;
      set UnLoad_ModalityIdentifier = null;
      //
      select first ordmodalityidentifier
        into Load_ModalityIdentifier
        from usr.OrdModalityTankPlanning
        where tankplanningactiontype = 'Load' and ordmodalityidentifier = Local_OrdmodalityIdentifier
        and preorderproductidentifier = Local_PreOrderProductIdentifier
        order by recordnumber asc;
      //
      if Load_ModalityIdentifier is null then
        select first ordmodalityidentifier
          into Load_ModalityIdentifier
          from usr.OrdModalityTankPlanning
          where tankplanningactiontype = 'Load'
          order by recordnumber asc
      end if;
      //
      select first ordmodalityidentifier
        into UnLoad_ModalityIdentifier
        from usr.OrdModalityTankPlanning
        where tankplanningactiontype = 'Unload' and ordmodalityidentifier = Local_OrdmodalityIdentifier
        and preorderproductidentifier = Local_PreOrderProductIdentifier
        order by recordnumber asc;
      //
      if UnLoad_ModalityIdentifier is null then
        select first ordmodalityidentifier into UnLoad_ModalityIdentifier from OrdModalityTankPlanning where tankplanningactiontype = 'Unload' order by recordnumber asc
      end if;
      //
      if UnLoad_ModalityIdentifier is null then
        set UnLoad_ModalityIdentifier = Load_ModalityIdentifier+100
      end if;
      //
      if Local_OrdmodalityIdentifier between Load_ModalityIdentifier and UnLoad_ModalityIdentifier then
        insert into ordmodproduct
          ( OrdModalityId,
          OrdProductId,
          ExporterId,
          ImporterId,
          ExportClrId,
          ImportClrId ) 
          select Local_OrdmodalityIdentifier,
            (select ordproductid from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = PreOrderProduct.Sequence),
            (select ExporterId from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = PreOrderProduct.Sequence),
            (select ImporterId from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = PreOrderProduct.Sequence),
            (select ExportClrId from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = PreOrderProduct.Sequence),
            (select ImportClrId from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = PreOrderProduct.Sequence)
            from PreOrderProduct join preorder
            where preorder.preorderid = In_PreOrderIdentifier
            and not exists(select * from ordmodproduct where OrdModalityId = Local_OrdmodalityIdentifier and OrdProductId = (select ordproductid from ordproduct where ordproduct.orderid = toid and ordproduct.sequence = PreOrderProduct.Sequence))
            and preorder.preorderid = In_PreOrderIdentifier
            and preorderproduct.sequence = (select sequence from preorderproduct where preorderproductid = Local_PreOrderProductIdentifier)
      end if
    end loop PreOrderProductLoop; //
    close Cursor_PreOrderProduct;
    //
    set Local_CustomsRemarks = (select MergePreOrderProductFields(In_PreOrderIdentifier,Local_OrdmodalityIdentifier,Local_OrdmodalityFromAction,Local_OrdmodalityToAction,Local_OrdmodalityRouteIdentifier,'CustomsRemarks'));
    set Local_MessageToDriver = (select MergePreOrderProductFields(In_PreOrderIdentifier,Local_OrdmodalityIdentifier,Local_OrdmodalityFromAction,Local_OrdmodalityToAction,Local_OrdmodalityRouteIdentifier,'MessageToDriver'));
    set Local_MessageToPlanner = (select MergePreOrderProductFields(In_PreOrderIdentifier,Local_OrdmodalityIdentifier,Local_OrdmodalityFromAction,Local_OrdmodalityToAction,Local_OrdmodalityRouteIdentifier,'MessageToPlanner'));
    //
    if Local_PreOrderSwap = 1 and(Local_OrdmodalityFromAction = 3 and Local_OrdmodalityToAction = 3) then
      set Local_MessageToDriver = Local_MessageToDriver || Local_PreOrderSwapRemark;
      set Local_MessageToPlanner = Local_MessageToPlanner || Local_PreOrderSwapRemark
    end if;
    //
    if Local_CustomsRemarks is not null or Local_MessageToDriver is not null or Local_MessageToPlanner is not null then
      update usr.ordmodality
        set CustomsRemarks = CustomsRemarks || Local_CustomsRemarks,
        PlannerMemoGeneral = PlannerMemoGeneral || Local_MessageToPlanner,
        InstructionGeneral = InstructionGeneral || Local_MessageToDriver
        where ordmodalityid = Local_OrdmodalityIdentifier
    end if
  end loop ModalityLoop; //
  close Cursor_Modality;
  //
  update ordmodality
    set OverrulePlanboardid = GetPlanboardRule(ordmodalityid)
    where ordmodality.orderid = ToId and ordmodality.routeid is null;
  //
  // Change status to Booked/Confirmed for Dossier related route deepsea modalities
  //
  update usr.OrdModality OM join tmp_OrdModalityStatus OMS on OM.OrdModalityId = OMS.OrdModalityId
    set OM.Status = OMS.Status;
  //
  // Fill start of chain with OverrulePlanboardId when OverruleStartOfchain is checked for a plan board rule
  //
  call usr.SetOverrulePlanboardAtBeginningOfChain(ToId);
  //
  update orderrequest
    set orderid = ToId
    where orderrequest.preorderid = ToId;
  //
  update orderrequestline
    set orderrequestline.ordproductid = (select first ordproductid from ordproduct where ordproduct.orderid = orderrequest.preorderid
      and ordproduct.sequence = preorderproduct.sequence order by ordproductid asc) from
    usr.orderrequestline
    join usr.orderrequest
    join usr.preorderproduct on orderrequestline.preorderproductid = preorderproduct.preorderproductid
    where orderrequest.preorderid = ToId;
  //
  if IsNull(TankPLanning_TankId,Local_PreOrder_EquipmentId,0) > 0 and usr.GetOption('INFORIT_MMP_Fill_Equipment_Active') <> 1 then
    call usr.MP_LastModalityWithEquipment(isnull(TankPLanning_TankId,Local_PreOrder_EquipmentId));
    //
    update usr.equipment
    set SkipLogMMP = 0
    where EquipmentId = isnull(TankPLanning_TankId,Local_PreOrder_EquipmentId);
    //
    update usr.EquipmentMMPInfo
    set SkipLogMMP = 0
    where EquipmentId = isnull(TankPLanning_TankId,Local_PreOrder_EquipmentId);
  end if;
  //
  case Local_PreOrder_Status
  when 2 then
    update preorder set status = 9 where preorderid = In_PreOrderIdentifier
  when 3 then
    if Local_OnlyUseMMPTankSelection = 1 and local_PreOrdBatchId = 0 then
      update preorder
        set status = 7
        where preorderid = In_PreOrderIdentifier
    else
      update preorder
        set status = 9
        where preorderid = In_PreOrderIdentifier
    end if
  end case; //
  return ToId
end