create or replace VIEW "USR"."JobFinancialsView"(OrderId,ProvisionalRevenue,ActualRevenue,ProvisionalCost,ActualCost,UnforeseenCosts,ToExpectCosts,ToBeInvoiced,ActualRevenueFreight,ActualRevenueExtra,ToBeInvoicedFreight,ToBeInvoicedExtra, TotalCalculationAmount)
//modified  13 sep 2005 Paul Evers  Fields added:  ActualRevenueFreight,ActualRevenueExtra,ToBeInvoicedFreight,ToBeInvoicedExtra
//PvdL 28-11-2018 Specify join between OrdInvoice and Curreny   
//DR   09-11-2020 Extra veld toegevoegd: TotalCalculationAmount
 as 
 select OrderId
  , isnull((select sum((AmountExcl/Per)*Factor) from usr.OrdInvoice join usr.currency on OrdInvoice.CurrencyId = Currency.CurrencyId where orderid = O.OrderId),0)
  , isnull((select sum((IRL.AmountExcl/Per)*Factor) from usr.InvRecLine as IRL join usr.InvReceivable join usr.Currency,usr.OrdInvoice as OI where(OI.OrdInvoiceId = IRL.OrdInvoiceId) and(OI.OrderId = O.OrderId)),0)
  , isnull((select sum((FCosts/Per)*Factor) from usr.OrdCost join usr.Currency where orderid = O.OrderId),0)
  , isnull((select sum((IPL.AmountExcl/Per)*Factor) from usr.InvPayLine as IPL join usr.InvPayable join usr.Currency,usr.OrdCost as OC where(IPL.OrdCostId = OC.OrdCostId) and(OC.OrderId = O.OrderId)),0)
  , isnull((select sum((UC.Amount/Per)*Factor) from usr.UnforeseenCost as UC join usr.Currency where(UC.OrderId = O.OrderId)),0)
  , isnull((select sum((OC.FCosts/Per)*Factor) from usr.OrdCost as OC join usr.Currency where(OC.orderid = O.OrderId) and((select first invpaylineid from usr.invpayline where OrdCostId = OC.OrdCostId) is null)),0)
  , isnull((select sum((oi.AmountExcl/Per)*Factor) from usr.OrdInvoice as oi join usr.currency on oi.CurrencyId = Currency.CurrencyId where(oi.orderid = O.OrderId) and((select first invreclineid from usr.invrecline where ordinvoiceid = oi.ordinvoiceid) is null)),0)
  , isnull((select sum((IRL.AmountExcl/Per)*Factor) from usr.InvRecLine as IRL join usr.InvReceivable join usr.Currency,usr.OrdInvoice as OI where (OI.OrdInvoiceId = IRL.OrdInvoiceId) and (OI.OrderId = O.OrderId) and (IRL."Type" = 'F')),0)
  , isnull((select sum((IRL.AmountExcl/Per)*Factor) from usr.InvRecLine as IRL join usr.InvReceivable join usr.Currency,usr.OrdInvoice as OI where (OI.OrdInvoiceId = IRL.OrdInvoiceId) and (OI.OrderId = O.OrderId) and (IRL."Type" = 'E')),0)
  , isnull((select sum((OI.AmountExcl/Per)*Factor) from usr.OrdInvoice as OI join usr.currency on oi.CurrencyId = Currency.CurrencyId where (OI.orderid = O.OrderId) and (OI."Type" = 'F') and ((select first invreclineid from usr.invrecline where ordinvoiceid = OI.ordinvoiceid) is null)),0)
  , isnull((select sum((OI.AmountExcl/Per)*Factor) from usr.OrdInvoice as OI join usr.currency on oi.CurrencyId = Currency.CurrencyId where (OI.orderid = O.OrderId) and (OI."Type" = 'E') and ((select first invreclineid from usr.invrecline where ordinvoiceid = OI.ordinvoiceid) is null)),0)
  , isnull((select (rate/per)*factor from precalc join usr.currency on Currency.CurrencyId = precalc.currencyid where precalcid in (select precalcid from prerevision where prerevisionid = (select prerevisionid from quote where quoteid = (select quoteid from orders ord where ord.orderid = o.orderid)))),0)
from usr.Orders as O